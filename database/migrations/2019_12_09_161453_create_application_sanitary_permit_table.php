<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationSanitaryPermitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_sanitary_permit', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reference_code')->nullable();
            $table->bigInteger('owner_user_id')->nullable();
            $table->bigInteger('business_id')->nullable();
            $table->string('status')->nullable();
            $table->string('type')->nullable();
            $table->datetime('application_date')->nullable();
            $table->string('payment_method')->nullable();
            $table->decimal('application_fee', 25, 2)->nullable();
            $table->decimal('service_fee', 25, 2)->nullable();
            $table->decimal('total', 25, 2)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_sanitary_permit');
    }
}
