<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBusinessTableAddAddressDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('business', function($table){
            $table->string('region_name')->nullable()->after('region');
            $table->string('province_name')->nullable()->after('province');
            $table->string('town_name')->nullable()->after('town');
            $table->string('brgy_name')->nullable()->after('brgy');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('business', function($table){
            $table->dropColumn(['region_name', 'province_name', 'town_name', 'brgy_name']);
        });
    }
}
