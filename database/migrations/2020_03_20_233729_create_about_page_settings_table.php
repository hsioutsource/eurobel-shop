<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAboutPageSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('about_page_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('align_image_left')->nullable();
            $table->longText('image')->nullable();
            $table->longText('text')->nullable();
            $table->longText('indexposition')->nullable();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('about_page_settings');
    }
}
