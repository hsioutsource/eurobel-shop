<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddThirdLineAndTextColorToProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('third_line')->nullable()->after('description');
            $table->string('text_color')->nullable()->after('third_line');
            $table->string('background_color')->nullable()->after('third_line');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            // $table->dropColumn('third_line');
            // $table->dropColumn('text_color');
            // $table->dropColumn('background_color');
        });
    }
}
