<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionManagerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_manager', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('owner_user_id')->nullable();
            $table->bigInteger('business_id')->nullable();
            $table->string('reference_code')->nullable();
            $table->string('type')->nullable();
            $table->decimal('amount', 25, 2)->nullable();
            $table->string('status')->nullable();
            $table->datetime('expiry_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_manager');
    }
}
