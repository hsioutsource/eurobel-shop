<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUserTableAddOtherInfoFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('user', function($table){
            $table->string('suffix')->nullable()->after('name');
            $table->string('lastname')->nullable()->after('name');
            $table->string('middlename')->nullable()->after('name');
            $table->string('firstname')->nullable()->after('name');
            $table->string('tin_no')->nullable()->after('type');
            $table->string('pagibig_no')->nullable()->after('type');
            $table->string('philhealth_no')->nullable()->after('type');
            $table->string('sss_no')->nullable()->after('type');
            $table->string('street_address', 255)->nullable()->after('type');
            $table->string('house_no')->nullable()->after('type');
            $table->string('zipcode')->nullable()->after('type');
            $table->string('brgy')->nullable()->after('type');
            $table->string('town')->nullable()->after('type');
            $table->string('province')->nullable()->after('type');
            $table->string('region')->nullable()->after('type');
            $table->string('tel_no')->nullable()->after('type');
            $table->string('mobile_no')->nullable()->after('type');
            $table->string('citizenship')->nullable()->after('type');
            $table->string('civil_status')->nullable()->after('type');
            $table->date('birthdate')->nullable()->after('type');
            $table->string('gender')->nullable()->after('type');
            $table->datetime('mobile_no_verified_at')->nullable()->after('email_verified_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('user', function($table){
            $table->dropColumn(['suffix', 'lastname', 'firstname', 'middlename', 'tin_no', 'pagibig_no', 'philhealth_no', 'sss_no', 'street_address', 'house_no', 'zipcode', 'brgy', 'town', 'province', 'region', 'tel_no', 'mobile_no', 'citizenship', 'civil_status', 'birthdate', 'gender', 'mobile_no_verified_at']);
        });
    }
}
