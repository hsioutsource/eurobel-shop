<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransactionManagerTableAddPaymentStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('transaction_manager', function($table){
            $table->string('payment_status')->default("pending")->nullable()->after('status');
            $table->string('payment_method')->default("DIGIPEP")->nullable()->after('status');
            $table->timestamp('payment_date')->nullable()->after('status');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('transaction_manager', function($table){
            $table->dropColumn(['payment_status','payment_method','payment_date']);
        });
    }
}
