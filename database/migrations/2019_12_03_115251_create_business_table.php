<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('owner_user_id')->unsigned()->nullable();
            $table->string('business_type')->nullable();
            $table->string('business_scope')->nullable();
            $table->string('business_number')->nullable();
            $table->string('bn_number')->nullable();
            $table->string('dominant_name')->nullable();
            $table->string('business_name')->nullable();
            $table->string('mobile_no')->nullable();
            $table->string('telephone_no')->nullable();
            $table->string('email', 255)->nullable();
            $table->decimal('capitalization',25,2)->nullable();
            $table->integer('no_of_employee')->nullable();
            $table->string('unit_no')->nullable();
            $table->string('street_address', 255)->nullable();
            $table->string('brgy')->nullable();
            $table->string('zipcode')->nullable();
            $table->string('town')->nullable();
            $table->string('province')->nullable();
            $table->string('region')->nullable();
            $table->string('sss_no')->nullable();
            $table->string('philhealth_no')->nullable();
            $table->string('pagibig_no')->nullable();
            $table->string('tin_no')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business');
    }
}
