<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScheduleDemosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedule_demos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('demo_date')->nullable();
            $table->longText('demo_time')->nullable();
            $table->longText('full_name')->nullable();
            $table->longText('address')->nullable();
            $table->longText('email_address')->nullable();
            $table->longText('contact_number')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedule_demos');
    }
}
