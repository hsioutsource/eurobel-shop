<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomePageImageslidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_page_imagesliders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('image_banner_1')->nullable();
            $table->longText('image_banner_text1')->nullable();
            $table->longText('image_banner_2')->nullable();
            $table->longText('image_banner_text2')->nullable();
            $table->longText('image_banner_3')->nullable();
            $table->longText('image_banner_text3')->nullable();
            $table->longText('image_banner_4')->nullable();
            $table->longText('image_banner_text4')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_page_imagesliders');
    }
}
