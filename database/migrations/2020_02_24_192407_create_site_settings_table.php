<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('show_banner');
            $table->string('banner_modal_image')->nullable();
            $table->longText('mission_vision');
            $table->longText('facebook_link');
            $table->longText('instagram_link');
            $table->longText('email')->nullable();
            $table->longText('contact_number_1')->nullable();
            $table->longText('contact_number_2')->nullable();
            $table->longText('contact_number_3')->nullable();
            $table->longText('contact_number_4')->nullable();
            $table->longText('catalog_link')->nullable();
            $table->longText('catalog_name')->nullable();
            $table->longText('privacy_link')->nullable();
            $table->longText('privacy_name')->nullable();
            $table->longText('terms_of_service_link')->nullable();
            $table->longText('terms_of_service_link_name')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_settings');
    }
}
