<?php

use Illuminate\Database\Seeder;
use App\Laravel\Models\HomePageImageslider;


class HomagePageImagesliderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $homepageSiteSettings = HomePageImageslider::create([
            "image_banner_1" =>  env('APP_URL').'frontend/images/banner/banner9.png',
            "image_banner_text1" => "AMENITY + STYLE + WARMTH",
            "image_banner_2" =>  env('APP_URL').'frontend/images/banner/banner9.png',
            "image_banner_text2" => "AMENITY + STYLE + WARMTH",
            "image_banner_3" =>  env('APP_URL').'frontend/images/banner/banner8.png',
            "image_banner_text3" => "AMENITY + STYLE + WARMTH",
            "image_banner_4" =>  env('APP_URL').'frontend/images/banner/banner7.png',
            "image_banner_text4" => "AMENITY + STYLE + WARMTH",
        ]);

    }
}
