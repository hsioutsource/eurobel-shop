<?php

use App\Laravel\Models\SiteSettings;
use Illuminate\Database\Seeder;

class SiteSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sitesettings = SiteSettings::create([
            'show_banner' => "1", 
            'mission_vision' => 'Our flooring technicians are real craftsmen, passionate about making your living space simply fabulous. We use only the highest quality tools for Dustless Sanding, Floor and Staircase Refinishing and Repair. We also take special care to ensure a safe, clean and eco-friendly work environment.',
            'facebook_link' =>'https//facebook.com',
            'instagram_link' => 'https//instagram.com',
            'email' => 'eurobel@gmail.com',
            'contact_number_1' => '51112331',
            'contact_number_2' => '51123111',
            'contact_number_3' => '511123111',
            'contact_number_4' => '51123111',
            'banner_modal_image' => env('APP_URL').'frontend/images/sale.png'
        ]);

       
    }
}
