<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Laravel\Services\CustomValidator;
use Illuminate\Support\Facades\Validator;
use Schema;
use App\Laravel\Models\SiteSettings;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        Validator::resolver(function($translator, $data, $rules, $messages)
        {
            return new CustomValidator($translator, $data, $rules, $messages);
        });


        View::composer('*', function ($view) {
            $sitesettings = SiteSettings::where('id', '=', '1')->get();
            $view->with(['sitesettings' => $sitesettings]);
    
        });
    }
}
