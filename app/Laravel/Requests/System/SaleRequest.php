<?php

namespace App\Laravel\Requests\System;

use App\Laravel\Requests\RequestManager;

class SaleRequest extends RequestManager
{
    public function rules()
    {
        if(request()->route()->methods()[0] === "PATCH")
        {
            $rules = [
                'name' => "required",
                'code' => "required",
                "sizes" => "required|array",
                "sizes.*" => "required",
                "conversion_sizes" => "required|array",
                "conversion.*" => "required",
                "prices" => "array|max:5",
                "prices.*" => "integer|nullable",
                "image_rectangle" => "image|mimes:jpeg,jpg,png,gif,svg",
                "image_square" => "image|mimes:jpeg,jpg,png,gif,svg",
                "image_circle" => "image|mimes:jpeg,jpg,png,gif,svg",
            ];
        }
        else
        {
            $rules = [
                'name' => "required",
                'code' => "required",
                "sizes" => "required|array",
                "conversion_sizes" => "required|array",
                "prices" => "array|max:5",
                "prices.*" => "integer|nullable",
                "sizes.*" => "required",
                "conversion.*" => "required",
                "image_rectangle" => "image|mimes:jpeg,jpg,png,gif,svg",
                "image_square" => "image|mimes:jpeg,jpg,png,gif,svg",
                "image_circle" => "image|mimes:jpeg,jpg,png,gif,svg",
            ];
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'price.integer' => "Price should be a whole value",
        ];
    }


}