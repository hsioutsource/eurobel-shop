<?php

namespace App\Laravel\Middleware\System;

use App\Laravel\Models\Sale;
use Closure;

class SaleExist
{
    public function handle($request,Closure $next)
    {
        $sale = Sale::find($request->route('id'));

        if($sale)
        {
            $request->merge([
                'sale' => $sale,
            ]);

            return $next($request);
        }
        else
        {
            session('error-message',"Sale Not Found");
            
            return redirect()->route('system.sale.index');
        }
    }
}