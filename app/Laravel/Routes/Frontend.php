<?php

/*,'domain' => env("FRONTEND_URL", "wineapp.localhost.com")*/

Route::group(['as' => "frontend.",
		 'namespace' => "Frontend",
		 'middleware' => ["web"]
		],function() {

	Route::group(['prefix' => "/",'as' => "home."],function(){
		Route::get('/',['as' => "index",'uses' => "HomeController@index"]);
		// Route::get('/underdevelopment',['as' => "index",'uses' => "HomeController@index"]);
	});


	Route::group(['prefix' => "/about-us",'as' => "about."],function(){
		Route::get('/',['as' => "index",'uses' => "AboutController@index"]);
	});

	Route::group(['prefix' => "showroom",'as' => "showroom."],function(){
		Route::get('/',['as' => "index",'uses' => "ShowroomController@index"]);
	});

	Route::group(['prefix' => "contact",'as' => "contact."],function(){
		Route::get('/',['as' => "index",'uses' => "ContactController@index"]);
		Route::post('/save',['as' => "save",'uses' => "ContactController@save"]);

	});



	Route::group(['prefix' => "subscribe",'as' => "subscribe."],function(){
		Route::post('/save',['as' => "save",'uses' => "SubscribeController@save"]);

	});


	Route::group(['prefix' => "demo-list",'as' => "demo."],function(){
		Route::get('/{id}',['as' => 'remove', 'uses' => "DemoController@removeDemolist"]);
		Route::get('/',['as' => "index",'uses' => "DemoController@index"]);
		Route::post('/save',['as' => "save",'uses' => "DemoController@save"]);
	});


	Route::group(['prefix' => "product",'as' => "product."],function(){
		Route::get('/',['as' => "index",'uses' => "ProductController@index"]);
		Route::get('/search',['as' => "search",'uses' => "ProductController@search"]);
		Route::get('/filter', ['as' => 'filter', 'uses' => 'ProductController@filter']);
		Route::get('show/{id?}/{code?}',['as' => "show",'uses' => "ProductController@show"]);
		Route::get('{product_id}/demo/{demo_id}',['as' => "demo",'uses' => "ProductController@storeDemoProduct"]);
	});

	Route::group(['prefix' => "download",'as' => "download."],function(){
		Route::get('/catalog/{download_name}/{file_name}',['as' => "catalog",'uses' => "DownloadController@downloadCatalog"]);
	});

	Route::group(['prefix' => "sales","as" => "sales."],function(){
		Route::get('/',["as" => "index","uses" => "SaleController@index"]);
		Route::get('{id?}/show',['as' => 'show', "uses" => "SaleController@show"]);
	});

});