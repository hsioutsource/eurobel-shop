<?php

/*,'domain' => env("FRONTEND_URL", "wineapp.localhost.com")*/
Route::group([
		'as' => "system.",
		'namespace' => "System",
		'prefix' => "admin",
		],function() {

	Route::group(['middleware'=>['system.guest']],function(){
       Route::get('login/{redirect_uri?}',['as' => "login",'uses' => "AuthController@login"]);
		Route::post('login/{redirect_uri?}',['uses' => "AuthController@authenticate"]);
    });



	Route::group(['middleware' => ["system.auth"]], function(){
		
		Route::get('/',['as' => "index",'uses' => "ProductController@index"]);
		Route::get('lock',['as' => "lock", 'uses' => "AuthController@lock"]);
		Route::post('lock',['uses' => "AuthController@unlock"]);
		Route::get('logout',['as' => "logout",'uses' => "AuthController@destroy"]);

		Route::group(['prefix' => "/sitesettings",'as' => "sitesettings."],function(){
			Route::get('/',['as' => "index",'uses' => "SiteSettingsController@index"]);
			Route::post('update',['as' => "update",'uses' => "SiteSettingsController@update"]);
		});


		Route::group(['prefix' => "/aboutpagesettings",'as' => "aboutpagesettings."],function(){
			Route::get('/',['as' => "index",'uses' => "AboutPageSettingsController@index"]);
			Route::post('update',['as' => "update",'uses' => "AboutPageSettingsController@update"]);
			
		});



		Route::group(['prefix' => "/products",'as' => "products."],function(){
			Route::get('/',['as' => "index",'uses' => "ProductController@index"]);
			Route::get('{id}/edit',['as' => "edit",'uses' => "ProductController@edit"]);
			Route::post('{id}/update',['as' => "edit",'uses' => "ProductController@update"]);
			Route::get('/create',['as' => "create",'uses' => "ProductController@create"]);
			Route::post('/save',['as' => "save",'uses' => "ProductController@save"]);
			Route::get('{id}/delete',['as' => "delete",'uses' => "ProductController@delete"]);
		});

		Route::group(['prefix' => "/inquiries",'as' => "inquiries."],function(){
			Route::get('/',['as' => "index",'uses' => "InquiryController@index"]);
			Route::get('{id}/delete',['as' => "delete",'uses' => "InquiryController@delete"]);
		});


		Route::group(['prefix' => "/subscribe",'as' => "subscribe."],function(){
			Route::get('/',['as' => "index",'uses' => "SubscribeController@index"]);
			Route::get('{id}/delete',['as' => "delete",'uses' => "SubscribeController@delete"]);
		});

		Route::group(['prefix' => "/showroom",'as' => "showroom."],function(){
			Route::get('/',['as' => "index",'uses' => "ShowRoomController@index"]);
			Route::get('/create',['as' => "create",'uses' => "ShowRoomController@create"]);
			Route::post('{id}/update',['as' => "edit",'uses' => "ShowRoomController@update"]);
			Route::post('/save',['as' => "save",'uses' => "ShowRoomController@save"]);
			Route::get('{id}/edit',['as' => "edit",'uses' => "ShowRoomController@edit"]);
			Route::get('{id}/delete',['as' => "delete",'uses' => "ShowRoomController@delete"]);
		});
		
		Route::group(['prefix' => "/scheduled-demo",'as' => "scheduled-demo."],function(){
			Route::get('/',['as' => "index",'uses' => "DemoController@index"]);
			Route::get('{id}/show',['as' => "show",'uses' => "DemoController@show"]);
			Route::get('{id}/delete',['as' => "delete",'uses' => "DemoController@delete"]);
		});
		

		Route::group(['prefix' => "/transaction",'as' => "transaction."],function(){
			Route::get('/',['as' => "index",'uses' => "TransactionController@index"]);
			Route::get('/details',['as' => "show",'uses' => "TransactionController@show"]);
		});

		Route::group(['prefix' => "/reporting",'as' => "reporting."],function(){
			Route::get('/daily',['as' => "daily",'uses' => "ReportingController@daily"]);
			Route::get('/monthly',['as' => "monthly",'uses' => "ReportingController@monthly"]);
			Route::get('/transaction-logs',['as' => "transaction_logs",'uses' => "ReportingController@transaction_logs"]);
		});

		Route::group(['prefix' => "/account-management",'as' => "account_management."],function(){
			Route::group(['prefix' => "/user",'as' => "user."],function(){
				Route::get('/',['as' => "index",'uses' => "AccountManagementController@user"]);
				Route::get('/create',['as' => "create",'uses' => "AccountManagementController@create_user"]);
				Route::get('/list-of-transaction',['as' => "list_of_transaction",'uses' => "AccountManagementController@list_of_transaction"]);
			});
			Route::group(['prefix' => "system-user",'as' => "system_user."],function(){
				Route::get('/',['as' => "index",'uses' => "AccountManagementController@system_user"]);
				Route::get('/create',['as' => "create",'uses' => "AccountManagementController@create_system_user"]);
			});
		});

		Route::group(['prefix' => "/settings",'as' => "settings."],function(){
			Route::get('/',['as' => "index",'uses' => "SettingsController@index"]);
		});

		Route::group(['prefix' => "sale", 'as' => "sale."],function(){
			Route::get('/',['as' => "index", "uses" => "SaleController@index"]);
			Route::get('create',['as' => "create", "uses" => "SaleController@create"]);
			Route::post('create',['as' => "store", "uses" => "SaleController@store"]);
			Route::group(['middleware' => "system.saleExist"],function(){
				Route::get('{id?}/show',['as' => "show", "uses" => "SaleController@show"]);
				Route::get('{id?}/edit',['as' => "edit", "uses" => "SaleController@edit"]);
				Route::patch('{id?}/edit',['as' => "update", "uses" => "SaleController@update"]);
				Route::get('{id?}/delete',['as' => "delete", "uses" => "SaleController@delete"]);
			});
		});
	
	});

});