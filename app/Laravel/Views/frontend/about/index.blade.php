@extends('frontend._layouts.main')
@section('page-dynamic-metas')
<title>Eurobel - About Us</title>
<meta property="og:url"  content="{{env('APP_URL').'about-us'}}" />
<meta property="og:description" content="AMENITY + STYLE + WARMTH. Serving you as the largest retailer of premium rugs and carpets in the Philippines since 1995. Book your FREE DEMO today! @eurobel_rugs, @eurobelrugscarpets, @eurobelrugsandcarpets@gmail.com" />

<meta name="robots" content="index, follow">

<meta name="description" content="AMENITY + STYLE + WARMTH. Serving you as the largest retailer of premium rugs and carpets in the Philippines since 1995. Book your FREE DEMO today! @eurobel_rugs, @eurobelrugscarpets, @eurobelrugsandcarpets@gmail.com">

@stop

@section('content')

<!-- ===================================================
  Loading Transition
==================================================== -->
<div id="loader-wrapper">
    <div id="loader"></div>
</div>

<!--
=============================================
  Theme Header
==============================================
-->
<!--
      =============================================
        Theme inner Banner
      ==============================================
      -->
<div class="inner-banner">
    <div class="opacity">
        <div class="container banner-container">
            <h2>ABOUT US</h2>

            <div class="clearfix">
                <ul class="banner-container-indicator">
                    <li><a href="{{route('frontend.home.index')}}" class="tran3s">Home</a></li>
                    <li>/</li>
                    <li><a href="#" class="tran3s">About Us</a></li>

                </ul>
            </div>
        </div>
        <!-- /.container -->
    </div>
    <!-- /.opacity -->
</div>
<!-- /.inner-banner -->

<!-- 
      =============================================
        Shop Page 
      ============================================== 
      -->
<div class="request-quote-section bg-light-gray" style="padding:50px 0px 0px 0px">

    <div class="container" style="margin-bottom:5%">

        @if(!$aboutpagesettings->isEmpty()) 
        @foreach($aboutpagesettings as $aboutpagesettings)
        @if($aboutpagesettings->align_image_left === '1')
     
        <div class="row section-container">
                <div class="col-md-6">
                    <div class=" about-us-image__container"> 
                        <img  class="about-us-image" src="{{$aboutpagesettings->image}}" alt="">
                   </div>
                </div>
                <!-- /.quote-form -->
                <div class="col-md-6 ">
                    <div class="text-about-us"> 
                         {!! $aboutpagesettings->text !!}
                    </div>
                </div>
                <!-- /.text -->
        </div>
            <!-- /.row -->
        @else
        <div class="row section-container">
            <div class="col-md-6">
                <div class="text-about-us"> 
                      {!! $aboutpagesettings->text !!}
                </div>
            </div>
            <!-- /.text -->

            <div class="col-md-6">
                <div class=" about-us-image__container"> 
                     <img  class="about-us-image" src="{{$aboutpagesettings->image}}" alt="">
                </div>
            </div>
            <!-- /.quote-form -->
        </div>
        @endif
        @endforeach
        @else
        <div class="row">
            <div class="col-md-12"> 
                  <h1>Kindly update the content on the cms....</h1>
            </div>
        </div>
        @endif



        

    </div>
    <!-- /.container -->
    <div class="about-slider" style="margin: 0px !important">
    <div class="opacity opacityBg" style="background: rgba(0,0,0,0.75)">
        <div class="container text-white">
           <div class="row equal">
               <div class="col-md-4 col-md-4-vertical" style="vertical-align: middle;">
                    <h1 class=" text-white text-uppercase mt-10 ">Mission / Vision</h1>
               </div>
               <div class="col-md-8  text-justify">
                @foreach($sitesettings as $sitesetting)
                    <p>{!! $sitesetting->mission_vision !!}</p>
                 @endforeach
               </div>
           </div>
          
        </div>
        <!-- /.container -->
    </div>
    <!-- /.opacity -->
</div>
</div>
<!-- /.request-quote-section -->

<!-- Scroll Top Button -->
<button class="scroll-top tran3s bg-blue-royal">
    <i class="fa fa-arrow-up" aria-hidden="true"></i>
</button>

@stop @section('page-styles')
<style type="text/css">
       
    .client-slider .opacityBg {background: rgba(0,0,0,0.75);}

    .about-us-image__container {
        width: auto;
        height:  auto;
     }

     .about-us-image {
         width: 100%;
         height: 100%;
         object-fit: cover;
         object-position: center;
        margin-bottom: 5%;
     }

     .section-container { 
        margin-bottom: 5%;  
        margin-top: 5%;
        display: block; 
        align-items:left
     }


     @media(min-width:768px) { 

        .section-container { 
        margin-bottom: 5%;  
        display: flex; 
        align-items:center;
         }
         .about-us-image__container {
            width: 100%;
            height:  400px;
        }

     }

     @media(min-width:992px) { 
         .about-us-image__container {
            width: 100%;
            height:  350px;
        }

     }
    .equal {
        display: flex;
        display: -webkit-flex;
        flex-wrap: wrap;
    }
    .col-md-4-vertical {
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .about-slider {
        background: url(frontend/images/mission_vision_bg.jpg) no-repeat center;
        background-size: cover;
        background-attachment: fixed;
        margin: 90px 0 115px 0;
    }
    .about-slider .opacity {
        text-align: center;
        padding: 80px 0 80px 0;
    }
    .about-slider .opacity p{
        font-size: 16px;
    }
    
    .about-slider .opacity h1{
      letter-spacing: 8px !important;
    }
    .about-slider .opacity h6 {
        color: #fff;
        position: relative;
        display: inline-block;
        margin-bottom: 52px;
    }
    .shop-page .main-wrapper .shop-sidebar {
        width: 100% !important;
    }
    
    .shop-page .main-wrapper .col {
        padding: 0px !important;
    }
    
    .shop-page .main-wrapper .shop-sidebar .price-ranger .theme-button {}
    
    .shop-page .main-wrapper .shop-sidebar .price-ranger {
        border: 1px solid transparent !important;
        margin-bottom: 0px !important;
        padding: 18px 20px 20px 20px !important;
    }
    
    .shop-page .main-wrapper .shop-sidebar .price-ranger .ui-widget-header {
        background: #172A55 !important;
    }
    
    .mt-10 {
        margin-top: 10px !important;
    }
    
    .shop-page .shop-page-pagination li a:hover {
        background-color: #172A55 !important;
        border-color: #172A55 !important;
        color: white !important;
    }
    
    .shop-page .shop-page-pagination li:last-child a {
        background-color: #172A55 !important;
        border-color: #172A55 !important;
        color: white !important;
    }
    
    .shop-page .main-wrapper .shop-sidebar .price-ranger .ui-slider-handle {
        border: solid !important;
    }
</style>

@stop