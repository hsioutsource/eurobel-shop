@extends('frontend._layouts.main')

@section('page-dynamic-metas')
<title>Eurobel - Contact Us</title>
<meta property="og:url"  content="{{env('APP_URL').'contact'}}" />
<meta property="og:description" content="AMENITY + STYLE + WARMTH. Serving you as the largest retailer of premium rugs and carpets in the Philippines since 1995. Book your FREE DEMO today! @eurobel_rugs, @eurobelrugscarpets, @eurobelrugsandcarpets@gmail.com" />

<meta name="robots" content="index, follow">

<meta name="description" content="AMENITY + STYLE + WARMTH. Serving you as the largest retailer of premium rugs and carpets in the Philippines since 1995. Book your FREE DEMO today! @eurobel_rugs, @eurobelrugscarpets, @eurobelrugsandcarpets@gmail.com">

@stop



@section('content')
<!-- ===================================================
    Loading Transition
    ==================================================== -->
    <div id="loader-wrapper">
        <div id="loader"></div>
    </div>
    
    <div class="inner-banner">
        <div class="opacity">
            <div class="container banner-container">
                <h2>CONTACT US</h2>
                
                <div class="clearfix">
                    <ul class="banner-container-indicator">
                        <li><a href="{{route('frontend.home.index')}}" class="tran3s">Home</a></li>
                        <li>/</li>
                        <li><a href="#" class="tran3s">Contact Us</a></li>
                    </ul>
                </div>
            </div> <!-- /.container -->
        </div> <!-- /.opacity -->
    </div> <!-- /.inner-banner -->
    
    <div class="bg-light-gray">
        <div class="opacity opacityOne" style="padding-top: 40px;padding-bottom: 90px">
            <div class="container text-white">
                <h3 class=" text-blue text-center mb-30 mt-50">CONTACT US</h3>
                <div class="row">
                      <!-- error message --> 
                @if ($errors->any())
                <div class="col-md-12 col-lg-12 mt-3">
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                @endif
                <!-- error message --> 
                
                <!-- success message --> 
                @if(Session::has('success-message'))
                <div class="col-md-12 col-lg-12  mt-3">
                    <div class="alert alert-success">
                        {{ Session::get('success-message') }}
                    </div>
                </div>
                @endif

                        <!-- error message --> 
                 @if(Session::has('error-message'))
                <div class="col-md-12 col-lg-12  mt-3">
                    <div class="alert alert-danger">
                        {{ Session::get('error-message') }}
                    </div>
                </div>
                @endif
                
                <!-- success message --> 
                    <div class="col-md-12 col-sm-12">
                        <div class="row">
                        <form action="{{route('frontend.contact.save')}}" method="POST"> 
                                @csrf
                                <div class="col-md-12">
                                <input type="text" placeholder="Full Name" class="form-control input-contact border-blue" name="full_name" value="{{old('full_name')}}" required>
                                </div>
                                <div class="col-md-6">
                                    <input type="email" placeholder="Email Address" class="form-control input-contact border-blue" name="email" value="{{old('email')}}">
                                </div>
                                <div class="col-md-6">
                                    <input type="number" placeholder="Contact Number" class="form-control input-contact border-blue" name="contact_number" value="{{old('contact_number')}}">
                                </div>
                                <div class="col-md-12 text-center">
                                    <input type="submit" class="btn bg-blue text-white" value="SUBMIT" style="border-radius: 10px;padding: 15px 50px 15px 50px" />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                
            </div>
            <!-- /.container -->
        </div>
        <!-- /.opacity -->
    </div>
    <div class="client-slider" style="margin: 0px !important">
        <div class="opacity opacityOne">
            <div class="container text-white">
                
                <div class="row">
                    <div class="col-md-6">
                        <h3 class=" text-white">Schedule a free demo now!</h3>
                    </div>
                    <div class="col-md-6">
                        <a href="/demo-list?#demo" class="btn bg-blue text-white" style="border-radius: 10px;padding: 15px 50px 15px 50px">SCHEDULE DEMO</a>
                    </div>
                    
                </div>
                
            </div>
            <!-- /.container -->
        </div>
        <!-- /.opacity -->
    </div>
    <!-- /.client-slider -->
    
    <!-- Scroll Top Button -->
    <button class="scroll-top tran3s bg-blue-royal">
        <i class="fa fa-arrow-up" aria-hidden="true"></i>
    </button>
    
    @stop @section('page-styles')
    <style type="text/css">
        .our-service {
            padding: 80px 0 80px 0;
            margin: 0px 0 0px 0 !important;
        }
        .client-slider .opacity {
            text-align: center;
            padding: 50px 0 50px 0 !important;
        }
        .border-blue {
            border:1px solid #172A55 !important;
        }
    </style>
    @stop