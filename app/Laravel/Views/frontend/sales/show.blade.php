@extends('frontend._layouts.main')
@section('page-dynamic-metas')
  <title>Eurobel - {{$sale->name}}</title>
  <meta name="twitter:image" content="{{$sale->image_rectangle}}">
  <meta name="twitter:image:alt" content="{{$sale->name}}"/>
  <meta property="og:title" content="Eurobel -{{$sale->name}}"/>
  <meta property="og:description" content="{{$sale->description}}" />
  <meta property="og:image" content="{{$sale->image_rectangle}}"/>
  <meta property="og:url"  content="{{env('APP_URL').'product/'.$sale->id.'/show'}}" />
@stop

@section('content')
    
  <!-- ===================================================
    Loading Transition
  ==================================================== -->
  <div id="loader-wrapper">
    <div id="loader"></div>
  </div>


  <!--
  =============================================
    Theme Header
  ==============================================
  -->
  <!--
        =============================================
          Theme inner Banner
        ==============================================
        -->
    
        <div class="inner-banner">
          <div class="opacity">
            <div class="container banner-container">
              <h2>PRODUCT DETAILS</h2>
              <div class="clearfix">
                <ul class="banner-container-indicator">
                  <li><a href="{{route('frontend.home.index')}}" class="tran3s">Home</a></li>
                  <li>/</li>
                  <li><a href="{{route('frontend.product.index')}}" class="tran3s">Products</a></li>
                  <li>/</li>
                  <li><a href="#" class="tran3s">{{$sale->name}}</a></li>
                </ul>
              </div>
            </div> <!-- /.container -->
          </div> <!-- /.opacity -->
        </div> <!-- /.inner-banner -->




        <!-- 
        =============================================
        product-page 
        ============================================== 

        -->
        <div class="shop-page shope-page-container">
          <div class="container-fluid">
            <div class="main-wrapper">
              <div class="col float-right shop-details shop-large-side">
                <div class="single-product-details clearfix">
                  <div class="container">
                    <div class="row" style="margin-bottom:3%">
                      @if ($sale->image_rectangle)
                      <div class="{{$sale->image_square && $sale->image_circle ? "col-md-4" : ($sale->image_square || $sale->image_circle ? "col-md-6" : "col-md-12")}}">
                          <div class="product-container" style="padding-bottom: 30px;"> 
                              <div class="product-image-container-detail"> 
                                  <img style="width:100%; height:100%;" src="{{ $sale->image_rectangle}}" alt="">
                              </div>
                              <!-- <h3 class="text-blue">Main Image</h3>   -->
                          </div>
                      </div>
                      @endif
                      @if ($sale->image_square)
                      <div class="{{$sale->image_rectangle && $sale->image_circle ? "col-md-4" : ($sale->image_rectangle || $sale->image_circle ? "col-md-6" : "col-md-12")}}">
                          <div class="product-container" style="padding-bottom: 30px;"> 
                              <div class="product-image-container-detail"> 
                                  <img style="width:100%; height:100%;" src="{{ $sale->image_square}}" alt="">
                              </div>
                              <!-- <h3 class="text-blue">Room Setting</h3>   -->
                          </div>
                      </div>
                      @endif
                      @if ($sale->image_circle)
                      <div class="{{$sale->image_square && $sale->image_rectangle ? "col-md-4" : ($sale->image_square || $sale->image_rectangle ? "col-md-6" : "col-md-12")}}">
                          <div class="product-container" style="padding-bottom: 30px;"> 
                              <div class="product-image-container-detail"> 
                                  <img style="width:100%; height:100%;" src="{{ $sale->image_circle}}" alt="">
                              </div>
                              <span id="products-details" style="position:absolute; top:300px"> </span>
                              <!-- <h3 class="text-blue" >Close-up</h3>   -->
                          </div>
                      </div>
                      @endif
                    </div>
                  </div>


                
                  <div class="row bg-light-gray" style="padding-bottom:1.5%;padding-top: 30px;margin-top: 10px" >
                
                    <div class="col-md-12">

                      <div class="container">
                      <div class="row">
                      <!-- success message --> 
                      @if(Session::has('success-message-add-product'))
                          <div class="col-md-12 col-lg-12  mt-3">
                              <div class="alert alert-success">
                                Product added to your  <a href="/demo-list" style="text-decoration:none">  Demo List </a>
                              </div>
                          </div>
                          @endif
                      <!-- success message --> 

                      <!-- error message --> 
                          @if(Session::has('error-message'))
                          <div class="col-md-12 col-lg-12  mt-3">
                              <div class="alert alert-danger">
                                  {{ Session::get('error-message') }}
                              </div>
                          </div>
                          @endif
                      </div>

                      <h2 class="text-blue" >{{$sale->name}}</h2>  
                      <h4 class="text-gray">{{$sale->code}}</h4>
                      @if ($sale->description)
                          <h4 class="text-blue text-medium" style="margin-top: 20px">Product Description</h4>
                          <p class="text-small">{!!$sale->description!!}</p>
                      @endif
                          <div class="row" style="margin-top: 30px">
                          <div class="col-xs-6  col-sm-4 col-md-4">
                              <h4>Size</h4>
                          </div>
                          <div class="col-xs-6   col-sm-4  col-md-4 ">
                            <h4>Price</h4>
                          </div>
                          <div class="col-sm-4  col-md-4">
                          </div>
                        </div>
                      @foreach($sale->productdetail as $detail)
                        @if($detail->price)
                        <div class="row">
                          <div class="col-xs-6 col-sm-4 col-md-4  size-container">
                            <h4 style="margin-bottom:-5px !important">{{$detail->size}}</h4> 
                            <p style="color:#929EB3;margin-top:5px; margin-left:2%; margin-bottom:0px !important">{{'('.$detail->size_conversion.')'}}</p>
                          </div>
                          <div class="col-xs-6 col-sm-4  col-md-4">
                            <h4>P <span  class="prices">{{$detail->price}}</span></h4>
                          </div>
                          <div class="col-xs-12   col-sm-4 col-md-4">
                          <form action="/product/{{$detail->product_id}}/demo/{{$detail->id}}" method="GET">
                            <button type="submit" class="btn bg-blue text-white btn-add-demo-list">ADD TO DEMO LIST</button>
                          </form>
                          </div>
                        </div>
                        @endif
                        @endforeach
                      </div>
                    </div>
                  </div>

                  
                </div> <!-- /.single-product-details -->


              </div> <!-- /.shop-large-side -->
              <!-- =========================== SHOP SIDEBAR ============================= -->
              
            </div> <!-- /.main-wrapper -->
          </div> <!-- /.container -->
        </div> <!-- /.product-page --> <!-- /.product-page -->
        
    


      <!-- Scroll Top Button -->
      <button class="scroll-top tran3s bg-blue-royal">
    <i class="fa fa-arrow-up" aria-hidden="true"></i>
  </button>

@stop

@section('page-styles')
<style type="text/css">

.btn-add-demo-list { 
  border-radius: 5px; margin-bottom:6.5%
}



@media(min-width: 655px) {
  .size-container{ 
    display:flex; 
    flex-direction:row;
  }
}
.product-preview-images{
  display: flex;
  flex-direction: 'row';
  justify-content: space-between
}
  .shop-page .main-wrapper .shop-sidebar {
    width: 100% !important;
  }

  .shop-page .main-wrapper .col {
    padding:0px !important;
  }
  .shop-page .main-wrapper .shop-sidebar .price-ranger .theme-button {

  }
  .shop-page .main-wrapper .shop-sidebar .price-ranger {
    border: 1px solid transparent !important;
    margin-bottom: 0px !important;
    padding: 18px 20px 20px 20px !important;
  }
  .shop-page .main-wrapper .shop-sidebar .price-ranger .ui-widget-header {
    background: #172A55 !important;
  }
  .mt-10 {
    margin-top: 10px !important;
  }
  .shop-page .shop-page-pagination li a:hover {
    background-color: #172A55 !important;
    border-color:#172A55 !important;
    color: white !important;
  }
  .shop-page .shop-page-pagination li:last-child a {
    background-color: #172A55 !important;
    border-color:#172A55 !important;
    color: white !important;
  }
  .shop-page .main-wrapper .shop-sidebar .price-ranger .ui-slider-handle {
    border: solid !important;
  }



</style>

@stop