<meta property="og:site_name" content="Eurobel | Rugs and Carpet"/>
<meta property="og:image" content="{{asset('frontend/images/eurobel-thumbnail.jpg')}}"/>
<meta name="twitter:image:alt" content="Eurobel | Rugs and Carpet"/>
<meta property="twitter:image:type" content="image/jpeg" />
<meta name="twitter:image" content="{{asset('frontend/images/eurobel-thumbnail.jpg')}}">
<meta name="twitter:card" content="summary" />
<meta property="og:title" content="Eurobel | Rugs and Carpet"/>
<meta property="og:type" content="website" />
<meta property="og:image:type" content="image/jpeg" />
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="author" content="Leonardo Louie Ordonez">
<link rel="apple-touch-icon" sizes="57x57" href="{{asset('assets/img/eurobel/apple-icon-57x57.png')}}">
<link rel="apple-touch-icon" sizes="60x60" href="{{asset('assets/img/eurobel//apple-icon-60x60.png')}}">
<link rel="apple-touch-icon" sizes="72x72" href="{{asset('assets/img/eurobel//apple-icon-72x72.png')}}">
<link rel="apple-touch-icon" sizes="76x76" href="{{asset('assets/img/eurobel//apple-icon-76x76.png')}}">
<link rel="apple-touch-icon" sizes="114x114" href="{{asset('assets/img/eurobel//apple-icon-114x114.png')}}">
<link rel="apple-touch-icon" sizes="120x120" href="{{asset('assets/img/eurobel//apple-icon-120x120.png')}}">
<link rel="apple-touch-icon" sizes="144x144" href="{{asset('assets/img/eurobel//apple-icon-144x144.png')}}">
<link rel="apple-touch-icon" sizes="152x152" href="{{asset('assets/img/eurobel//apple-icon-152x152.png')}}">
<link rel="apple-touch-icon" sizes="180x180" href="{{asset('assets/img/eurobel//apple-icon-180x180.png')}}">
<link rel="icon" type="image/png" sizes="192x192"  href="{{asset('assets/img/eurobel//android-icon-192x192.png')}}">
<link rel="icon" type="image/png" sizes="32x32" href="{{asset('assets/img/eurobel//favicon-32x32.png')}}">
<link rel="icon" type="image/png" sizes="96x96" href="{{asset('assets/img/eurobel//favicon-96x96.png')}}">
<link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/img/eurobel//favicon-16x16.png')}}">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="{{asset('assets/img/eurobel//ms-icon-144x144.png')}}">
<meta name="theme-color" content="#ffffff">
