<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/style.css?v=2.0')}}">
<!-- responsive style sheet -->
<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/responsive.css?v=1.0')}}">
<link href="https://fonts.googleapis.com/css?family=Didact+Gothic&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="{{asset('frontend/vendor/bootstrap/bootstrap.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('frontend/vendor/bootstrap-select/dist/css/bootstrap-select.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('frontend/vendor/revolution/settings.css')}}">

<link rel="stylesheet" type="text/css" href="{{asset('frontend/vendor/revolution/layers.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('frontend/vendor/revolution/navigation.css')}}">

<link rel="stylesheet" type="text/css" href="{{asset('frontend/vendor/fancy-box/jquery.fancybox.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('frontend/vendor/menu/dist/css/slimmenu.css')}}">
{{-- <link rel="stylesheet" type="text/css" href="{{asset('frontend/fonts/font-awesome/css/font-awesome.min.css')}}">
 --}}<link rel="stylesheet" type="text/css" href="{{asset('frontend/fonts/icon/font/flaticon.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('frontend/vendor/owl-carousel/owl.carousel.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('frontend/vendor/owl-carousel/owl.theme.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('frontend/vendor/animate.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('frontend/vendor/hover.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('frontend/vendor/jquery-ui/jquery-ui.min.css')}}">

<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">

<style type="text/css">
h1,h2,h3,h4,h5,h5,p,span,a,button,li,ul {
	font-family: 'Roboto', sans-serif !important;
	letter-spacing: 1px !important;
}

.footer-product-name { 
	margin-bottom: -15px;
}

.header_socials { 
	display: none;
}

@media(min-width:768px) { 
    .header_socials { 
		display: block;
	}
}
@media(min-width:1024px)  {
	.footer-product-name { 
		margin-bottom: 0px;
	}
}


.shope-page-container { 
    margin-top: 10px;
    margin-bottom:0px !important;
  }
  

  @media(min-width:768px) {
    .shope-page-container { 
    margin-top: 40px;
    margin-bottom:0px !important;
    }
    
  }

  
  @media(min-width:1024px) {
    .shope-page-container { 
    margin-top: 70px;
    margin-bottom:0px !important;
    }
  
  }
.banner-container { 
	margin-top: 30px;
}

.banner-container-indicator{ 
 float: left;
 padding-top: 5px;
}


@media(min-width: 375px)  {
		
		.banner-container { 
			margin-top: 1.5%;
			margin-bottom:1.5%;
		}
	
	}



@media(min-width: 768px)  {
	.banner-container-indicator{ 
		 float: left;
 		 padding-top: 5px;
	}
	
  

}

.client-slider .opacity {
    text-align: center;
    padding: 50px 0 50px 0 !important;
}

.text-no-spacing { 
	margin: 0 0 0px !important;
	line-height: 20px;
}

.bg-blue-royal{
        background-color: #263A8D;
    }
.clearfix  > .ul {
margin-bottom: 0px !important;
}
.fb_dialog_advanced {
    bottom: 50pt !important;
}
.subscribe-input::placeholder {
	color: gray !important;
}
.subscribe-input{
	margin-bottom: 3px !important;
}

@media(min-width:768px){
	.subscribe-input{
		font-size: 13px;
	}
}

@media(min-width:1440px){

	.subscribe-input{
		font-size: 15px;
	}
}

@media(min-width: 425px) {
	.header-app-logo { 
	height:50px;
	width: 40% !important;
    }
}

@media(min-width: 445px) {
	.header-app-logo { 
	height:50px;
	width: 40% !important;
    }
}
@media(min-width: 522px) {
	.header-app-logo { 
	height:50px;
	width: 30% !important;
    }
}

@media(min-width: 768px) {
	.header-app-logo { 
	height:50px;
	width: 20% !important;
    }
}

@media(min-width: 1024px) {
	.header-app-logo { 
	height:50px;
	width: 20%;
}
}


.product-container {
	display: flex;
	flex-direction: column;
	align-items: center;
}
.product-image-container {
    -webkit-box-shadow: 8px 11px 15px 6px rgba(202,202,202,0.4);
    -moz-box-shadow: 8px 11px 15px 6px rgba(202,202,202,0.4);
     box-shadow: 8px 11px 15px 6px rgba(202,202,202,0.4);
     width:275px; 
     height: 392.5px;
}



.product-image-container-detail {
	-webkit-box-shadow: 8px 11px 15px 6px rgba(202,202,202,0.4);
	-moz-box-shadow: 8px 11px 15px 6px rgba(202,202,202,0.4);
	box-shadow: 8px 11px 15px 6px rgba(202,202,202,0.4);
	width:300px; 
	height: 417.5px;
}


.product-image{
  height: 100%;
  width: 100%;
}
#btn-demo-list {
	border-radius: 30px; 
	border:1px solid #172A55;
	font-size:16px; 
	padding:2px 25px;
	font-weight:bold;
	background:linear-gradient(to bottom, #ebeff4 5%, #677694 100%);
	color:#172A55;
}



.bg-light-gray {
	background-color: #F8F9FC !important;
}
.input-contact {
	padding: 30px;background-color: rgba(255,255,255,.2); margin-bottom: 20px
}
.input-subscribe {
	padding: 30px;  margin-bottom: 20px
}
.font-semibold {
	font-weight: 500;
}
.shop-page .main-wrapper .shop-large-side {
	width: 100% !important
}
.text-no--decoration {
	text-decoration: none !important;
}
.bg-blue {
background-color: #172A55 !important;
}
.bg-gray {
background-color: #333 !important;
}
.bg-light-blue {
	background-color: #283890 !important;
}
.bg-white {
background-color: white !important;
}
.text-blue {
color: #172A55 !important;
}
.text-gray {
	color: #333 !important;
}

.text-white {
color: white !important;
}
.collapse-button .icon-bar {
	background: #172A55 !important;
}
#mega-menu-holder>ul> .last> a   {
padding: 8px 0 20px 0 !important;
color: #333 !important
}


header.theme-main-header.fixed #mega-menu-holder>ul>li.last>a {
	padding: 8px 0 20px 0 !important;

}
.justify-content-center {
	justify-content:center;
}
header.theme-main-header.fixed #mega-menu-holder>ul>li.active>a {

color: #172A55 !important
}
header.theme-main-header.fixed #mega-menu-holder>ul>li>a:hover {

color: #172A55 !important
}

.flex-column {
	flex-direction: column;
}

#mega-menu-holder>ul>li.active>a {
	color: #172A55 !important;
	/*border-bottom: 2px solid white;*/
	
}
#mega-menu-holder>ul>li>a>span:hover {
	background-color: #172A55 !important;
	color: white;
	border-radius: 5px;
		
	
}

.btn-schedule-demo {
	padding: 20px;padding-left: 30px;padding-right: 30px;margin-top: 15px;
	background-color: #DDDDDC; color: #333;
}
header.theme-main-header.fixed #mega-menu-holder>ul>.last>a {
padding-top: 28px !important;
}

header.theme-main-header.fixed {
	background-color: white !important
}
.text-about-us { 
	font-size: 16px !important;
	line-height: 30px;
	text-align: justify;
}
.text-xxxsmall {
font-size: 0.75rem !important;
}
.text-xxsmall {
font-size: 0.8rem !important;
}
.text-xsmall {
font-size: 15px !important;
}
.text-header-small {
font-size: 1.1rem !important;
}
.text-small {
font-size: 18px !important;
}
.text-medium {
font-size: 22px !important;
}
.text-xmedium {
font-size: 1.7rem !important;
}
.text-large {
font-size: 2rem !important;
}
.text-xlarge {
font-size: 2.3rem !important;
}

/*text-spacing*/
.spacing-1 {
letter-spacing: 1px;
}
.spacing-2 {
letter-spacing: 2px;
}
.spacing-3 {
letter-spacing: 7px;
}
.spacing-4 {
letter-spacing: 10px;
}

/*text height*/
.text-height-1 {
line-height: 25px;
}
.text-height-2 {
line-height: 40px;
}
.d-flex {
	display: flex;
}
.flex-row {
	flex-direction: row;
}
.hermes .tp-bullet.selected {
	width: 18px !important;
    height: 15px !important; 
    border-radius: 10px !important;
    background-color: white;
}
img.nav-logo{

	width: 13%; 
	position: absolute;
	top: -17px;
	margin-bottom:10%;
}

.hermes .tp-bullet {
	width:15px !important;
    height: 15px !important; 
    border-radius: 10px !important;
    background-color: white
}
.collapse-button{
    margin: 5px 0 25px 0 !important;
}
@media(max-width:1199px){
	.text-sm-center {
		text-align: center !important; 
	}
	.text-sm-left {
		text-align: left !important; 
	}
	.text-sm-right {
		text-align: right !important; 
	}
}

@media (max-width: 991px) {
	#mega-menu-holder {
	background: white !important;
	color:#333 !important;
	}
	.w-sm-100 {
		width: 100%
	}
}

@media (max-width: 796px){

	img.nav-logo{
		width: 60%; 
		position: absolute;
		top: 0;
		margin-bottom:10%;
	}
	#btn-demo-list {
		font-size: 12px;
		padding: 2px 10px!important;
		margin-bottom: 5px;
	}
	#input_search{
		padding: 0 !important;
	}
	.collapse-button{
    	padding: 0px 10px !important;
    }
    .theme-main-header{
    	height: 75px !important;
    }
    .nav-logo{
    	top: -12px !important;
    }
}

@media (max-width: 591px) {

	.w-sm-100 {
		width: 100%
	}

}
@media (min-width: 992px) {
	#mega-menu-holder>ul> li> a {
 	padding:10px 0 20px 0 !important;
	}
	header.theme-main-header.fixed #mega-menu-holder>ul>li>a {

	color: #333 !important;
	}
}

.mt-20 {
	margin-top: 20px !important;
}
.mt-15 {
	margin-top: 15px !important;
}
.mt-25 {
	margin-top: 25px !important;
}
.mt-30 {
	margin-top: 30px !important;
}
.mt-35 {
	margin-top: 35px !important;
}
.mt-40 {
	margin-top: 40px !important;
}
.mt-45 {
	margin-top: 45px !important;
}
.mt-50 {
	margin-top: 50px !important;
}

.mb-20 {
	margin-bottom: 20px !important;
}
.mb-15 {
	margin-bottom: 15px !important;
}
.mb-25 {
	margin-bottom: 25px !important;
}
.mb-30 {
	margin-bottom: 30px !important;
}
.mb-35 {
	margin-bottom: 35px !important;
}
.mb-40 {
	margin-bottom: 40px !important;
}
.mb-45 {
	margin-bottom: 45px !important;
}
.mb-50 {
	margin-bottom: 50px !important;
}

.our-service .single-service:hover .text.style-one {
    border-left-color: transparent !important;
}

.our-service .single-service .text.style-one {
     border-left: 1px solid transparent !important; 
   
}
.p-10 {
	padding: 10px;
}
.btn-free--demo {
	padding: 5px;padding-left:15px;padding-right:15px;border-radius: 5px;
}
.icon-social--navbar {
	margin-top: 8px;margin-left: 10px;
}
.btn-download--catalog {
	padding: 20px;
	padding-left: 40px;
	padding-right: 40px;
	border-radius: 5px;
	width:100%
}


.btn-free--demo1 {
   margin-bottom: -50px;
   padding: 20px;
   padding-left: 40px;
   padding-right: 40px;
   border-radius: 5px;
   width:100%
}
.btn-subscribe {
	color:#ffff;
	padding: 20px;
	padding-left: 40px;
	padding-right: 40px;
	border-radius: 5px;
	width:100%
  
}

@media(min-width:990px) { 
	.btn-free--demo1 {
		margin-bottom: 0px;
		width:auto;
	}

	
	.btn-download--catalog {
		width:initial;
	}
	.btn-subscribe {
		padding: 21px;
		padding-left: 40px;
		color:#ffff;
		padding-right: 30px;
		width:auto;
		border-radius: 0px;
	
	}

	.subscribe-input-container { 
		padding-right: 0px !important;
	}
}


@media (max-width: 770px) {
	 .text-sm-center {
		text-align: center !important;
	 }
	 .justify-sm-center {
		justify-content: center !important;
	 }
	 .w-sm-100 {
	 	width: 100%;
	 }
	
}
@media (max-width: 991px) {
	.carpet-image--size  {
	 	height: 370px;width: auto;
	 }
}
@media (min-width: 992px) {
	.carpet-image--size  {
	 	height: 570px;width: auto;
	 }

	 .btn--subscribe-container {
		padding-right: 0px !important;padding-left: 0px !important;
	 }
}


.icon-input {
	position: absolute;top: 15px;right: 30px;
}
.fb-icon-white {
	border-radius: 50%;
	background-color: white;
	padding-left:4px;
	padding-right:4px;
	padding-top: 3px;
	padding-bottom: 0px;
}
.fb-icon-blue {
	border-radius: 50%;
	background-color: #172A55;
	padding-left:6px;
	padding-right:6px;
	padding-top: 5px;
	padding-bottom: 0px;
}

</style>   