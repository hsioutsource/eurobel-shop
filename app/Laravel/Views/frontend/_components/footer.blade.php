<div class="row bg-light-blue text-sm-center">
    <div class="col-lg-6 bg-blue text-center ">
        <div class="row" style="padding: 18px;padding-top: 20px">
            <div class="col-lg-4 text-right text-sm-center">
                <p><i class="fas fa-phone text-white" style="font-size: 30px;margin-top: 16px"></i></p>
            </div>
            <div class="col-lg-6">
                @if($sitesettings )
                @foreach($sitesettings as $sitesetting)
                <div class="row">
                    <div class="col-lg-6 text-white">
                        <p>{{ $sitesetting->contact_number_1}}</p>
                    </div>
                    <div class="col-lg-6 text-white">
                        <p>{{ $sitesetting->contact_number_2}}</p>
                    </div>
                    <div class="col-lg-6 text-white">
                        <p>{{ $sitesetting->contact_number_3}}</p>
                    </div>
                    <div class="col-lg-6 text-white">
                        <p>{{ $sitesetting->contact_number_4}}</p>
                    </div>
                </div>
                @endforeach
                @endif
            </div>
            <div class="col-lg-2"></div>
            
            
        </div>
    </div>
    <div class="col-lg-6 text-center bg-light-blue">
        <div class="row" style="padding: 23px">
            <div class="col-lg-3 text-right text-sm-center">
                @if($sitesettings )
                @foreach($sitesettings as $sitesetting)
                <p><a  href="mailto:{{$sitesetting->email}}"> <i class="fas fa-envelope text-white" style="font-size: 30px;margin-top: 12px;margin-bottom: 10px"></i> </a></p>
                @endforeach
                @endif
            </div>
            <div class="col-lg-6 text-white text-left text-sm-center" style="margin-top: 18px">
                
                @if($sitesettings )
                @foreach($sitesettings as $sitesetting)
                <a  class="text-white" style="text-decoration:none" href="mailto:{{$sitesetting->email}}">{{$sitesetting->email}} </a>
                @endforeach
                @endif
                
            </div>
        </div>
    </div>
</div>

<!-- 
    =============================================
    Footer
    ============================================== 
-->
<footer class="bg-white text-sm-center" style="padding-top: 0px !important;">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-sm-8  footer-logo" style="padding-top: 10px">
                <a href="{{route('frontend.home.index')}}"><img style="height: 140px;width: auto" src="{{asset('frontend/images/Logo-Long-v3.png')}}" alt="Logo"></a>
                
                <ul style="margin-left: 20px">
                    <li class="text-blue text-small font-weight-bold">Follow Us
                        
                        
                        @if($sitesettings )
                        @foreach($sitesettings as $sitesetting)
                        <a href="{{$sitesetting->facebook_link}}" class="tran3s" style="margin-left: 10px;" target="_blank">
                            <span>
                                <i class="fab fa-facebook-f text-white fb-icon-blue" aria-hidden="true"></i>
                            </span>
                        </a>
                        <a href="{{$sitesetting->instagram_link}}" class="tran3s" style="margin-left: 10px;" target="_blank">
                            <i class="fab fa-instagram text-blue" aria-hidden="true"></i>
                        </a>
                        @endforeach
                        @endif
                    </li>
                    
                </ul>
            </div>
            <!-- /.footer-logo -->
            
            <div class="col-md-3 col-sm-4 footer-list" style="margin-top:50px">
                <ul>
                    <li><a href="{{route('frontend.showroom.index')}}" class="tran3s text-gray font-semibold  text-small text-no--decoration">Our Showrooms</a></li>
                    <li><a href="{{route('frontend.contact.index')}}" class="tran3s text-gray text-small text-no--decoration">Contact</a></li>
                    <li><a href="{{$sitesetting->privacy_link}}" class="tran3s text-gray text-small text-no--decoration" target="_blank">Privacy Policy</a></li>
                    <li><a  href="{{$sitesetting->terms_of_service_link}}" class="tran3s text-gray text-small text-no--decoration" target="_blank">Terms of Service</a></li>
                    
                </ul>
            </div>
            <!-- /.footer-list -->

            <div class="col-md-4 col-sm-12"> 
            @foreach($sitesettings  as $sitesetting)
                <div class="row">
                    <div class="col-md-6 col-lg-6 col-xs-12" style="margin-top:50px">
                            <a href="/demo-list?#demo"  class="btn bg-blue text-white btn-free--demo1">FREE DEMO</a>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xs-12" style="margin-top:50px">
                        <a href="{{$sitesetting->catalog_link}}" class="btn bg-blue text-white btn-download--catalog" target="_blank">DOWNLOAD CATALOG</a>
                    </div>
                </div>

                <div class="row" style="margin-top:2%" id="subscribe">
                                  <!-- success message --> 
                    @if(Session::has('subs-error'))
                    <div class="col-md-12 col-lg-12  mt-3">
                        <div class="alert alert-danger">
                            @foreach (Session::get('subs-error') as $error)
                            <p>{{ $error }}</p>
                            @endforeach
                        </div>
                    </div>
                    @endif
                    
                    <!-- success message --> 
                    @if(Session::has('success-message-subs'))
                    <div class="col-md-12 col-lg-12  mt-3">
                        <div class="alert alert-success">
                            {{ Session::get('success-message-subs') }}
                        </div>
                    </div>
                    @endif
                    <form action="{{route('frontend.subscribe.save')}}" method="POST"> 
                        @csrf
                    <div class="col-md-9 subscribe-input-container">
                    <input style="border-radius: 0px" type="email" placeholder="Enter your email address here." class="form-control input-contact subscribe-input" name="email" value="{{old('email')}}" required>
                    </div>
                    <div class="col-md-1 btn--subscribe-container">
                      <input type="submit" class="btn bg-blue btn-subscribe text-white" value="SUBSCRIBE">
                    </div>
                    </form>
                </div>
                @endforeach
            </div>
    
            <!-- /.newsletter -->
        </div>
        <!-- /.top-footer -->
    </div>
    <!-- /.container -->
    
    <div class="bottom-footer bg-blue" style="margin-top: 40px !important">
        <div class="container">
            <p class="float-left footer-product-name">Copyright &copy; 2020 Eurobel Rugs + Carpets</p>
            <ul class="float-right">
                <li>All Rights Reserved</li>
                
            </ul>
        </div>
        <!-- /.container -->
    </div>
    <!-- /.bottom-footer -->
</footer>