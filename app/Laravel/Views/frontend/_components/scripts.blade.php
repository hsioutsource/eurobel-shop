<script type="text/javascript" src="{{asset('frontend/vendor/jquery.2.2.3.min.js')}}"></script>

<!-- Bootstrap JS -->
<script type="text/javascript" src="{{asset('frontend/vendor/bootstrap/bootstrap.min.js')}}"></script>
<!-- Bootstrap Select JS -->
<script type="text/javascript" src="{{asset('frontend/vendor/bootstrap-select/dist/js/bootstrap-select.js')}}"></script>

<!-- revolution -->
<script src="{{asset('frontend/vendor/revolution/jquery.themepunch.tools.min.js')}}"></script>
<script src="{{asset('frontend/vendor/revolution/jquery.themepunch.revolution.min.js')}}"></script>
<script type="text/javascript" src="{{asset('frontend/vendor/revolution/revolution.extension.slideanims.min.js')}}"></script>
<script type="text/javascript" src="{{asset('frontend/vendor/revolution/revolution.extension.layeranimation.min.js')}}"></script>
<script type="text/javascript" src="{{asset('frontend/vendor/revolution/revolution.extension.navigation.min.js')}}"></script>
<script type="text/javascript" src="{{asset('frontend/vendor/revolution/revolution.extension.kenburn.min.js')}}"></script>
<!-- menu  -->
<script type="text/javascript" src="{{asset('frontend/vendor/menu/src/js/jquery.slimmenu.js')}}"></script>
<script type="text/javascript" src="{{asset('frontend/vendor/jquery.easing.1.3.js')}}"></script>
<!-- fancy box -->
<script type="text/javascript" src="{{asset('frontend/vendor/fancy-box/jquery.fancybox.pack.js')}}"></script>
<!-- owl.carousel -->
<script type="text/javascript" src="{{asset('frontend/vendor/owl-carousel/owl.carousel.min.js')}}"></script>
<!-- js count to -->
<script type="text/javascript" src="{{asset('frontend/vendor/jquery.appear.js')}}"></script>
<script type="text/javascript" src="{{asset('frontend/vendor/jquery.countTo.js')}}"></script>
<!-- MixitUp -->
<script type="text/javascript" src="{{asset('frontend/vendor/jquery.mixitup.min.js')}}"></script>

<script src="{{asset('frontend/vendor/jquery-ui/jquery-ui.min.js')}}"></script>

<!-- Theme js -->
<script type="text/javascript" src="{{asset('frontend/js/theme.js')}}"></script>
<!-- This site is converting visitors into subscribers and customers with Rocketbots - https://rocketbots.io -->
<script src="https://app.rocketbots.io/facebook/chat/plugin/24101/1670531153210254" async></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-101283539-10"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-101283539-10');
</script>

<!-- https://rocketbots.io/ -->
<script type="text/javascript">

function numberWithCommas(number) {
    var parts = number.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}


$(document).ready(function() {
  $(".prices").each(function() {
    var num = $(this).text();
    var commaNum = numberWithCommas(num);
    $(this).text(commaNum);
  });

  


  function loadPrices() {
  $(".input-prices").click(function() {

  var value = $(this).val()
  alert(value)
  var commaNum = numberWithCommas(value);
  $(this).val(commaNum)
  } )
  }
  
});


	
</script>

<script>
  window.addEventListener('load', function() {
var str=setInterval(function(){
if(jQuery('.alert-success:contains(Your preferred demo schedule has been successfully sent.)').is(':visible') && window.location.pathname.indexOf('/demo-list')!=-1){
gtag('event', 'conversion', {'send_to': 'AW-648900630/OFg7CP6lxdEBEJbgtbUC'});
clearInterval(str)
}
},1000);
  })
</script>