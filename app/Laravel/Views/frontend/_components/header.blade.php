<header class="theme-main-header z-index--header bg-white" style="z-index: 1000000;height: 90px;">
    <div class="bg-blue">
        <div class="container" style="padding-top: 5px">
            <div class="row">
                <div class="col-sm-6 col-xs-12 text-sm-center header_socials">
                    <div class="d-flex flex-row justify-sm-center">
                        <p class="text-white">Follow Us</p>
                        
                        @if($sitesettings )
                        @foreach($sitesettings as $sitesetting)
                        <a href="{{$sitesetting->facebook_link}}" target="_blank"> 
                            <i class="fb-icon-white fab fa-facebook-f text-blue ml-3 mt-2 icon-social--navbar"></i>
                        </a>
                        <a href="{{$sitesetting->instagram_link}}" target="_blank"> 
                            <i class="fab fa-instagram text-white ml-3 mt-2 icon-social--navbar"></i>
                        </a>
                        @endforeach
                        @endif
                    </div> 
                </div>
                <div class="col-sm-6 col-xs-12 text-right text-sm-center" style="margin: 0px !important;">
                    <form action="/product/search" method="GET">
                            <i class="fas fa-search text-white"></i>
                            <input type="text" style="background-color: transparent;border: 1px solid transparent;color: white;padding: 5px" placeholder="Search Eurobel" name="search_text" id="input_search">
                        <a id="btn-demo-list" href="{{route('frontend.demo.index')}}" class="btn bg-light-blue" ><b> Demo List </b></a>
                    </form>

                </div>
            </div>
        </div>
    </div>
    
    <!-- ======================= Theme Main Menu ====================== -->
    <div class="container">
        <div class="theme-main-menu clearfix">
            <div class="logo float-left header-app-logo">
                     <a href="{{route('frontend.home.index')}}"><img  src="{{asset('frontend/images/eurobel-logo-updated.png')}}" class="nav-logo"></a>
            </div>
            <!-- ============== Menu Warpper ================ -->
            <nav id="mega-menu-holder" class="float-right">
                <ul class="clearfix " style="margin-bottom:0px !important">
                    <li class="active"><a href="{{route('frontend.home.index')}}" class="text-gray"><span class="p-10">Home</span></a></li>
                    <li class="dropdown">
                        <a href="javascript:void(0)" class="text-gray" data-toggle="dropdown">
                            <span class="p-10">Products</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="{{route('frontend.product.index')}}" class="dropdown-item"><span class="p-10">Eurobel Rugs + Carpets</span></a></li>
                            <li><a href="{{route('frontend.sales.index')}}" class="dropdown-item"><span class="p-10">Carpetworld Rugs & Carpets</span></a></li>
                        </ul>
                    </li>                        
                    <li><a href="{{route('frontend.showroom.index')}}" class="text-gray"><span class="p-10">Showrooms</span></a></li>
                    <li><a href="{{route('frontend.about.index')}}" class="text-gray"><span class="p-10">About Us</span></a></li>
                    <li><a href="{{route('frontend.contact.index')}}" class="text-gray"><span class="p-10">Contact</span></a></li>
                    <li class="last"><a href="/demo-list?#demo" class="text-gray"><span class="btn bg-blue text-white btn-free--demo">FREE DEMO</span></a></li>
                </ul>
            </nav>
            <!-- /#mega-menu-holder -->
        </div>
        <!-- /.theme-main-menu -->
    </div>
    <!-- /.container -->
</header>
