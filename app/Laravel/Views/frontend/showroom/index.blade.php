@extends('frontend._layouts.main')
@section('page-dynamic-metas')
<title>Eurobel - Showrooms</title>
<meta property="og:url"  content="{{env('APP_URL').'showroom'}}" />
<meta property="og:description" content="AMENITY + STYLE + WARMTH. Serving you as the largest retailer of premium rugs and carpets in the Philippines since 1995. Book your FREE DEMO today! @eurobel_rugs, @eurobelrugscarpets, @eurobelrugsandcarpets@gmail.com" />

<meta name="robots" content="index, follow">

<meta name="description" content="AMENITY + STYLE + WARMTH. Serving you as the largest retailer of premium rugs and carpets in the Philippines since 1995. Book your FREE DEMO today! @eurobel_rugs, @eurobelrugscarpets, @eurobelrugsandcarpets@gmail.com">
@stop

@section('content')
    
<!-- ===================================================
  Loading Transition
==================================================== -->
<div id="loader-wrapper">
  <div id="loader"></div>
</div>


<!--
=============================================
  Theme Header
==============================================
-->
<!--
      =============================================
        Theme inner Banner
      ==============================================
      -->
      <div class="inner-banner">
        <div class="opacity">
          <div class="container banner-container">
            <h2>SHOWROOMS</h2>
          
            <div class="clearfix">
              <ul class="banner-container-indicator">
                <li><a href="{{route('frontend.home.index')}}" class="tran3s">Home</a></li>
                <li>/</li>
                <li><a href="#" class="tran3s">Showroom</a></li>
              </ul>
            </div>
          </div> <!-- /.container -->
        </div> <!-- /.opacity -->
      </div> <!-- /.inner-banner -->




      <!-- 
      =============================================
        Shop Page 
      ============================================== 

      -->
      <div class="shop-page bg-light-gray" style="margin-bottom:0px !important;padding:10px">
        <div class="container">
          <div class="main-wrapper">
            <div class="col float-right shop-details shop-large-side">
              <div class="single-product-details clearfix">
                <div class="row bg-light-gray showroom-container">

                @if(!$showrooms->isEmpty())
                @foreach($showrooms as $showroom)
                  <div class="col-md-6 mb-20">
                    <div class="row">
                      <div class="col-md-12">
                        <h2 class="text-blue">{{$showroom->location_name}}</h2>
                      </div>
                      <div class="col-md-6 col-lg-5 mt-10">
                        <div class="show-room-image-container"> 
                            <img src="{{$showroom->image}}" alt="" class="show-room-image">
                        </div>
                      </div>
                      <div class="col-md-6 col-lg-7 mt-30">
                        <div class="d-flex flex-row">
                          <i class="text-blue fas fa-map-marker-alt fa-2x" style="margin-right: 20px;margin-top: 12px"></i>
                          <p class="text-xsmall text-gray">{{$showroom->address}}</p>
                        </div>
                         <div class="d-flex flex-row">
                          <i class="text-blue fas fa-phone-square fa-2x" style="margin-right: 20px;margin-top: 12px"></i>
                          <p class="text-xsmall mt-10 text-gray">{{$showroom->contact_number}} </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  @endforeach
                  @else
                  <h3 style="text-align:center">No Showrooms Available</h3>
                  <p style="text-align:center">Kindly contact the website administrator</p>
                  @endif
                </div>
                
              </div> <!-- /.single-product-details -->


            </div> <!-- /.shop-large-side -->
            <!-- =========================== SHOP SIDEBAR ============================= -->
            
          </div> <!-- /.main-wrapper -->
        </div> <!-- /.container -->
      </div> <!-- /.shop-page --> <!-- /.shop-page -->
      



    <!-- Scroll Top Button -->
    <button class="scroll-top tran3s bg-blue-royal">
  <i class="fa fa-arrow-up" aria-hidden="true"></i>
</button>


@stop

@section('page-styles')
<style type="text/css">


     .show-room-image {
         width: 100%;
         height: 100%;
         object-fit: cover;
         object-position: center;
     }

     .show-room-image-container{
          width: 220px;
          height: 220px;
        }


     @media(min-width:768px) { 
         .show-room-image-container{
          width: 220px;
          height: 220px;
        }

     }

     @media(min-width:992px) { 
         .show-room-image-container {
          width: 220px;
          height: 220px;
        }

     }


.showroom-container {
  padding-bottom:0px;
  padding-top: 0px;
  margin-top: 0px;
  padding-left:5%;
}

@media(min-width:768px) { 
  
  .showroom-container {
    padding-bottom:70px;
    padding-top: 30px;
    margin-top: 50px;
  }

}

.thumbnail-img {
  width: 220px;
  height: 220px;

}
.thumbnail-img img {
  height: 100%;
  width: 100%;
}
.thumbnail-img img.portrait {
  height: 100%;
  width: 100%;
}

/* .thumbnail img.portrait {
  width: 100%;
  height: auto;
} */
  .shop-page .main-wrapper .shop-sidebar {
    width: 100% !important;
  }

  .shop-page .main-wrapper .col {
    padding:0px !important;
  }
  .shop-page .main-wrapper .shop-sidebar .price-ranger .theme-button {

  }
  .shop-page .main-wrapper .shop-sidebar .price-ranger {
    border: 1px solid transparent !important;
    margin-bottom: 0px !important;
    padding: 18px 20px 20px 20px !important;
  }
  .shop-page .main-wrapper .shop-sidebar .price-ranger .ui-widget-header {
    background: #172A55 !important;
  }
  .mt-10 {
    margin-top: 10px !important;
  }
  .shop-page .shop-page-pagination li a:hover {
    background-color: #172A55 !important;
    border-color:#172A55 !important;
    color: white !important;
  }
  .shop-page .shop-page-pagination li:last-child a {
    background-color: #172A55 !important;
    border-color:#172A55 !important;
    color: white !important;
  }
  .shop-page .main-wrapper .shop-sidebar .price-ranger .ui-slider-handle {
    border: solid !important;
  }

</style>

@stop