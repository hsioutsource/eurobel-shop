@extends('frontend._layouts.main') 
@section('page-dynamic-metas')
<title>Eurobel - Products Search</title>
<meta property="og:url"  content="{{env('APP_URL').'product/search'}}" />
<meta property="og:description" content="AMENITY + STYLE + WARMTH. Serving you as the largest retailer of premium rugs and carpets in the Philippines since 1995. Book your FREE DEMO today! @eurobel_rugs, @eurobelrugscarpets, @eurobelrugsandcarpets@gmail.com" />
@stop

@section('content')

<!-- ===================================================
  Loading Transition
==================================================== -->
<div id="loader-wrapper">
    <div id="loader"></div>
</div>

<!--
=============================================
  Theme Header
==============================================
-->
<!--
      =============================================
        Theme inner Banner
      ==============================================
      -->
<div class="inner-banner">
    <div class="opacity">
        <div class="container banner-container">
            <h2>PRODUCTS SEARCH</h2>

            <div class="clearfix">
                <ul class="banner-container-indicator">
                    <li><a href="{{route('frontend.home.index')}}" class="tran3s">Home</a></li>
                    <li>/</li>
                    <li>Products</li>
                    <li>/</li>
                    <li>Search</li>
                </ul>
            </div>
        </div>
        <!-- /.container -->
    </div>
    <!-- /.opacity -->
</div>
<!-- /.inner-banner -->

<!-- 
      =============================================
        Shop Page 
      ============================================== 
      -->
<div class="shop-page shope-page-container">
    <div class="container">
        <div class="main-wrapper"  style="padding-left: 20px;padding-right: 20px;">

            <form action="/product" action="GET">
                <div class="col float-right all-product-wrapper shop-large-side">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col float-left shop-sidebar">

                              
                            </div>
                            <!-- /.shop-sidebar -->
                        </div>
                    </div>
                    <div class="row">

                        @if(!$products->isEmpty())
                        @foreach ($products as $product)
                           <!-- /.single-product -->
                           <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                            <div class="single-item text-center">
                                <div class="product-container"> 
                                <a href="{{route('frontend.product.show',[$product->name,$product->code])}}">
                                <div class="product-image-container"> 
                                    <img src="{{$product->image_rectangle}}" class="product-image"  alt=""> 
                                </div>
                                </a>
                                <h5><a href="{{route('frontend.product.show',[$product->name,$product->code])}}" class="text-medium text-no--decoration tran3s text-hover-blue"> {{ $product->name }}</a></h5>
                                <div class="clearfix text-center">
                                    <a href="{{route('frontend.product.show',[$product->name,$product->code])}}" class="text-no--decoration tran3s text-blue">
                                    <p class="text-small text-gray text-no-spacing"> {{ $product->code }}</p>
                                    </a>
                                </div>
                                </div>
                            </div>
                            </div>
                             <!-- /.single-product -->
                        @endforeach

                        @else
                        <div class="col-md-12 col-xs-12 col-lg-12" style="margin-bottom:5%">
                             <h1 class="product-search-text"> Product entered: {{count($products)}} items found for "{{$search_text}}". Please try again. </h1>
                        </div>
                        @endif

                    </div>
                    <div class="row">
                        <!-- /.col- -->
                        <div class="col-md-12 d-flex justify-content-center">
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.shop-large-side -->
                <!-- =========================== SHOP SIDEBAR ============================= -->
            </form> 
        </div>
    </div>
    <!-- /.container -->
</div>
<!-- /.shop-page -->

<!-- Scroll Top Button -->
<button class="scroll-top tran3s p-bg-color">
    <i class="fa fa-arrow-up" aria-hidden="true"></i>
</button>

@stop @section('page-styles')
<style type="text/css">


    .product-search-text{
     font-size: 24px;
    }

    @media(min-width:768px) { 
        .product-search-text{
        font-size: 36px;
        }

    }


    .form-control:focus {
        border-color: transparent !important; 
        box-shadow: 0px !important;
    }
    .shop-page .main-wrapper .shop-sidebar {
        width: 100% !important;
    }
    
    .shop-page .main-wrapper .col {
        padding: 0px !important;
    }
    
    .shop-page .main-wrapper .shop-sidebar .price-ranger .theme-button {}
    
    .shop-page .main-wrapper .shop-sidebar .price-ranger {
        border: 1px solid transparent !important;
        margin-bottom: 0px !important;
        padding: 18px 20px 20px 20px !important;
    }
    
    .shop-page .main-wrapper .shop-sidebar .price-ranger .ui-widget-header {
        background: #172A55 !important;
    }
    
    .mt-10 {
        margin-top: 10px !important;
    }
    
    .shop-page .shop-page-pagination li a:hover {
        background-color: #172A55 !important;
        border-color: #172A55 !important;
        color: white !important;
    }
    
    .shop-page .shop-page-pagination li:last-child a {
        background-color: #172A55 !important;
        border-color: #172A55 !important;
        color: white !important;
    }
    
    .shop-page .main-wrapper .shop-sidebar .price-ranger .ui-slider-handle {
        border: solid !important;
    }

    .pagination { 

     display: flex;
    }

    .pagination .page-item  a {
    line-height: 31px;
    border: 1px solid #f5f5f5;
    border-radius: 3px;
    font-weight: 600;
    color: rgba(0,0,0,0.3);
    font-size: 20px;
    padding: 5 11px;
    margin: 0 1%;

    }

    .pagination > .disabled > span, 
    .pagination > .disabled > span:hover, 
    .pagination > .disabled > span:focus, 
    .pagination > .disabled > a, .pagination > 
    .disabled > a:hover, .pagination > 
    .disabled > a:focus {
    
    line-height: 31px;
    border: 1px solid #f5f5f5;
    border-radius: 3px;
    font-weight: 600;
    color: rgba(0,0,0,0.3);
    font-size: 20px;
    padding: 5 11px;
    margin: 0 1%;
    }

    

    .pagination > 
    .active > a, 
    .pagination > 
    .active > span,
    .pagination > 
    .active > a:hover, 
    .pagination > 
    .active > span:hover, 
    .pagination > 
    .active > a:focus, 
    .pagination > 
    .active > span:focus {
    padding: 5 11px !important;
    background-color: #172A55 !important;
    border-color: #172A55 !important;
    color: white !important;
    font-weight: 600;
    border-radius: 3px;
    line-height: 31px;
    font-size: 20px;
    margin: 0 1%;

    }
</style>

@stop