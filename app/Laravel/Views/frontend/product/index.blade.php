@extends('frontend._layouts.main') 
@section('page-dynamic-metas')
<title>Eurobel - Products</title>
<meta property="og:url"  content="{{env('APP_URL').'product'}}" />
<meta property="og:description" content="AMENITY + STYLE + WARMTH. Serving you as the largest retailer of premium rugs and carpets in the Philippines since 1995. Book your FREE DEMO today! @eurobel_rugs, @eurobelrugscarpets, @eurobelrugsandcarpets@gmail.com" />


<meta name="robots" content="index, follow">

<meta name="description" content="AMENITY + STYLE + WARMTH. Serving you as the largest retailer of premium rugs and carpets in the Philippines since 1995. Book your FREE DEMO today! @eurobel_rugs, @eurobelrugscarpets, @eurobelrugsandcarpets@gmail.com">
@stop

@section('content')

<!-- ===================================================
  Loading Transition
==================================================== -->
<div id="loader-wrapper">
    <div id="loader"></div>
</div>

<!--
=============================================
  Theme Header
==============================================
-->
<!--
      =============================================
        Theme inner Banner
      ==============================================
      -->
<div class="inner-banner">
    <div class="opacity">
        <div class="container banner-container">
            <h2>PRODUCTS</h2>

            <div class="clearfix">
                <ul class="banner-container-indicator">
                    <li><a href="{{route('frontend.home.index')}}" class="tran3s">Home</a></li>
                    <li>/</li>
                    <li><a href="#" class="tran3s">Products</a></li>

                </ul>
            </div>
        </div>
        <!-- /.container -->
    </div>
    <!-- /.opacity -->
</div>
<!-- /.inner-banner -->

<!-- 
      =============================================
        Shop Page 
      ============================================== 
      -->
<div class="shop-page shope-page-container">
    <div class="container">
        <div class="main-wrapper"  style="padding-left: 20px;padding-right: 20px;">

            <form action="/product" action="GET" onsubmit="SetData();">
                <div class="col float-right all-product-wrapper shop-large-side">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col float-left shop-sidebar">

                                <div class="sidebar-price">

                                    <div class="price-ranger" style="padding-left: 0px !important;padding-right: 0px !important">
                                        <div class="ranger-min-max-block row clearfix">
                                            <div class="col-md-3 col-sm-12 col-xs-12 mt-10">
                                                <h3 class="text-blue">Product Catalogue</h3>
                                            </div>

                                            <div class="col-md-2 col-sm-12 col-xs-12 mt-10">
                                                <div class="d-flex flex-row">
                                                    <span class="text-nowrap" style="margin-top: 5px; margin-right: 5px;">Sort by </span>
                                                    <select class="form-control outline-0" name="sort">
                                                        @if($sort == "desc")
                                                        <option  value="desc" selected="desc">Z - A </option>
                                                        <option value="asc" >A - Z</option>
                                                        @elseif($sort == "asc")
                                                        <option value="asc" selected="asc">A - Z</option>
                                                        <option  value="desc">Z - A </option>
                                                        @else
                                                        <option  value="asc" >A - Z</option>
                                                        <option  value="desc" >Z - A </option>
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-sm-4 col-xs-12 mt-10">
                                                <div class="d-flex flex-row">
                                                    <span class="text-nowrap" style="margin-top: 5px; margin-right: 10px;">Price </span>
                                                    <input type="text" class="min text-right" value="" id="min_price" name="min_price" readonly>
                                                    <input type="hidden" value="{{$product_min_price}}" id="product_min_price" name="product_min_price" >
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-sm-4 col-xs-12 mt-10" style="margin-bottom: 10px">
                                                <div id="slider-range" style="margin-top: 15px !important;"></div>
                                            </div>
                                            <div class="col-md-2 col-sm-4 col-xs-12 mt-10">
                                                <input type="text" class="max text-right" value="" id="max_price" name="max_price" readonly>
                                                <input type="hidden" value="{{$product_max_price}}" id="product_max_price" name="product_max_price" >
                                                <input type="hidden" value="{{$default_max_price}}" id="default_max_price" name="default_max_price" >
                                                
                                            </div>
                                            <div class="col-md-1 col-sm-12 col-xs-12 text-sm-center">
                                                <button type="submit" style="border-radius: 5px;width: 100%;" class="bg-blue btn text-xsmall text-white w-sm-100 mt-10">APPLY</button>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- /price-ranger -->
                                </div>
                                <!-- /.sidebar-price -->

                            </div>
                            <!-- /.shop-sidebar -->
                        </div>
                    </div>
                    <div class="products_list">

                        @if(!$products->isEmpty())
                        @foreach ($products as $product)
                           <!-- /.single-product -->
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3" style="height:500px; margin-bottom:38px;">
                                <div class="single-item text-center">
                                    <div class="product-container"> 
                                        <a href="{{route('frontend.product.show',[$product->name,$product->code])}}">
                                        <div class="product-image-container"> 
                                            <img src="{{$product->image_rectangle}}" class="product-image"  alt=""> 
                                        </div>
                                        </a>
                                        <div class="clearfix text-center">
                                            <h5><a href="{{route('frontend.product.show',[$product->name,$product->code])}}" class="text-medium text-no--decoration tran3s text-hover-blue"> {{ $product->name }}</a></h5>
                                            <a href="{{route('frontend.product.show',[$product->name,$product->code])}}" class="text-no--decoration tran3s text-blue">
                                            <p class="text-small text-gray text-no-spacing"> {{ $product->code }}</p>
                                            @if ($product->third_line)
                                                <p class="text-small text-no-spacing" style="line-height: 30px; color:{{$product->text_color ? $product->text_color : "#000000"}};border:1px solid black 0.3">
                                                    {{$product->third_line}}
                                                </p>
                                            @endif
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                        @else
                            <div class="col-md-12">
                            <h1> Oops no products found try it again.. </h1>
                            </div>
                        @endif

                    </div>
                    <div class="row">
                        <!-- /.col- -->
                        <div class="col-md-12 d-flex justify-content-center">
                            {{ $products->appends(request()->except('page'))->links() }}
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.shop-large-side -->
                <!-- =========================== SHOP SIDEBAR ============================= -->
            </form> 
        </div>
    </div>
    <!-- /.container -->
</div>
<!-- /.shop-page -->

<!-- Scroll Top Button -->
<button class="scroll-top tran3s bg-blue-royal">
    <i class="fa fa-arrow-up" aria-hidden="true"></i>
</button>

@stop @section('page-styles')
<style type="text/css">
    .form-control:focus {
        border-color: transparent !important; 
        box-shadow: 0px !important;
    }
    .shop-page .main-wrapper .shop-sidebar {
        width: 100% !important;
    }
    
    .shop-page .main-wrapper .col {
        padding: 0px !important;
    }
    
    .shop-page .main-wrapper .shop-sidebar .price-ranger .theme-button {}
    
    .shop-page .main-wrapper .shop-sidebar .price-ranger {
        border: 1px solid transparent !important;
        margin-bottom: 0px !important;
        padding: 18px 20px 20px 20px !important;
    }
    
    .shop-page .main-wrapper .shop-sidebar .price-ranger .ui-widget-header {
        background: #172A55 !important;
    }
    
    .mt-10 {
        margin-top: 10px !important;
    }
    
    .shop-page .shop-page-pagination li a:hover {
        background-color: #172A55 !important;
        border-color: #172A55 !important;
        color: white !important;
    }
    
    .shop-page .shop-page-pagination li:last-child a {
        background-color: #172A55 !important;
        border-color: #172A55 !important;
        color: white !important;
    }
    
    .shop-page .main-wrapper .shop-sidebar .price-ranger .ui-slider-handle {
        border: solid !important;
    }

    .pagination { 
     display: flex;
    }

    .pagination .page-item  a {
    line-height: 31px;
    border: 1px solid #C7CBE0;
    border-radius: 3px;
    color: rgba(0,0,0,0.3);
    padding: 5% 12px !important;
    margin: 0 10%;

    }

    .pagination > .disabled > span, 
    .pagination > .disabled > span:hover, 
    .pagination > .disabled > span:focus, 
    .pagination > .disabled > a, .pagination > 
    .disabled > a:hover, .pagination > 
    .disabled > a:focus {
    
    line-height: 31px;
    border: 1px solid #C7CBE0;
    border-radius: 3px;
    font-weight: 600;
    color: #C7CBE0;
    padding: 5% 12px !important;
    margin: 0 10%;
    }

    .pagination > li:last-child > a, .pagination > li:last-child > span {
        padding: 5% 12px !important;
         margin: 0 20%;
  
    }
    

    .pagination > 
    .active > a, 
    .pagination > 
    .active > span,
    .pagination > 
    .active > a:hover, 
    .pagination > 
    .active > span:hover, 
    .pagination > 
    .active > a:focus, 
    .pagination > 
    .active > span:focus {
    background-color: #47569C !important;
    border-color: #C7CBE0 !important;
    color: white !important;
    font-weight: 600;
    border-radius: 3px;
    line-height: 31px;
    padding: 5% 12px !important;
    margin: 0 10%;
    }
</style>

@stop