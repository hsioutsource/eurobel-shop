@extends('frontend._layouts.main')

@section('page-dynamic-metas')
<title>Eurobel - Demo List</title>
<meta property="og:url"  content="{{env('APP_URL').'demo-list'}}" />
<meta property="og:description" content="AMENITY + STYLE + WARMTH. Serving you as the largest retailer of premium rugs and carpets in the Philippines since 1995. Book your FREE DEMO today! @eurobel_rugs, @eurobelrugscarpets, @eurobelrugsandcarpets@gmail.com" />
@stop



@section('content')


<div class="inner-banner">
    <div class="opacity">
        <div class="container banner-container">
            <h2>DEMO LIST</h2>

            <div class="clearfix">
                <ul class="banner-container-indicator">
                    <li><a href="{{route('frontend.home.index')}}" class="tran3s">Home</a></li>
                    <li>/</li>
                    <li><a href="#" class="tran3s">Demo List</a></li>

                </ul>
            </div>
        </div>
        <!-- /.container -->
    </div>
    <!-- /.opacity -->
</div>

<!--
=============================================
  Our Service
==============================================
-->
<div class="bg-light-gray">
    <div class="our-service">
        <div class="container">
            <div class="row">
              <!-- success message --> 
              @if(Session::has('success-remove-message'))
                <div class="col-md-12 col-lg-12  mt-3">
                    <div class="alert alert-success">
                        {{ Session::get('success-remove-message') }}
                    </div>
                </div>
                @endif
                <!-- success message --> 
                @if($products)
                    @foreach ($products as $demo)
                        <div class="col-md-3 col-sm-6 col-xs-6">
                               <!-- /.single-service -->
                            <div class="single-service">
                                <form action="/demo-list/{{$demo->id}}" method="GET">
                                <button type="submit"><i class="fas fa-times-circle fa-2x bg-white text-danger icon-close"></i></button>
                                </form>
                                <div class="image">
                                @if($demo->product->image_rectangle)
                                <a target="_blank" href="/product/{{$demo->product->id}}/show">
                                     <img class="img-product-size" src="{{$demo->product->image_rectangle}}" alt="{{$demo->product->name}}">
                                </a>
                                @endif
                                </div>
                                <div class="tran3s text-center" style="padding:5%">
                                <p class="text-medium"><a target="_blank" href="/product/{{$demo->product->id}}/show" class="text-no--decoration tran3s text-blue">{{$demo->product->name}}</a></p>
                                <a href="/product/{{$demo->product->id}}/show" class="text-no--decoration tran3s text-blue">
                                    <span class="text-large text-gray">{{$demo->product->code}}</span>
                                    <p class="text-xs text-gray text-no-spacing ">Size: {{$demo->size}}</p>
                                    <p class="text-xs text-gray text-no-spacing ">Price: P<span  class="prices">{{$demo->price}}</span></p>
                                </a>
                                </div>
                                <!-- /.text -->
                            </div>
                            <!-- /.single-service -->
                        </div>
                    @endforeach
                    @else
                   <div class="col-md-12">
                        <h2>Demo list is empty.</h2>
                        <p>Please select items for demo from the product page.</p>
                    </div>
                    @endif
   
                    <!-- /.item -->
                <!-- /.service-sldier -->
            
            </div>
            <div class="container" id="demo"> 
                <div class="col-md-12" style="height:40px" > 
                </div>
            </div>
            <!-- /.row -->
        </div>
   
        
        <!-- /.container -->
     
    </div>
    <!-- /.our-service -->
    
</div>

<div class="client-slider " style="margin: 0px !important" >  
    <div class="opacity opacityOne">
        <div class="container text-white">
            <h3 class=" text-white" style="margin-bottom: 30px">SCHEDULE FOR A FREE DEMO</h3>
            <div class="row" >
                    <!-- error message --> 
                @if ($errors->any())
                <div class="col-md-12 col-lg-12 mt-3">
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                @endif
                <!-- error message --> 
                
                <!-- success message --> 
                @if(Session::has('success-message'))
                <div class="col-md-12 col-lg-12  mt-3">
                    <div class="alert alert-success">
                        {{ Session::get('success-message') }}
                    </div>
                </div>
                @endif
                <!-- success message --> 

                 <!-- error message --> 
                 @if(Session::has('error-message'))
                <div class="col-md-12 col-lg-12  mt-3">
                    <div class="alert alert-danger">
                        {{ Session::get('error-message') }}
                    </div>
                </div>
                @endif
                <!-- error message --> 
            <form action="/demo-list/save" method="POST"> 
                @csrf
                <div class="col-md-6">
                    <input id="preffered_demo_date"  placeholder="Preferred Demo Date" class="form-control input-contact text-white" name="demo_date" value="{{old('demo_date')}}" required>
                    <i class="fas fa-calendar-alt fa-2x icon-input" id="icon-calendar"></i>
                </div>
                <div class="col-md-6">
                    <input id="preffered_demo_time" type="time"   value="12:00" step="900" placeholder="Preferred Demo Time" class="form-control input-contact text-white" name="demo_time" required>
                    <i class="fas fa-clock fa-2x icon-input"  id="icon-time"></i>
                </div>
                <div class="col-md-12">
                    <input type="text" placeholder="Full Name" class="form-control input-contact text-white" name="full_name" value="{{old('full_name')}}" required>
                </div>
                <div class="col-md-12">
                    <input type="text" placeholder="Address" class="form-control input-contact text-white" name="address"  value="{{old('address')}}" required>
                </div>
                <div class="col-md-6">
                    <input type="email" placeholder="Email Address" class="form-control input-contact text-white" name="email_address"  value="{{old('email_address')}}">
                </div>
                <div class="col-md-6">
                    <input type="number" placeholder="Contact Number" class="form-control input-contact text-white" name="contact_number" value="{{old('contact_number')}}">
                </div>
                <div class="col-md-12 ">
                    <p class="text-xsmall" style="width: 100%; font-size:16.5px !important">Note: We will bring your selected rugs from your Demo List to your place at your most convenient date and time FREE of CHARGE.</p>
                    <button type="submit" class="btn btn-schedule-demo" style="border-radius: 10px">SCHEDULE DEMO</button>
                </div>
            </form>
            </div>

        </div>
        <!-- /.container -->
        
    </div>
    <!-- /.opacity -->
</div>

<!-- /.client-slider -->
<!-- Scroll Top Button -->
<button class="scroll-top tran3s bg-blue-royal">
    <i class="fa fa-arrow-up" aria-hidden="true"></i>
</button>

@stop @section('page-styles')
<style type="text/css">
    .our-service {
        padding: 80px 0 80px 0;
        margin: 0px 0 0px 0 !important;
    }
  
     

    .img-product-size {    
    height: 400px;width: 100%;
    }

    .icon-close {
        position: absolute;
        top: 0px;
        z-index: 100000;
        right: 0px;
        border-radius: 50%;
        padding: 7px;
    }
    ::placeholder {
      color: white !important;
      opacity: 1; /* Firefox */
    }

    :-ms-input-placeholder { /* Internet Explorer 10-11 */
     color: white !important;
    }

    ::-ms-input-placeholder { /* Microsoft Edge */
     color: white !important;
    }
</style>
@stop




@section('page-scripts')
<script type="text/javascript">
  $(document).ready(function(){

        $("#preffered_demo_date").datepicker({
        dateFormat: "yy-mm-dd",
        maxDate: new Date('2099-12-12')
        });
        
        $('#icon-calendar').click(function() {
            $("#preffered_demo_date").focus();
        });

        $('#icon-time').click(function() {
            $("#preffered_demo_time").focus();
        });
  });
</script>

@stop