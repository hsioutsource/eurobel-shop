<!DOCTYPE html>
<html>
<title>Eurobel | Rugs and Carpet</title>
<meta property="og:url"  content="{{env('APP_URL')}}" />
<meta property="og:description" content="Eurobel strives to deliver nothing but the best quality and design options for any room setting. In light of this, we envision designers, architects, hotel owners, and households all around the Philippine archipelago to benefit from our products to achieve the aesthetic of their various spaces" />
<meta property="og:site_name" content="Eurobel | Rugs and Carpet"/>
<meta property="og:image" content="{{asset('frontend/images/eurobel-thumbnail.jpg')}}"/>
<meta name="twitter:image:alt" content="Eurobel | Rugs and Carpet"/>
<meta property="twitter:image:type" content="image/jpeg" />
<meta name="twitter:image" content="{{asset('frontend/images/eurobel-thumbnail.jpg')}}">
<meta name="twitter:card" content="summary" />
<meta property="og:title" content="Eurobel | Rugs and Carpet"/>
<meta property="og:type" content="website" />
<meta property="og:image:type" content="image/jpeg" />
<head>
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<title>Eurobel Rugs + Carpet</title>
</head>
<body>
	<div class="container text-center align-items-center min-vh-100">	
		<div class="row text-center" style="display: block !important; margin-top:10%">
			<img src="http://eurobel.com.ph/frontend/images/Logo-Long-v3.png" class="text-center" style="width: 50%;">
		</div>
		<div class="row text-center" style="display: block !important; margin-top:5%">
			<h1 style="">Something really great is coming! <br>Eurobel Rugs + Carpet Website is under development.</h1>
		</div>
		<div class="row text-center" style="display: block !important; margin-top:5%">	
			<a href="https://www.facebook.com/eurobelrugscarpets/"> <button type="button" class="btn  btn-lg btn-primary">Facebook <i class="fa fa-facebook-square"></i></button> </a>
			<a href="https://www.instagram.com/eurobel_rugs/"> <button type="button" class="btn btn-lg  btn-danger btn-primary">Instagram  <i class="fa fa-instagram"></i></button> </a>
		</div>
	</div>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.jss">
	</script> 
</body>
</html>