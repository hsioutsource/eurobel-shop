@extends('frontend._layouts.main')
@section('page-dynamic-metas')
<title>Eurobel Rugs + Carpets Manila | Free Demo and Shipping (NCR)‎</title>
<meta property="og:url"  content="{{env('APP_URL')}}" />
<meta property="og:description" content="AMENITY + STYLE + WARMTH. Serving you as the largest retailer of premium rugs and carpets in the Philippines since 1995. Book your FREE DEMO today! @eurobel_rugs, @eurobelrugscarpets, @eurobelrugsandcarpets@gmail.com" />

<meta name="robots" content="index, follow">

<meta name="description" content="AMENITY + STYLE + WARMTH. Serving you as the largest retailer of premium rugs and carpets in the Philippines since 1995. Book your FREE DEMO today! @eurobel_rugs, @eurobelrugscarpets, @eurobelrugsandcarpets@gmail.com">
@stop

@section('content')

<!-- ===================================================
  Loading Transition
==================================================== -->
<div id="loader-wrapper">
    <div id="loader"></div>
</div>


<div id="banner" class="homeOne-banner">
    <div class="rev_slider_wrapper">

        <!-- START REVOLUTION SLIDER 5.0.7 auto mode -->
        <div id="theme-main-banner" class="rev_slider" data-version="5.0.7" >
            <ul>
                    <!-- MAIN IMAGE -->
                    @if($homepage_imageslider[0]->image_banner_1 != "")
                       
                        <!-- SLIDE1  -->
                         <li data-index="rs-1" data-transition="zoomout" data-slotamount="default" data-rotate="0" data-saveperformance="off" data-title="01" data-description="">
                          
                            <div class="overlay"></div>
                            <img src="{{$homepage_imageslider[0]->image_banner_1}}" alt="image" class="rev-slidebg" data-bgparallax="3" data-bgposition="center center" data-duration="20000" data-ease="Linear.easeNone" data-kenburns="on" data-no-retina="" data-offsetend="0 0" data-offsetstart="0 0" data-rotateend="0" data-rotatestart="0" data-scaleend="100" data-scalestart="140">
                                <!-- LAYERS -->
                                <!-- LAYER NR. 1 -->

                                <!-- LAYER NR. 7 -->
                                <div class="tp-caption text-center" data-x="['left','left','left','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['42','42','42','80']" data-width="full" data-height="none" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];" data-mask_out="x:inherit;y:inherit;" data-start="500" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6;">
                                    <h1 style="font-size:60px">{{$homepage_imageslider[0]->image_banner_text1}}</h1>
                                </div>
                        </li>

                    @endif

                        <!-- LAYER NR. 8 -->
                    @if($homepage_imageslider[0]->image_banner_2 != "")
                        <!-- SLIDE2  -->
                        <li data-index="rs-2" data-transition="zoomout" data-slotamount="default" data-rotate="0" data-saveperformance="off" data-title="01" data-description="">
                        <div class="overlay"></div>
                        <img src="{{$homepage_imageslider[0]->image_banner_2}}" alt="image" class="rev-slidebg" data-bgparallax="3" data-bgposition="center center" data-duration="20000" data-ease="Linear.easeNone" data-kenburns="on" data-no-retina="" data-offsetend="0 0" data-offsetstart="0 0" data-rotateend="0" data-rotatestart="0" data-scaleend="100" data-scalestart="140">
                         <!-- LAYERS -->
                        <!-- LAYER NR. 1 -->

                        <!-- LAYER NR. 7 -->
                        <div class="tp-caption text-center" data-x="['left','left','left','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['42','42','42','80']" data-width="full" data-height="none" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];" data-mask_out="x:inherit;y:inherit;" data-start="500" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6;">
                            <h1 style="font-size:60px">{{$homepage_imageslider[0]->image_banner_text2}}</h1>
                        </div>
                        </li>
                    @endif
                                         
                        <!-- LAYER NR. 8 -->
                    @if($homepage_imageslider[0]->image_banner_3 != "")
                     <!-- SLIDE3  -->
                     <li data-index="rs-3" data-transition="zoomout" data-slotamount="default" data-rotate="0" data-saveperformance="off" data-title="01" data-description="">
                        <div class="overlay"></div>
                        <img src="{{$homepage_imageslider[0]->image_banner_3}}" alt="image" class="rev-slidebg" data-bgparallax="3" data-bgposition="center center" data-duration="20000" data-ease="Linear.easeNone" data-kenburns="on" data-no-retina="" data-offsetend="0 0" data-offsetstart="0 0" data-rotateend="0" data-rotatestart="0" data-scaleend="100" data-scalestart="140">
                         <!-- LAYERS -->
                        <!-- LAYER NR. 1 -->

                        <!-- LAYER NR. 7 -->
                        <div class="tp-caption text-center" data-x="['left','left','left','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['42','42','42','80']" data-width="full" data-height="none" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];" data-mask_out="x:inherit;y:inherit;" data-start="500" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6;">
                            <h1 style="font-size:60px">{{$homepage_imageslider[0]->image_banner_text3}}</h1>
                        </div>
                        </li>
                    @endif

                        <!-- LAYER NR. 8 -->
                    @if($homepage_imageslider[0]->image_banner_4 != "")
                        <!-- SLIDE4  -->
                        <li data-index="rs-4" data-transition="zoomout" data-slotamount="default" data-rotate="0" data-saveperformance="off" data-title="01" data-description="">
                            <div class="overlay"></div>
                        <img src="{{$homepage_imageslider[0]->image_banner_4}}"  alt="image" class="rev-slidebg" data-bgparallax="3" data-bgposition="center center" data-duration="20000" data-ease="Linear.easeNone" data-kenburns="on" data-no-retina="" data-offsetend="0 0" data-offsetstart="0 0" data-rotateend="0" data-rotatestart="0" data-scaleend="100" data-scalestart="140">
                         <!-- LAYERS -->
                        <!-- LAYER NR. 1 -->

                        <div class="tp-caption text-center" data-x="['left','left','left','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['42','42','42','80']" data-width="full" data-height="none" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];" data-mask_out="x:inherit;y:inherit;" data-start="500" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6;">
                            <h1 style="font-size:60px">{{$homepage_imageslider[0]->image_banner_text4}}</h1>
                        </div>
                        </li>

                    @endif

                        <!-- LAYER NR. 8 -->
                    @if(!$homepage_imageslider[0])
                       <!-- default  -->
                       <li data-index="rs-280" data-transition="zoomout" data-slotamount="default" data-rotate="0" data-saveperformance="off" data-title="01" data-description="">
                        <div class="overlay"></div>
                        <img src="{{asset('frontend/images/home/slide-1.jpg')}}" alt="image" class="rev-slidebg" data-bgparallax="3" data-bgposition="center center" data-duration="20000" data-ease="Linear.easeNone" data-kenburns="on" data-no-retina="" data-offsetend="0 0" data-offsetstart="0 0" data-rotateend="0" data-rotatestart="0" data-scaleend="100" data-scalestart="140">
                         <!-- LAYERS -->
                        <!-- LAYER NR. 1 -->

                        <!-- LAYER NR. 7 -->
                        <div class="tp-caption text-center" data-x="['left','left','left','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['42','42','42','80']" data-width="full" data-height="none" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];" data-mask_out="x:inherit;y:inherit;" data-start="500" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6;">
                            <h1 style="font-size:60px">AMENITY + WARMTH + STYLE</h1>
                        </div>
                        <!-- LAYER NR. 8 -->
                        </li>
                        
                    @endif
                   

               
            </ul>
        </div>
    </div>
    <!-- END REVOLUTION SLIDER -->
</div>

<!--  /#banner -->

<!--
=============================================
  Our Service
==============================================
-->
<div class="bg-light-gray">
    <div class="our-service">
        <div class="container">
            <div class="text-center">
                <h2 class="text-blue" style="font-weight: normal !important;">FEATURED PRODUCTS</h2>
            </div>
            <!-- /.theme-title -->

            <div class="row">

                @if(!$products->isEmpty())
                        <!-- /.service-sldier -->
                <div class="service-slider">
                    @foreach($products as $product)
                    <div class="item">
                        <div class="single-service">
                        
                            <div class="product-container"> 
                                <a href="{{route('frontend.product.show',[$product->name,$product->code])}}">
                                <div class="product-image-container"> 
                                    <img style="width:100%; height:100%;" src="{{ $product->image_rectangle}}" alt="">
                                </div>
                                 </a>

                                <div class="text style-one tran3s text-center">
                                    <span class="text-medium"><a href="{{route('frontend.product.show',[$product->name,$product->code])}}" class="text-no--decoration tran3s text-blue">{{ $product->name}}</a></span>
                                    <p class="text-small 
                                    text-gray"><a href="{{route('frontend.product.show',[$product->name,$product->code])}}" class="text-no--decoration tran3s text-blue"> {{ $product->code }} </a></p>
                                </div>
                                 
                            </div>
                        </div>
                        
                        <!-- /.single-service -->
                    </div>
                    @endforeach
                </div>
                <!-- /.service-sldier -->
                @else
                     <h3 style="text-align:center">No Products Available...</h3>
                     <p style="text-align:center">Kindly contact the website administrator</p>
                @endif


            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
        <div id="demo"> </div>
    </div>
    <!-- /.our-service -->
 
</div>

<div class="client-slider"  style="margin: 0px !important">
    <div class="opacity opacityOne">
        <div class="container text-white">
            <h3 class=" text-white" style="margin-bottom: 30px">SCHEDULE FOR A FREE DEMO</h3>
            <div class="row">
                    <!-- error message --> 
                @if ($errors->any())
                <div class="col-md-12 col-lg-12 mt-3">
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                @endif
                <!-- error message --> 
                
                <!-- success message --> 
                @if(Session::has('success-message'))
                <div class="col-md-12 col-lg-12  mt-3">
                    <div class="alert alert-success">
                        {{ Session::get('success-message') }}
                    </div>
                </div>
                @endif
                <!-- success message --> 

                 <!-- error message --> 
             @if(Session::has('error-message'))
                <div class="col-md-12 col-lg-12  mt-3">
                    <div class="alert alert-danger">
                        {{ Session::get('error-message') }}
                    </div>
                </div>
                @endif
                <!-- error message --> 
            <form action="/demo-list/save" method="POST">
                @csrf
                <div class="col-md-6">
                    <input id="preffered_demo_date"  placeholder="Preferred Demo Date" class="form-control input-contact text-white" name="demo_date" value="{{old('demo_date')}}" required>
                    <i class="fas fa-calendar-alt fa-2x icon-input" id="icon-calendar"></i>
                </div>
                <div class="col-md-6">
                    <input id="preffered_demo_time" type="time"   value="12:00" step="900" placeholder="Preferred Demo Time" class="form-control input-contact text-white" name="demo_time" required>
                    <i class="fas fa-clock fa-2x icon-input"  id="icon-time"></i>
                </div>
                <div class="col-md-12">
                    <input type="text" placeholder="Full Name" class="form-control input-contact text-white" name="full_name" value="{{old('full_name')}}" required>
                </div>
                <div class="col-md-12">
                    <input type="text" placeholder="Address" class="form-control input-contact text-white" name="address"  value="{{old('address')}}" required>
                </div>
                <div class="col-md-6">
                    <input type="email" placeholder="Email Address" class="form-control input-contact text-white" name="email_address"  value="{{old('email_address')}}">
                </div>
                <div class="col-md-6">
                    <input type="number" placeholder="Contact Number" class="form-control input-contact text-white" name="contact_number" value="{{old('contact_number')}}">
                </div>
                <div class="col-md-12 ">
                    <p class="text-xsmall" style="width: 100%; font-size:16.5px !important">Note: We will bring your selected rugs from your Demo List to your place at your most convenient date and time FREE of CHARGE.</p>
                    <button type="submit" class="btn btn-schedule-demo" style="border-radius: 10px">SCHEDULE DEMO</button>
                </div>
            </form>
            </div>

        </div>
        <!-- /.container -->
    </div>
    <!-- /.opacity -->
</div>
<!-- /.client-slider -->


<!-- Scroll Top Button -->
<button class="scroll-top tran3s bg-blue-royal">
    <i class="fa fa-arrow-up" aria-hidden="true"></i>
</button>


<!--Banner Modal --->

@foreach($sitesettings as $sitesetting)

@if($sitesetting->show_banner === '1')
<div class="modal fade" id="featuredModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="z-index: 9999999 !important;position: fixed;top: 15%;overflow-y: hidden; max-height:550px">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-body" style="padding: 0px !important;">
            <i data-dismiss="modal" class="bg-white fas fa-times-circle fa-2x text-blue close-modal"></i>
            @if($sitesetting->banner_modal_image)
            <div class="modal-image-container"> 
                 <img src="{{$sitesetting->banner_modal_image}}" class="modal-image">
            </div>
            @else
            <img src="{{asset('frontend/images/sale.png')}}">
            @endif
        </div>
        </div>
    </div>
</div>
 
@endif
@endforeach
<!--Banner Modal --->

@stop @section('page-styles')
<style type="text/css">

   
    .overlay {
        position: absolute;
        width: 100%;
        height: 100%;
        z-index: 2;
        background-color: rgba(0,0,0,0.75);
        opacity: .3;
    }



    .modal-image-container { 
      
    }

    .modal-image { 
        width: 100%;
        height: 100%;
    }


    .our-service {
        padding: 80px 0 80px 0;
        margin: 0px 0 0px 0 !important;
    }
    .close-modal {
      position: absolute;top: 15px;right: 20px;border-radius: 50%;padding: 8px;
    }
    .z-index--header {
        z-index: 1000 !important;
    }
    ::placeholder {
      color: white !important;
      opacity: 1; /* Firefox */
    }

    :-ms-input-placeholder { /* Internet Explorer 10-11 */
     color: white !important;
    }

    ::-ms-input-placeholder { /* Microsoft Edge */
     color: white !important;
    }
</style>
@stop

@section('page-scripts')
<script type="text/javascript">
  $(document).ready(function(){

  
        $("#preffered_demo_date").datepicker({
        dateFormat: "yy-mm-dd",
        maxDate: new Date('2099-12-12')
        });
        
        $('#icon-calendar').click(function() {
            $("#preffered_demo_date").focus();
        });

        $('#icon-time').click(function() {
            $("#preffered_demo_time").focus();
        });


    if (sessionStorage.getItem("isVisit") === null) {
        $("#featuredModal").modal('show');
     }
    
   setSessionVisit(); 
    function setSessionVisit(){
        sessionStorage.setItem("isVisit",true);
    }


  });
</script>

@stop