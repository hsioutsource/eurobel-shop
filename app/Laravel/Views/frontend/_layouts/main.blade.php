<!DOCTYPE html>
<html>
<head>
	@yield('page-dynamic-metas')
	@include('frontend._components.metas')
	@include('frontend._components.styles')
	@yield('page-styles')
	
</head>

<body>	
		<div class="main-page-wrapper">    

			@include('frontend._components.header')	
			@yield('content')
			
			@include('frontend._components.footer')	
			
			
		</div>
			@include('frontend._components.scripts')
			@yield('page-scripts')

</body>
</html>