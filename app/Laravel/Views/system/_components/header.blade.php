 <header class="navbar navbar-header navbar-header-fixed bg-white">
      <a href="#" id="mainMenuOpen" class="burger-menu"><i data-feather="menu"></i></a>
      <div class="navbar-brand">
        <a href="{{ route('system.index')}}" class="df-logo">
          <img src="{{asset('assets/img/logo1.png')}}" class="logo-size" style="height:50px !important;">
        </a>
      </div><!-- navbar-brand -->
      <div id="navbarMenu" class="navbar-menu-wrapper">
        <div class="navbar-menu-header">
          <a href="{{ route('system.index')}}" class="df-logo">
            <img src="{{asset('assets/img/logo1.png')}}" class="logo-size mt-2 mb-2" style="height:50px !important;">
          </a>
          <a id="mainMenuClose" href="#"><i data-feather="x"></i></a>
        </div><!-- navbar-menu-header -->
        <ul class="nav navbar-menu d-block d-lg-none ml-3">
          <li class="nav-label mg-b-15 mt-3">Products</li>
          <li class="nav-item mt-2 navbar-hover">
            <a href="{{ route('system.products.index')}}" class="nav-link font-small  navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="list"></i> Product List</a>
          </li>
          <li class="nav-item mt-2 navbar-hover">
            <a href="{{ route('system.products.create')}}" class="nav-link font-small  navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="plus"></i> Add Product</a>
          </li>
          <li class="nav-label mg-b-15 mt-3">Showrooms</li>
          <li class="nav-item mt-2 navbar-hover">
            <a href="{{ route('system.showroom.index')}}" class="nav-link font-small navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="list"></i> List Show Rooms</a>
          </li>  
          <li class="nav-item mt-2 navbar-hover">
            <a href="{{ route('system.showroom.create')}}" class="nav-link font-small navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="plus"></i> Add Show Rooms</a>
          </li>
          <li class="nav-label mg-b-15 mt-3">Demo List</li>
          <li class="nav-item mt-2 navbar-hover">
            <a href="{{ route('system.scheduled-demo.index')}}" class="nav-link font-small navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="clock"></i> Scheduled demo</a>
          </li>    
          <li class="nav-label mg-b-15 mt-3">Inquiries</li>
          <li class="nav-item mt-2 navbar-hover">
            <a href="{{ route('system.inquiries.index')}}" class="nav-link font-small  navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="file-text"></i> Inquiries List</a>
          </li>
          <li class="nav-label mg-b-15 mt-3">Subscribers</li>
          <li class="nav-item mt-2 navbar-hover">
            <a href="{{ route('system.subscribe.index')}}" class="nav-link font-small  navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="mail"></i> Subscribers List</a>
          </li>
          <li class="nav-label mg-b-15 mt-3">Site Management</li>
          <li class="nav-item mt-2 navbar-hover">
            <a href="{{ route('system.aboutpagesettings.index')}}" class="nav-link font-small  navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="info"></i> About Page Settings</a>
          </li>
          <li class="nav-item mt-2 navbar-hover">
            <a href="{{ route('system.sitesettings.index')}}" class="nav-link font-small  navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="settings"></i> Site Settings</a>
          </li>
        </ul>
      </div><!-- navbar-menu-wrapper -->
      <div class="navbar-right">
        <div class="dropdown dropdown-profile">

          <a href="#" class="dropdown-link" data-toggle="dropdown" data-display="static">
           <div class="avatar avatar-sm"><img src="{{asset('assets/img/img1.png')}}" class="rounded-circle" alt=""></div>
          </a><!-- dropdown-link -->
          <div class="dropdown-menu dropdown-menu-right tx-13">
            <div class="avatar avatar-lg mg-b-15"><img src="{{asset('assets/img/img1.png')}}" class="rounded-circle" alt=""></div>
            <h6 class="tx-semibold mg-b-5">Administrator</h6>

            <div class="dropdown-divider"></div>
            {{-- <a href="{{ route('system.settings.index')}}" class="dropdown-item"><i data-feather="settings"></i>Account Settings</a> --}}
            <a href="{{ route('system.logout')}}" class="dropdown-item"><i data-feather="log-out"></i>Sign Out</a>
          </div><!-- dropdown-menu -->
        </div><!-- dropdown -->
      </div><!-- navbar-right -->
    </header><!-- navbar -->