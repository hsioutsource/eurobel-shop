<div id="sidebarMenu" class="sidebar sidebar-fixed sidebar-components">
      <div class="sidebar-header">
        <a href="#" id="mainMenuOpen"><i data-feather="menu"></i></a>
        <h5>Components</h5>
        <a href="#" id="sidebarMenuClose"><i data-feather="x"></i></a>
      </div><!-- sidebar-header -->
      <div class="sidebar-body">
        <ul class="sidebar-nav">
          <li class="nav-label mg-b-15 mt-3">Products</li>
          <li class="nav-item mt-2 navbar-hover">
            <a href="{{ route('system.products.index')}}" class="nav-link font-small  navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="list"></i> Product List</a>
          </li>
          <li class="nav-item mt-2 navbar-hover">
            <a href="{{ route('system.products.create')}}" class="nav-link font-small  navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="plus"></i> Add Product</a>
          </li>
          <li class="nav-label mg-b-15 mt-3">Sale</li>
          <li class="nav-item mt-2 navbar-hover">
            <a href="{{ route('system.sale.index')}}" class="nav-link font-small  navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="list"></i> Sale List</a>
          </li>
          <li class="nav-item mt-2 navbar-hover">
            <a href="{{ route('system.sale.create')}}" class="nav-link font-small  navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="plus"></i> Add Sale</a>
          </li>
          <li class="nav-label mg-b-15 mt-3">Showrooms</li>
          <li class="nav-item mt-2 navbar-hover">
            <a href="{{ route('system.showroom.index')}}" class="nav-link font-small navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="list"></i> List Show Rooms</a>
          </li>  
          <li class="nav-item mt-2 navbar-hover">
            <a href="{{ route('system.showroom.create')}}" class="nav-link font-small navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="plus"></i> Add Show Rooms</a>
          </li>
          <li class="nav-label mg-b-15 mt-3">Demo List</li>
          <li class="nav-item mt-2 navbar-hover">
            <a href="{{ route('system.scheduled-demo.index')}}" class="nav-link font-small navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="clock"></i> Scheduled demo</a>
          </li>    
          <li class="nav-label mg-b-15 mt-3">Inquiries</li>
          <li class="nav-item mt-2 navbar-hover">
            <a href="{{ route('system.inquiries.index')}}" class="nav-link font-small  navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="file-text"></i> Inquiries List</a>
          </li>
          <li class="nav-label mg-b-15 mt-3">Subscribers</li>
          <li class="nav-item mt-2 navbar-hover">
            <a href="{{ route('system.subscribe.index')}}" class="nav-link font-small  navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="mail"></i> Subscribers List</a>
          </li>
          
          <li class="nav-label mg-b-15 mt-3">Site Management</li>
          <li class="nav-item mt-2 navbar-hover">
            <a href="{{ route('system.aboutpagesettings.index')}}" class="nav-link font-small  navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="info"></i> About Page Settings</a>
          </li>
          <li class="nav-item mt-2 navbar-hover">
            <a href="{{ route('system.sitesettings.index')}}" class="nav-link font-small  navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="settings"></i> Site Settings</a>
          </li>
        </ul>
      </div><!-- sidebar-body -->
    </div><!-- sidebar -->