@extends('system._layouts.main')

@section('content')
<div class="content content-components mb-5">
    <div class="row">
        <div class="col-lg-12 col-xl-12 mg-t-10">
            <div class="mg-b-10">
                <div class="card-header pd-t-20 d-sm-flex align-items-start justify-content-between bd-b-0 pd-b-0">
                    <div>
                        <h4 class="mg-b-5">Add Show Room </h4>
                    </div>
                </div><!-- card-header -->
                
            </div><!-- card -->            
        </div><!-- col -->      
        
        
        <!-- error message --> 
        @if ($errors->any())
        <div class="col-md-10 col-lg-10 ml-4 mt-3">
            <div class="alert alert-danger">
                <h4>Warning! Some fields are not accepted.</h4>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
        @endif
       <!-- error message --> 
        
        <!-- success message --> 
        @if(Session::has('success-message'))
        <div class="col-md-10 col-lg-10 ml-4 mt-3">
            <div class="alert alert-success">
                {{ Session::get('success-message') }}
            </div>
        </div>
        @endif

        <!-- success message --> 
       
        
        <!-- form --> 
        <div class="col-md-10 col-lg-10 ml-4 mt-5"> 
            <form action="{{route('system.showroom.save')}}" method="POST" id="showroomform" enctype="multipart/form-data">
                @csrf
                <div class="form-row">
                    <div class="form-group  col-md-8">
                        <label for="name">Location Name</label>
                        <input id="name" name="location_name" type="text" class="form-control" value="{{old('location_name')}}"  placeholder="Enter Location Name">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group  col-md-8">
                        <label for="address">Address</label>
                        <textarea   id="address" name="address" class="form-control" id="exampleFormControlTextarea1" placeholder="Enter Address"rows="3">{{old('address')}}</textarea>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group  col-md-8">
                        <label for="contact_number">Contact Number</label>
                        <input id="contact_number" name="contact_number" type="text" class="form-control" value="{{old('contact_number')}}"  placeholder="Enter Contact Number">
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-5">
                            <div style="height:550px; width:100%">
                                  <img src="{{asset('frontend/images/show_room.jpg')}}" style="height: 100%;width: 100%" alt="" id="image">
                                 <input class="form-control" onchange="readURLImage(this);" data-preview="#preview" name="image" value="{{old('image')}}" type="file" id="image" required/>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="button"  class="btn btn-primary mt-5" id="submit-showroom" value="Create new Show room">
            </form>
        </div>
        
        
        <!-- form -->
        
    </div><!-- container -->
</div><!-- content -->


@section('page-scripts')

<script type="text/javascript">


function readURLImage(input) {
    
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#image')
                .attr('src', e.target.result);
            };
            
            reader.readAsDataURL(input.files[0]);
        }
    }


    
    $(document).ready(function(){
        
        
        
        $("#submit-showroom").click(function(event){
            event.preventDefault()
            updateSiteSettings()
        })
        
        
        function updateSiteSettings() { 
            
            swal({
                title:"Are you sure you want to add new Show room?",
                icon: "info",
                buttons: true,
            })
            .then((willSubmit) => {
                if (willSubmit) {
                    $("#showroomform").submit()
                } else {
                    
                }
            })
        }
        
    })
    
    
    
</script>

@endsection

@stop