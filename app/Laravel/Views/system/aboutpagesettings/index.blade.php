@extends('system._layouts.main')

@section('content')
<div class="content content-components mb-5">
  <div class="row">
    <div class="col-lg-12 col-xl-12 mg-t-10">
      <div class="mg-b-10">
        <div class="card-header pd-t-20 d-sm-flex align-items-start justify-content-between bd-b-0 pd-b-0">
          <div>
            <h4 class="mg-b-5">About Page Settings </h4>
            <p> This section includes some details for the about us page </p>
              <a href="{{env("APP_URL")."about-us"}}" target="_blank">Click this to redirect to about us page </a>
          </div>
        </div><!-- card-header -->
        
      </div><!-- card -->            
    </div><!-- col -->      
    
    
    <!-- error message --> 
    @if ($errors->any())
    <div class="col-md-10 col-lg-10 ml-4 mt-3">
      <div class="alert alert-danger">
        <h4>Warning! Some fields are not accepted.</h4>
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    </div>
    @endif
    <!-- error message --> 
    
    <!-- success message --> 
    @if(Session::has('success-message'))
    <div class="col-md-10 col-lg-10 ml-4 mt-3">
      <div class="alert alert-success">
        {{ Session::get('success-message') }}
      </div>
    </div>
    @endif
    
    <!-- success message --> 
    
    
    <!-- form --> 
    <div class="col-md-10 col-lg-10 ml-4 mt-5"> 
      <form action="{{route('system.aboutpagesettings.update')}}" method="POST" id="aboutpagesettingsform" enctype="multipart/form-data">
        @csrf

        <div class="form-row" style="margin-bottom:5%" > 
          <button type="button" class="btn btn-info" id="btn-add-new-column">Add New Section   <i data-feather="plus"></i></button>
          
        </div>
        
        <div id="column-form"> 
        <!--- IF About page settings null display this-->

        @if($aboutpagesettings->isEmpty())
          <div class="form-row"> 
            <input type="hidden" name="id[]" />
            <button type="button" class="btn btn-danger btn-remove-column"  style="margin-bottom:3%">Remove Section   <i data-feather="trash"></i></button>
            <div class="form-group col-md-12">
              <div class="custom-control custom-checkbox">
                <input type="checkbox" class="form-check-input" name="alignimageleft[]">
                <label class="form-check-label">Align image left (Leave blank if image align right)</label>            
              </div>
            </div>
            <div class="form-group col-md-6">
              <label >Description</label>
              <textarea name="text[]" class="form-control description" rows="10" required></textarea>
            </div>
            <div class="form-group col-md-6">
              <label >Image</label>
              <div style="height: 400px; width:auto;"> 
              <img src="{{asset('frontend/images/home/6.jpg')}}" alt="" style="height:100%; width:100%;" id="preview">
              </div>
              <input class="form-control"  data-preview="#preview" name="image[]" type="file" accept="image/x-png,image/gif,image/jpeg" required/>
            </div>
          </div>

         @else 

        @foreach($aboutpagesettings as $key => $aboutpagesettings)
         <div class="form-row"> 
          <button type="button" class="btn btn-danger btn-remove-column"  style="margin-bottom:3%">Remove Section   <i data-feather="trash"></i></button>
         <input type="hidden" name="id[{{$aboutpagesettings->id}}]" value="{{$aboutpagesettings->id}}" />
          <div class="form-group col-md-12">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="form-check-input" name="alignimageleft[{{$aboutpagesettings->id}}]"  {{$aboutpagesettings->align_image_left === '1' ?  'checked': '' }}>
              <label class="form-check-label">Align image left (Leave blank if image align right)</label>            
            </div>
          </div>
          <div class="form-group col-md-6">
            <label >Description</label>
          <textarea name="text[{{$aboutpagesettings->id}}]" class="form-control description" rows="10" id="{{$aboutpagesettings->id}}" required>{{$aboutpagesettings->text}}</textarea>
          </div>
          <div class="form-group col-md-6">
            <label >Image</label>
            <div style="height: 400px; width:auto;"> 
            <img src="{{$aboutpagesettings->image}} " alt="" style="height:100%; width:100%;" id="preview">
            </div>
            <input class="form-control"  data-preview="#preview" name="image[{{$aboutpagesettings->id}}]" type="file" accept="image/x-png,image/gif,image/jpeg" value={{$aboutpagesettings->image}} required/>
          </div>
        </div>

        @endforeach


         @endif

        </div>




        <div class="form-group">
          <input class="btn btn-primary" type="submit" value="Save" id="btn-update-aboutpagesettings">    
        </div> 
      </form>
    </div>

   <div style="display:none"> 
    <div class="form-row" id="blank-form-about" > 
      <input type="hidden" name="id[]" value="on" />
      <button type="button" class="btn btn-danger btn-remove-column"  style="margin-bottom:3%">Remove Section   <i data-feather="trash"></i></button>
      <div class="form-group col-md-12">
        <div class="custom-control custom-checkbox">
          <input type="checkbox" class="form-check-input" name="alignimageleft[]">
          <label class="form-check-label">Align image left (Leave blank if image align right)</label>            
        </div>
      </div>
      <div class="form-group col-md-6">
        <label >Description</label>
        <textarea name="text[]" class="form-control description" rows="10" required></textarea>
      </div>
      <div class="form-group col-md-6">
        <label >Image</label>
        <div style="height: 400px; width:auto;"> 
        <img src="{{asset('frontend/images/home/6.jpg')}}" alt="" style="height:100%; width:100%;" id="preview">
        </div>
        <input class="form-control"  data-preview="#preview" name="image[]" type="file" accept="image/x-png,image/gif,image/jpeg" required/>
      </div>
    </div>
   </div>


    <!-- form -->
    
  </div><!-- container -->
</div><!-- content -->


@section('page-scripts')



<script type="text/javascript">

  $(document).ready(function(){

      $('.description').summernote({
        placeholder: 'Enter description',
        tabsize: 2,
        height: 300,
        toolbar: [
          ['style', ['style']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['table', ['table']],
          ['insert', ['link']],
          ['view', ['fullscreen']]
        ]
      });
    

    
    
    $("#btn-update-aboutpagesettings").click(function(event){
      event.preventDefault()
      updateAboutPageSettings()
    })



    function updateAboutPageSettings() { 
      
      swal({
        title:"Are you sure you want to save",
        icon: "info",
        buttons: true,
      })
      .then((willSubmit) => {
        if (willSubmit) {
          $("#aboutpagesettingsform").submit()
        } else {
          
        }
      })
    }

    
    $("#btn-add-new-column").click(function(){


      swal({
        title:"Are you sure you want to add new section?",
        icon: "info",
        buttons: true,
      })
      .then((willSubmit) => {
        if (willSubmit) {
          $('#blank-form-about').clone().insertAfter('#column-form')
        } else {
          
        }
      })

      
    })

    
      $(document).on("click", ".btn-remove-column", function() {
        
          swal({
            title:"Are you sure you want to remove this section?",
            icon: "warning",
            buttons: true,
          })
          .then((willSubmit) => {
            if (willSubmit) {
              $(this).parent().remove();
            } else {
              
            }
          })
        
      });
    
  })
  

</script>

@endsection

@stop