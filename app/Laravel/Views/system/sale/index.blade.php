@extends('system._layouts.main')


@section('content')
<div class="content content-components mb-5">
    <div class="row">
        <div class="col-lg-12 col-xl-12 mg-t-10">
            <div class="mg-b-10">
                <div class="card-header pd-t-20 d-sm-flex align-items-start justify-content-between bd-b-0 pd-b-0">
                    <div>
                        <h4 class="mg-b-5">Sale </h4>
                        <p> List of all Sale </p>
                    </div>
                </div><!-- card-header -->
                
            </div><!-- card -->            
        </div><!-- col -->      
        
        
        <!-- error message --> 
        @if ($errors->any())
        <div class="col-md-10 col-lg-10 ml-4 mt-3">
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
        @endif
        <!-- error message --> 
        
        <!-- success message --> 
        @if(Session::has('success-message'))
        <div class="col-md-10 col-lg-10 ml-4 mt-3">
            <div class="alert alert-success">
                {{ Session::get('success-message') }}
            </div>
        </div>
        @elseif(Session::has('error-message'))
        <div class="col-md-12 col-lg-12 ml-4 mt-3">
            <div class="alert alert-danger">
                {{ Session::get('error-message')}}
            </div>
        </div>
        @endif
        
        <!-- success message --> 
    
        <div class="col-lg-12 mt-3" style="padding:0 4%">
            <div data-label="All Products" class="df-example demo-table">
                <div class="table-responsive">
                    <table class="table table-hover mg-b-0" id="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Code</th>
                                <th scope="col">Name</th>   
                                <th scope="col">Created at</th>    
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($sales as $key => $sale)
                            <tr>
                                <th class="pt-3 pb-3">{{$key + 1}}</th>
                                <th class="pt-3 pb-3">{{$sale->code}}</th>
                                <td class="pt-3 pb-3">{{$sale->name}}</td>
                                <td class="pt-3 pb-3">{{$sale->created_at}}</td>
                                <td>
                                    <a class="btn btn-xs btn-light text-white bg-primary" href="/product/{{$sale->code}}" target="_blank">
                                        <i data-feather="eye"></i>
                                    </a>
                                    <a class="btn btn-xs btn-light text-white bg-info" href="/admin/sale/{{$sale->id}}/edit">
                                        <i data-feather="edit"></i>
                                    </a>
                                    <a class="btn btn-xs btn-light text-white bg-danger" id="btn-delete-product" product_id="{{$sale->id}}">
                                        <i data-feather="trash"></i>
                                    </a>
                                </td> 
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div><!-- table-responsive -->
            </div>
        </div>
        
        
        
        
        
    </div><!-- container -->
</div><!-- content -->


@section('page-scripts')
<script type="text/javascript">
    

    $(document).ready( function () {
        $('#table').DataTable();


        $(document).on("click", "#btn-delete-product", function() {
              
          var product_id = $(this).attr('product_id')
            swal({
                title:"Are you sure you want to delete this product?",
                icon: "warning",
                buttons: true,
            })
            .then((willSubmit) => {
                if (willSubmit) {
                   window.location.href="/admin/sale/"+product_id+"/delete"
                } else {
                
                 }
            })
      
        });
    } );
    
    
</script>

@endsection

@stop