@extends('system._layouts.main')


@section('content')
<div class="content content-components mb-5">
    <div class="row">
        <div class="col-lg-12 col-xl-12 mg-t-10">
            <div class="mg-b-10">
                <div class="card-header pd-t-20 d-sm-flex align-items-start justify-content-between bd-b-0 pd-b-0">
                    <div>
                        <h4 class="mg-b-5">Add Sale </h4>
                    </div>
                </div><!-- card-header -->
                
            </div><!-- card -->            
        </div><!-- col -->      
        
        
        <!-- error message --> 
        @if ($errors->any())
        <div class="col-md-10 col-lg-10 ml-4 mt-3">
            <div class="alert alert-danger">
                <h4>Warning! Some fields are not accepted.</h4>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
        @endif
        <!-- error message --> 
        
        <!-- success message --> 
        @if(Session::has('success-message'))
        <div class="col-md-10 col-lg-10 ml-4 mt-3">
            <div class="alert alert-success">
                {{ Session::get('success-message') }}
            </div>
        </div>
        @endif
        
        <!-- success message --> 
        
        
        <!-- form --> 
        <div class="col-md-10 col-lg-10 ml-4 mt-5"> 
            <form action="" method="POST" id="addproductform" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <div class="custom-control custom-checkbox">
                        <input name="featured" type="checkbox" class="custom-control-input" id="featured" {{ old('featured') === 'on' ? 'checked' : ''}}>
                        <label class="custom-control-label font-weight-bold" for="featured">Featured Product </label>                    
                    </div>
                    <p>(This product will display as featured product)</p>
                </div>
                <div class="form-group  col-md-11">
                    <label for="name">Name</label>
                    <input id="name" name="name" type="text" class="form-control" value="{{old('name')}}"  placeholder="Enter Product Name">
                </div>
                <div class="form-group col-md-5"> 
                    <label for="code">Code</label>
                    <input id="code" name="code" type="text" class="form-control"  value="{{old('code')}}" placeholder="Eg (RA 12434)">
                </div>
                <div class="form-group col-md-5">
                    <label for="third_line">Third Line Text</label>
                    <input type="text" name="third_line" id="third_line" class="form-control" placeholder="Third Line Text" value="{{old('third_line')}}" />
                </div>
                <div class="form-group col-md-5">
                    <label for="color">Text Color</label>
                    <input type="color" name="color" id="color" class="form-control" value="#6a77eew"/>
                </div>
                <div class="form-group col-md-11">
                    <label for="description">Description</label>
                    <textarea name="description" class="form-control description" rows="10"  id="description">{{old('description')}}</textarea>
                    </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <div style="height:196.25;width:80%"> 
                                  <h3 class="text-blue">Main Image</h3> 
                                  <img src="{{asset('frontend/images/product.jpg')}}"alt=""  style="100%;width: 100%" id="image_rectangle_preview">
                                  <input class="form-control" onchange="readURLImageRectangle(this);" data-preview="#preview" name="image_rectangle" value="{{old('image_rectangle')}}" type="file" id="image_rectangle" accept="image/x-png,image/gif,image/jpeg" required/>
                                
                            </div>
                        </div>
                        <div class="col-md-4">
                            <h3 class="text-blue">Room Setting</h3> 
                            <div style="height:196.25;width:80%"> 
                                  <img src="{{asset('frontend/images/product.jpg')}}"alt=""  style="100%;width: 100%" id="image_square_preview">
                                  <input class="form-control" onchange="readURLImageSquare(this);" data-preview="#preview" name="image_square" value="{{old('image_square')}}" type="file" id="image_square" accept="image/x-png,image/gif,image/jpeg" required/>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <h3 class="text-blue">Close-up</h3> 
                            <div  style="height:196.25;width:80%"> 
                                  <img src="{{asset('frontend/images/product.jpg')}}"alt=""  style="100%;width: 100%" id="image_circle_preview">
                                  <input class="form-control" onchange="readURLImageCircle(this);" data-preview="#preview" name="image_circle" value="{{old('image_circle')}}" type="file" id="image_circle" accept="image/x-png,image/gif,image/jpeg" required/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group"> 
                    <h3 class="mt-5">Product Details</h3>
                    <p>This will be serve the lists of size and price of the product</p>
                    <div id="product-detail-list"> 
                        <div id="details" style="margin-bottom:3%"> 
                            <div class="form-group row"> 
                                <div class="col-md-6">
                                    <labeL>Size</labeL>
                                    <input id="sizes" name="sizes[]" type="text" class="form-control"  placeholder="Size" value="160 X 230 cm" readonly>
                                    <input id="conversion_sizes" name="conversion_sizes[]" type="text" class="form-control" style="display:none" value="5'3&quot x 7'7&quot" readonly>
                                    <span>Inch Size: 5'3&quot x 7'7&quot</span>
                                </div>
                                <div class="col-md-6">
                                    <labeL>Price</labeL>
                                    <input id="prices" name="prices[]" type="text" class="form-control"  value="{{old('prices.0')}}" placeholder="Price">
                                    <span>"Leave blank if no available data in this size. It will not reflect on the website."</span>
                                </div>
                            </div>
                            <div class="form-group row"> 
                                <div class="col-md-6">
                                    <input id="sizes" name="sizes[]" type="text" class="form-control"  placeholder="Size" value="200 X 290 cm " readonly>
                                    <input id="conversion_sizes" name="conversion_sizes[]" type="text" class="form-control" style="display:none" value="6'7&quot x 9'6&quot" readonly>
                                    <span>Inch Size: 6'7&quot x 9'6&quot</span>
                                </div>
                                <div class="col-md-6">
                                    <input id="prices" name="prices[]" type="text" class="form-control"  value="{{old('prices.1')}}" placeholder="Price">
                                    <span>"Leave blank if no available data in this size. It will not reflect on the website."</span>
                                </div>
                            </div>
                            <div class="form-group row"> 
                                <div class="col-md-6">
                                    <input id="sizes" name="sizes[]" type="text" class="form-control"  placeholder="Size" value="240 X 340 cm" readonly>
                                    <input id="conversion_sizes" name="conversion_sizes[]" type="text" class="form-control" style="display:none" value="7'10&quot x 11'2&quot" readonly>
                                    <span>Inch Size: 7'10&quot x 11'2&quot</span>
                                </div>
                                <div class="col-md-6">
                                <input id="prices" name="prices[]" type="text" class="form-control"  value="{{old('prices.2')}}" placeholder="Price">
                                <span>"Leave blank if no available data in this size. It will not reflect on the website."</span>
                                </div>
                            </div>
                            <div class="form-group row"> 
                                <div class="col-md-6">
                                    <input id="sizes" name="sizes[]" type="text" class="form-control"  placeholder="Size" value="280 X 400 cm" readonly>
                                    <input id="conversion_sizes" name="conversion_sizes[]" type="text" class="form-control" style="display:none" value="9'2&quot x 13'2&quot" readonly>
                                    <span>Inch Size: 9'2&quot x 13'2&quot</span>
                                </div>
                                <div class="col-md-6">
                                    <input id="prices" name="prices[]" type="text" class="form-control"  value="{{old('prices.3')}}" placeholder="Price">
                                    <span>"Leave blank if no available data in this size. It will not reflect on the website."</span>
                                </div>
                            </div>
                            <div class="form-group row"> 
                                <div class="col-md-6">
                                    <input id="sizes" name="sizes[]" type="text" class="form-control"  placeholder="Size"  value="320 X 450 cm" readonly>
                                    <input id="conversion_sizes" name="conversion_sizes[]" type="text" class="form-control" style="display:none" value="10'6&quot x 14'9&quot" readonly>
                                    <span>Inch Size: 10'6&quot x 14'9&quot</span>
                                </div>
                                <div class="col-md-6">
                                    <input id="prices" name="prices[]" type="text" class="form-control"  value="{{old('prices.4')}}"  placeholder="Price">
                                    <span>"Leave blank if no available data in this size. It will not reflect on the website."</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="submit"  class="btn btn-primary" value="Create new Product" id="btn-save-product">
            </form>
        </div>
        
        
        <!-- form -->
        
    </div><!-- container -->
</div><!-- content -->


@section('page-scripts')
<script type="text/javascript">
    
    function readURLImageRectangle(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#image_rectangle_preview')
                .attr('src', e.target.result);
            };
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    function readURLImageSquare(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#image_square_preview')
                .attr('src', e.target.result);
            };
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    function readURLImageCircle(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#image_circle_preview')
                .attr('src', e.target.result);
            };
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    
    
    
    $(document).ready(function() {
        $('.description').summernote({
            placeholder: 'Enter Description',
            tabsize: 2,
            height: 300,
            toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link']],
            ['view', ['fullscreen']]
            ]
        });
        
        $("#btn-save-product").click(function(event){
            event.preventDefault()
            saveNewProduct()
        })
        
        $("#btn-more-detail").click(function() {
            $("#product-detail-list > #details:first-child").clone(true).insertBefore("#product-detail-list > #details:last-child");
            return false;
        });
        
        $("#btn-remove").click(function() {
            $(this).parent().remove();
        });
        
        
        
        function saveNewProduct() { 
            
            swal({
                title:"Are you sure you want to add new Product",
                icon: "info",
                buttons: true,
            })
            .then((willSubmit) => {
                if (willSubmit) {
                    $("#addproductform").submit()
                } else {
                    
                }
            })
        }
        
    })
    
    
    
</script>

@endsection

@stop