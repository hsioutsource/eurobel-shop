@extends('system._layouts.main')

@section('content')
<div class="content content-components mb-5">
  <div class="row">
    <div class="col-lg-12 col-xl-12 mg-t-10">
      <div class="mg-b-10">
        <div class="card-header pd-t-20 d-sm-flex align-items-start justify-content-between bd-b-0 pd-b-0">
          <div>
            <h4 class="mg-b-5">Site Settings </h4>
            <p> This section includes some details for the sites </p>
          </div>
        </div><!-- card-header -->
        
      </div><!-- card -->            
    </div><!-- col -->      
    
    
    <!-- error message --> 
    @if ($errors->any())
    <div class="col-md-10 col-lg-10 ml-4 mt-3">
      <div class="alert alert-danger">
        <h4>Warning! Some fields are not accepted.</h4>
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    </div>
    @endif
    <!-- error message --> 
    
    <!-- success message --> 
    @if(Session::has('success-message'))
    <div class="col-md-10 col-lg-10 ml-4 mt-3">
      <div class="alert alert-success">
        {{ Session::get('success-message') }}
      </div>
    </div>
    @endif
    
    <!-- success message --> 
    
    
    <!-- form --> 
    <div class="col-md-10 col-lg-10 ml-4 mt-5"> 
      <form action="{{route('system.sitesettings.update')}}" method="POST" id="sitesettingsform" enctype="multipart/form-data">
        @csrf
        
        @foreach($sitesettings as $sitesetting)
        <div class="form-group">
          <div class="custom-control custom-checkbox">
            <input name="show_banner" type="checkbox" class="custom-control-input" id="show_banner"  {{$sitesetting->show_banner === '1' ?  'checked': '' }} required>
            <label class="custom-control-label font-weight-bold" for="show_banner">Show Banner Modal </label>                    
          </div>
        </div>
        <div class="form-group">
          <label for="mission_vision">Mission/Vision</label>
          <textarea id="mission_vision" name="mission_vision" class="form-control mission_vision" rows="5" required>{{$sitesetting->mission_vision}}</textarea>
        </div>
        <div class="form-group">
          <label for="facebook_link">Facebook Link</label>
          <input id="facebook_link" name="facebook_link" type="text" class="form-control"  placeholder="Enter Facebook Link" value="{{$sitesetting->facebook_link}}" required>
        </div>
        
        <div class="form-group">
          <label for="instagram_link">Instagram Link</label>
          <input id="instagram_link" type="text" name="instagram_link" class="form-control"   placeholder="Enter Instagram Link"  value="{{$sitesetting->instagram_link}}" required>
        </div>
        <div class="form-group">
          <label for="email">Email</label>
          <input id="email" type="email" name="email" class="form-control"   placeholder="Enter Email"  value="{{$sitesetting->email}}" required>
        </div>
        
        <label>Contact Numbers</label>
        <div class="form-row">
          <div class="form-group col-md-4">
            <input type="text" class="form-control" name="contact_number_1" id="contact_number_1" value="{{$sitesetting->contact_number_1}}" required>
          </div>
          <div class="form-group col-md-4">
            <input type="text" class="form-control" name="contact_number_2"  id="contact_number_2"  value="{{$sitesetting->contact_number_2}}" required>
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-md-4">
            <input type="text" class="form-control"  name="contact_number_3"id="contact_number_3" value="{{$sitesetting->contact_number_3}}" required>
          </div>
          <div class="form-group col-md-4">
            <input type="text" class="form-control" name="contact_number_4" id="contact_number_4"  value="{{$sitesetting->contact_number_4}}" required>
          </div>
        </div>
        @endforeach
           <!--- catalog --> 
        <div class="form-row">
          <div class="form-group col-md-12">
            <label for="catalog_file">Catalog File Upload</label>
            <input type="file" class="form-control-file" id="catalog" name="catalog"  accept="application/pdf,application" >
          </div>

           @if($sitesetting->catalog_link)
          <div class="form-group col-md-12">
             <a href="{{$sitesetting->catalog_link}}">View Catalog / {{$sitesetting->catalog_link}}</a>
          </div>
          <div class="form-group col-md-12">
              <a href="/download/catalog/eurobel-catalog.pdf/{{$sitesetting->catalog_name}}">Download Catalog</a>
          </div>
          @else
          <div class="form-group col-md-12">
            <p> Upload file first to see the download and preview link
          </div>
          @endif
        </div>
        <!--- catalog --> 

        <!--- privacy --> 
        <div class="form-row">
          <div class="form-group col-md-12">
            <label for="catalog_file">Privacy File Upload</label>
            <input type="file" class="form-control-file" id="catalog" name="privacy"  accept="application/pdf,application" >
          </div>
           @if($sitesetting->privacy_link)
          <div class="form-group col-md-12">
             <a href="{{$sitesetting->privacy_link}}">View Privacy / {{$sitesetting->privacy_link}}</a>
          </div>
          @else
          <div class="form-group col-md-12">
            <p> Upload file first to see the download and preview link
          </div>
          @endif
        </div>

           <!--- terms of services --> 
         <div class="form-row">
          <div class="form-group col-md-12">
            <label for="terms_of_service">Terms of Services File Upload</label>
            <input type="file" class="form-control-file" id="catalog" name="terms_of_service"  accept="application/pdf,application" >
          </div>
           @if($sitesetting->terms_of_service_link)
          <div class="form-group col-md-12">
             <a href="{{$sitesetting->terms_of_service_link}}">View Terms of Service / {{$sitesetting->terms_of_service_link}}</a>
          </div>
          @else
          <div class="form-group col-md-12">
            <p> Upload file first to see the download and preview link
          </div>
          @endif
        </div>

           <!--- home page image banner --> 
          <div class="form-row mb-5">
            <h1 for="image_upload">Home Page Image Banner File Upload</h1>
           
          <div class="col-md-12">
            <h5>Image Banner 1 </h5>
            <div class="row"> 
            @if($homepage_imageslider[0]->image_banner_1 ||  $homepage_imageslider[0]->image_banner_text1)
              <div class="col-md-7 ">
                <img style="height: 300px;width: 100%"src="{{$homepage_imageslider[0]->image_banner_1}}" alt=""  id="image_square_preview">
              </div>
              <div class="col-md-5 ">
              <input type="file"  accept="image/x-png,image/gif,image/jpeg"  class="form-control-file mb-2" id="image_banner1" name="image_banner1"  value="{{$homepage_imageslider[0]->image_banner_1}}">
              <textarea type="text"  name="image_banner_text1" class="form-control banner_text"   placeholder="Enter text for the first image banner" required>{{$homepage_imageslider[0]->image_banner_text1}}</textarea>
              </div>
              @else
              <input type="file"  accept="image/x-png,image/gif,image/jpeg"  class="form-control-file" id="image_banner1" name="image_banner1" >
              <textarea type="text"  name="image_banner_text1" class="form-control banner_text"   placeholder="Enter text for the first image banner" required></textarea>
            @endif
            </div>
          </div>


          <div class="col-md-12">
            <h5>Image Banner 2 </h5>
            <div class="row"> 
            @if($homepage_imageslider[0]->image_banner_2 ||  $homepage_imageslider[0]->image_banner_text2)
              <div class="col-md-7 ">
                <img style="height: 300px;width: 100%"src="{{$homepage_imageslider[0]->image_banner_2}}" alt=""  id="image_square_preview">
              </div>
              <div class="col-md-5 ">
              <input type="file"  accept="image/x-png,image/gif,image/jpeg"  class="form-control-file mb-2" id="image_banner2" name="image_banner2"  value="{{$homepage_imageslider[0]->image_banner_2}}">
              <textarea type="text"  name="image_banner_text2" class="form-control banner_text"   placeholder="Enter text for the second image banner" required>{{$homepage_imageslider[0]->image_banner_text2}}</textarea>
              </div>
              @else
              <input type="file"  accept="image/x-png,image/gif,image/jpeg"  class="form-control-file" id="image_banner2" name="image_banner2" >
              <textarea type="text"  name="image_banner_text2" class="form-control banner_text"   placeholder="Enter text for the second image banner" required></textarea>
            @endif
            </div>
          </div>


          <div class="col-md-12">
            <h5>Image Banner 3 </h5>
            <div class="row"> 
            @if($homepage_imageslider[0]->image_banner_3 ||  $homepage_imageslider[0]->image_banner_text3)
              <div class="col-md-7 ">
                <img style="height: 300px;width: 100%"src="{{$homepage_imageslider[0]->image_banner_3}}" alt=""  id="image_square_preview">
              </div>
              <div class="col-md-5 ">
              <input type="file"  accept="image/x-png,image/gif,image/jpeg"  class="form-control-file mb-2" id="image_banner3" name="image_banner3"  value="{{$homepage_imageslider[0]->image_banner_3}}">
              <textarea type="text"  name="image_banner_text3" class="form-control banner_text"   placeholder="Enter text for the third image banner" required>{{$homepage_imageslider[0]->image_banner_text3}}</textarea>
              </div>
              @else
              <input type="file"  accept="image/x-png,image/gif,image/jpeg"  class="form-control-file" id="image_banner3" name="image_banner3" >
              <textarea type="text"  name="image_banner_text3" class="form-control banner_text"   placeholder="Enter text for the third image banner" required></textarea>
            @endif
            </div>
          </div>


          <div class="col-md-12">
            <h5>Image Banner 4 </h5>
            <div class="row"> 
            @if($homepage_imageslider[0]->image_banner_4 ||  $homepage_imageslider[0]->image_banner_text4)
              <div class="col-md-7 ">
                <img style="height: 300px;width: 100%"src="{{$homepage_imageslider[0]->image_banner_4}}" alt=""  id="image_square_preview">
              </div>
              <div class="col-md-5 ">
              <input type="file"  accept="image/x-png,image/gif,image/jpeg"  class="form-control-file mb-2" id="image_banner4" name="image_banner4"  value="{{$homepage_imageslider[0]->image_banner_4}}">
              <textarea type="text"  name="image_banner_text4" class="form-control banner_text"   placeholder="Enter text for the third image banner" required>{{$homepage_imageslider[0]->image_banner_text4}}</textarea>
              </div>
              @else
              <input type="file"  accept="image/x-png,image/gif,image/jpeg"  class="form-control-file" id="image_banner4" name="image_banner4" >
              <textarea type="text"  name="image_banner_text4" class="form-control banner_text"   placeholder="Enter text for the third image banner" required></textarea>
            @endif
            </div>
          </div>

        <!--- Modal Banner Image --> 
        <h1 for="image_upload">Home Page Modal Image Banner File Upload</h1>
        <div class="col-md-12">
            <h5>Banner Modal Image</h5>
            <div class="row"> 
            @if($sitesetting->banner_modal_image)
             <div class="col-md-7 ">
                <img style="height: 300px;width: 100%" src="{{$sitesetting->banner_modal_image}}" alt=""  id="image_square_preview">
              </div>
              <div class="col-md-5 ">
              <input type="file"  accept="image/x-png,image/gif,image/jpeg"  class="form-control-file mb-2" id="banner_modal_image" name="banner_modal_image"  value="{{$sitesetting->banner_modal_image}}">
              </div>
              @else
              <div class="col-md-5 ">
              <input type="file"  accept="image/x-png,image/gif,image/jpeg"  class="form-control-file" id="banner_modal_image" name="banner_modal_image" >
              </div>
            @endif
            </div>
          </div>

          </div>

  

    
        <div class="form-group">
          <input class="btn btn-primary" type="submit" value="Save" id="btn-update-sitesettings">    
        </div> 
        
      </form>
    </div>
    
    
    <!-- form -->
    
  </div><!-- container -->
</div><!-- content -->


@section('page-scripts')

<script type="text/javascript">
  
  $(document).ready(function(){
    $('.banner_text').summernote({
        tabsize: 2,
        height: 150,
        toolbar: [
          ['style', ['style']],
          ['font', ['bold', 'underline', 'clear']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['table', ['table']],
          ['insert', ['link']],
          ['view', ['fullscreen']]
        ]
      });

    $('.description').summernote({
        placeholder: 'Enter Description',
        tabsize: 2,
        height: 300,
        toolbar: [
          ['style', ['style']],
          ['font', ['bold', 'underline', 'clear']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['table', ['table']],
          ['insert', ['link']],
          ['view', ['fullscreen']]
        ]
      });

      $('.mission_vision').summernote({
        placeholder: 'Enter Mission/Vision',
        tabsize: 2,
        height: 300,
        toolbar: [
          ['style', ['style']],
          ['font', ['bold', 'underline', 'clear']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['table', ['table']],
          ['insert', ['link']],
          ['view', ['fullscreen']]
        ]
      });
    
    
    
    $("#btn-update-sitesettings").click(function(event){
      event.preventDefault()
      updateSiteSettings()
    })
    
    
    function updateSiteSettings() { 
      
      swal({
        title:"Are you sure you want to save",
        icon: "info",
        buttons: true,
      })
      .then((willSubmit) => {
        if (willSubmit) {
          $("#sitesettingsform").submit()
        } else {
          
        }
      })
    }
    
    
    
    $(document).on("click", ".browse", function() {
      var file = $(this).parents().find(".file");
      file.trigger("click");
    });

    
    $('input[type="file"]').change(function(e) {
      var fileName = e.target.files[0].name;
      $("#file").val(fileName);
      
      var reader = new FileReader();
      reader.onload = function(e) {
        // get loaded data and render thumbnail.
        document.getElementById("preview").src = e.target.result;
      };
      // read the image file as a data URL.
      reader.readAsDataURL(this.files[0]);
    });
    
  })
  
  function readURLImageSquare(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#image_square_preview')
                .attr('src', e.target.result);
            };
            
            reader.readAsDataURL(input.files[0]);
        }
    }
  
</script>

@endsection

@stop