@extends('system._layouts.main')

@section('content')
    <div class="content content-components mb-5">
      <div class="row">
        <div class="col-lg-12 col-xl-12 mg-t-10">
            <div class="mg-b-10">
              <div class="card-header pd-t-20 d-sm-flex align-items-start justify-content-between bd-b-0 pd-b-0">
                <div>
                  <h4 class="mg-b-5">Dashboard</h4>
                </div>
              </div><!-- card-header -->
              <div class="card-body pd-y-30">
                <div class="row">
                  <div class="media col-lg-4">
                    <div class="wd-40 wd-md-50 ht-40 ht-md-50 bg-teal tx-white mg-r-10 mg-md-r-10 d-flex align-items-center justify-content-center rounded op-6">
                      <i data-feather="bar-chart-2"></i>
                    </div>
                    <div class="media-body">
                      <h6 class="tx-sans tx-uppercase tx-10 tx-spacing-1 tx-color-03 tx-semibold tx-nowrap mg-b-5 mg-md-b-8">Expense</h6>
                      <h4 class="tx-20 tx-sm-18 tx-md-24 tx-normal tx-rubik mg-b-0">PHP 10,000.00</h4>
                    </div>
                  </div>
                  <div class="media col-lg-4">
                    <div class="wd-40 wd-md-50 ht-40 ht-md-50 bg-pink tx-white mg-r-10 mg-md-r-10 d-flex align-items-center justify-content-center rounded op-5">
                      <i data-feather="bar-chart-2"></i>
                    </div>
                    <div class="media-body">
                      <h6 class="tx-sans tx-uppercase tx-10 tx-spacing-1 tx-color-03 tx-semibold mg-b-5 mg-md-b-8">Pending Transactions</h6>
                      <h4 class="tx-20 tx-sm-18 tx-md-24 tx-normal tx-rubik mg-b-0">32</h4>
                    </div>
                  </div>
                  <div class="media col-lg-4">
                    <div class="wd-40 wd-md-50 ht-40 ht-md-50 bg-primary tx-white mg-r-10 mg-md-r-10 d-flex align-items-center justify-content-center rounded op-4">
                      <i data-feather="bar-chart-2"></i>
                    </div>
                    <div class="media-body">
                      <h6 class="tx-sans tx-uppercase tx-10 tx-spacing-1 tx-color-03 tx-semibold mg-b-5 mg-md-b-8">Completed Transactions</h6>
                      <h4 class="tx-20 tx-sm-18 tx-md-24 tx-normal tx-rubik mg-b-0">32</small></h4>
                    </div>
                  </div>
                </div>
              </div><!-- card-body -->
              
            </div><!-- card -->            
          </div><!-- col -->       
            <div class="col-lg-12 mt-3">
               <div data-label="Recent Transactions" class="df-example demo-table">
                <div class="table-responsive">
                  <table class="table table-hover mg-b-0">
                    <thead>
                      <tr>
                        <th scope="col">Transaction ID</th>
                        <th scope="col">Name of user</th>
                        <th scope="col">Number of Traveler</th>
                        <th scope="col">Transaction Date</th>
                        <th scope="col">Payment Date</th>
                        <th scope="col">Amount</th>
                        <th scope="col">Status</th>          
                        <th scope="col">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th class="pt-3 pb-3">5878955</th>
                        <td class="pt-3 pb-3">Dhen Mark Torreno</td>
                        <td class="pt-3 pb-3">4</td>
                        <td class="pt-3 pb-3">02-18-2019</td>
                        <td class="pt-3 pb-3">02-14-2020</td>
                        <td class="pt-3 pb-3">PHP 10,000.00</td>
                        <td class="pt-3 pb-3"><span class="bg-success text-white p-2 rounded">Completed</span></td>
                        
                        <td>
                          <a class="btn btn-sm btn-light text-white bg-success" href="transaction_details.html">
                            <i data-feather="eye"></i>
                          </a>
                        </td>
                        
                      </tr>
                    </tbody>
                  </table>
                </div><!-- table-responsive -->
              </div>
            </div>

      </div><!-- container -->
    </div><!-- content -->
@stop