@extends('portal._layouts.main')

@section('content')
<div class="content content-components section-wrapper mb-5">
  	<div class="mb-2">
      <h4 id="section1" class="mg-b-10">Transaction Details</h4>
      <hr>
        <div class="row">
          <div class="col-md-4">
              <span class="font-semibold">7855878</span>
              <p>Transaction ID</p>
              <span class="font-semibold">5</span>
              <p>Number of Traveler</p>
          </div>
          <div class="col-md-4">
              <span class="font-semibold">12-12-2019</span>
              <p>Transaction Date</p>
              <span class="font-semibold">12-12-2019</span>
              <p>Payment Date</p>
          </div>
          <div class="col-md-4">
              
              <span class="font-semibold font-size--amount">PHP 10,000.00</span>
              <p>Total Amount</p>
              <span class="font-semibold bg-success text-white p-1 rounded">Completed</span>
              <p>Status</p>
          </div>
        </div>
    </div>
    <div class="row">
      <div class="col-lg-12 mt-2">
        <div class="demo-table">
            <div class="table-responsive">
              <table class="table table-hover mg-b-0">
                <thead class="bg-primary">
                  <tr>
                    <th scope="col" class="text-white">Airline Details</th>
                    <th scope="col" class="text-white">Traveler Details</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td class="p-3">
                      <div class="row">
                        <div class="col-xl-6">
                          <span class="font-semibold">7855878</span>
                          <p>Airline Ticket Number</p>

                          <span class="font-semibold">Economy - PHP 163.00</span>
                          <p>Passage</p>

                          <span class="font-semibold">02-28-2019</span>
                          <p>Date of Travel</p>
                        </div>
                        <div class="col-xl-6">
                          <span class="font-semibold">7855878</span>
                          <p>Point of Exit</p>

                          <span class="font-semibold">Singapore</span>
                          <p>Destination Country</p>

                          <span class="font-semibold">02-28-2019</span>
                          <p>Destination City</p>
                        </div>
                      </div>
                    </td>
                    <td class="p-3">
                      <div class="row">
                        <div class="col-xl-4">
                          <span class="font-semibold">Dhen Mark Torreno</span>
                          <p>Fullname</p>

                          <span class="font-semibold">Makati City</span>
                          <p>Address</p>

                          <span class="font-semibold">Filipino</span>
                          <p>Nationality</p>
                        </div>
                        <div class="col-xl-4">
                          <span class="font-semibold">09772471850</span>
                          <p>Contact Number</p>

                          <span class="font-semibold">emal@gmail.com</span>
                          <p>Email Address</p>

                          <span class="font-semibold">31235ASDDA</span>
                          <p>Passport Number</p>
                        </div>
                        <div class="col-xl-4">
                          <span class="font-semibold">Manila</span>
                          <p>Passport Issuing Office</p>

                          <span class="font-semibold">02-18-2019</span>
                          <p>Passport Issued Date</p>

                          <span class="font-semibold">02-18-2029</span>
                          <p>Passport Expiration Date</p>
                        </div>
                      </div>
                    </td>               
                  </tr>
                </tbody>
              </table>
            </div><!-- table-responsive -->
          </div><!-- df-example -->
       </div>        
  </div><!-- container -->
</div><!-- content -->
@stop