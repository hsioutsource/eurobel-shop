@extends('portal._layouts.main')

@section('content')
<div class="content content-components section-wrapper mb-5">
  <div class="pl-2 pr-2">
    <div class="row">
      <div class="col-md-7">
        <div class="tx-13 mg-b-25">
          <h3>Account Settings</h3>
          <p class="tx-14 mg-b-30">Fill out the form below to change your password.</p>
            <section class="mt-4">     
                <div class="row row-sm">
                  <div class="form-group col-md-12">
                    <label class="text-uppercase font-light">Old Password</label>
                    <input type="text" class="form-control" name="old_password">
                  </div>
                  <div class="form-group col-md-12">
                    <label class="text-uppercase font-light">New Password</label>
                    <input type="text" class="form-control" name="password">
                  </div> 
                  <div class="form-group col-md-12">
                    <label class="text-uppercase font-light">Confirm New Password</label>
                    <input type="text" class="form-control" name="confirm_password">
                  </div> 
                  <div class="col-md-12 text-right">
                    <button class="btn btn-primary"><i data-feather="edit" class="mr-2"></i>Change Password</button>
                  </div>        
                </div><!-- row -->
            </section>
        </div>
      </div>
    </div>
  </div><!-- container -->
</div><!-- content -->
@stop