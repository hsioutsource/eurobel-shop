@extends('portal._layouts.main')

@section('content')
<div class="content content-components section-wrapper mb-5">
      <div>
        <div class="d-flex justify-content-md-between mb-2">
          <h4 id="section1" class="mg-b-10">Daily Reports</h4>
        </div>
        <div class="row">
        	<div class="col-lg-6">
		        <div data-label="Search Value" class=" demo-forms">
		          <div class="row">
		            <div class="col-12 padding-right--custom">
		              <input type="text" class="form-control" placeholder="Search">
		            </div><!-- col -->
		          </div><!-- row -->
		        </div><!-- df-example -->
        	</div>
        	<div class="col-lg-6 mt-3 mt-md-3 mt-lg-0 mt-xl-0">
		        <div data-label="Filter Date" class="demo-forms">
		          <div class="row">
		            <div class="col-lg-5 padding-right--custom">
		              <input type="text" id="dateFrom" class="form-control" placeholder="Date From">
		            </div><!-- col -->
		            <div class="col-lg-5 padding-right--custom">
		              <input type="text" id="dateTo" class="form-control" placeholder="Date To">
		            </div><!-- col -->
		            <div class="col-2 padding-right--custom">
		            	<button class="btn btn-primary"><i data-feather="search"></i></button>
		            </div><!-- col -->
		          </div><!-- row -->
		        </div><!-- df-example -->
        	</div>
          <div class="col-lg-12 mt-3">
             <div class="df-example demo-table">
            <div class="table-responsive">
              <table class="table table-hover mg-b-0">
                <thead>
                  <tr>
                    <th scope="col">Transaction ID</th>
                    <th scope="col">Name of User</th>
                    <th scope="col">Number of Traveler</th>
                    <th scope="col">Transaction Date</th>
                    <th scope="col">Payment Date</th>
                    
                    <th scope="col">Amount</th>
                    <th scope="col">Status</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th class="pt-3 pb-3">5878955</th>
                    <td class="pt-3 pb-3">Dhen Mark</td>
                    <td class="pt-3 pb-3">1</td>
                    <td class="pt-3 pb-3">02-18-2020</td>
                    <td class="pt-3 pb-3">02-18-2020</td>
                    <td class="pt-3 pb-3">PHP 10,000.00</td>
                    <td class="pt-3 pb-3"><span class="bg-success p-2 rounded text-white">Completed</span></td>
                    
                    <td>
                     <a href="{{route('portal.transaction.show')}}" class="btn btn-sm btn-light text-white bg-success">
                         <i data-feather="eye"></i>
                     </a>
                 
                    </td>
                  </tr>
                </tbody>
              </table>
            </div><!-- table-responsive -->
          </div><!-- df-example -->
          </div>
          
        
        </div>

      </div><!-- container -->
    </div><!-- content -->
@stop

@section('page-scripts')

<script type="text/javascript">
	 
</script>
@stop