@extends('system._layouts.main')


@section('content')
<div class="content content-components mb-5">
    <div class="row">
        <div class="col-lg-12 col-xl-12 mg-t-10">
            <div class="mg-b-10">
                <div class="card-header pd-t-20 d-sm-flex align-items-start justify-content-between bd-b-0 pd-b-0">
                    <div>
                        <h4 class="mg-b-5">Demo </h4>
                        <p> List of all Scheduled Demo </p>
                    </div>
                </div><!-- card-header -->
                
            </div><!-- card -->            
        </div><!-- col -->      
        
        
        <!-- error message --> 
        @if ($errors->any())
        <div class="col-md-10 col-lg-10 ml-4 mt-3">
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
        @endif
        <!-- error message --> 
        
        <!-- success message --> 
        @if(Session::has('success-message'))
        <div class="col-md-10 col-lg-10 ml-4 mt-3">
            <div class="alert alert-success">
                {{ Session::get('success-message') }}
            </div>
        </div>
        @endif
        
        <!-- success message --> 
    
        <div class="col-lg-12 mt-3" style="padding:0 4%">
            <div data-label="All Scheduled Demo"  class="df-example demo-table">
                <div class="table-responsive">
                    <table class="table table-hover mg-b-0" id="table-demo">
                        <thead>
                            <tr>
                                <th scope="col">Date</th>
                                <th scope="col">Time</th>
                                <th scope="col">Full Name</th>   
                                <th scope="col">Address</th>    
                                <th scope="col">Email Address</th>    
                                <th scope="col">Contact Number</th>
                                <th scope="col">Options</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($scheduled_demo as $demo)
                            <tr>
                                <th class="pt-3 pb-3">{{$demo->demo_date}}</th>
                                <td class="pt-3 pb-3">{{$demo->demo_time}}</td>
                                <td class="pt-3 pb-3">{{$demo->full_name}}</td>
                                <td class="pt-3 pb-3">{{$demo->address}}</td>
                                <td class="pt-3 pb-3">{{$demo->email_address}}</td>
                                <td class="pt-3 pb-3">{{$demo->contact_number}}</td>
                                <td>
                                    <a class="btn btn-xs btn-light text-white bg-primary" href="/admin/scheduled-demo/{{$demo->id}}/show" target="_blank">
                                        <i data-feather="eye"></i>
                                    </a>
                                     <a class="btn btn-xs btn-light text-white bg-danger" id="btn-delete-scheduled-demo" scheduled_demo_id="{{$demo->id}}" >
                                        <i data-feather="trash"></i>
                                    </a>
                                </td> 
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div><!-- table-responsive -->
            </div>
        </div>
        
        
        
        
        
    </div><!-- container -->
</div><!-- content -->


@section('page-scripts')
<script type="text/javascript">
    
    
    $(document).ready( function () {
        $('#table-demo').DataTable();


    
          $(document).on("click", "#btn-delete-scheduled-demo", function() {
              
              var scheduled_demo_id = $(this).attr('scheduled_demo_id')
                swal({
                    title:"Are you sure you want to delete this scheduled demo?",
                    icon: "warning",
                    buttons: true,
                })
                .then((willSubmit) => {
                    if (willSubmit) {
                       window.location.href="/admin/scheduled-demo/"+scheduled_demo_id+"/delete"
                    } else {
                    
                     }
                })
          
            });


    } );
    
    
</script>

@endsection

@stop