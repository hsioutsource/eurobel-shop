@extends('system._layouts.main')


@section('content')
<div class="content content-components mb-5">
    <div class="row">
        <div class="col-lg-12 col-xl-12 mg-t-10">
            <div class="mg-b-10">
                <div class="card-header pd-t-20 d-sm-flex align-items-start justify-content-between bd-b-0 pd-b-0">
                    <div>
                        <h4 class="mg-b-5">Schedule Demo</h4>
                    </div>
                </div><!-- card-header -->
                
            </div><!-- card -->            
        </div><!-- col -->      
        
        
        <!-- error message --> 
        @if ($errors->any())
        <div class="col-md-10 col-lg-10 ml-4 mt-3">
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
        @endif
        <!-- error message --> 
        
        <!-- success message --> 
        @if(Session::has('success-message'))
        <div class="col-md-10 col-lg-10 ml-4 mt-3">
            <div class="alert alert-success">
                {{ Session::get('success-message') }}
            </div>
        </div>
        @endif
        
        <!-- success message --> 
        
        <div class="col-lg-12 col-md-12 ml-4">
            <div class="row"> 
                @foreach($scheduled_demo_info as $scheduled_demo_info)
                <div class="col-md-6">
                    <p class="text-small"><b> Preferred Demo Date: </b><span class="text-blue">{{$scheduled_demo_info->demo_date}} </span> </p> 
                </div>
                <div class="col-md-6">
                    <p class="text-small"> <b> Preferred Demod Time: </b> <span class="text-blue">{{$scheduled_demo_info->demo_time}} </span> </p> 
                </div>
                <div class="col-md-6">
                    <p class="text-small"> <b> Full Name: </b><span class="text-blue">{{$scheduled_demo_info->full_name}} </span> </p> 
                </div>
                <div class="col-md-6">
                    <p class="text-small"><b>Address: </b><span class="text-blue">{{$scheduled_demo_info->address}} </span> </p> 
                </div>
                <div class="col-md-6">
                    <p class="text-small"><b>Email Address: </b><span class="text-blue">{{$scheduled_demo_info->email_address}} </span> </p> 
                </div>
                <div class="col-md-6">
                    <p class="text-small"><b>Contact Number: </b><span class="text-blue">{{$scheduled_demo_info->contact_number}} </span> </p> 
                </div>
            </div>
            @endforeach
            
            
        </div>
        
        <div class="col-lg-12 col-md-12 ml-4">
            @foreach($scheduled_demo as $demo)
           
                <div class="row bg-light-gray" style="padding-bottom:50px;padding-top: 30px;margin-top: 50px">
                    <div class="col-md-3">
                        <img src="{{$demo->image_rectangle}}" style="height: 50%;width: auto" alt="">
                    </div>
                    <div class="col-md-6">
                        <h2 class="text-blue">Product Description</h2>
                        <p class="text-small">Product Name: {!!$demo->name!!}</p>
                        <p class="text-small">{!!$demo->code!!}</p>
                        <p class="text-small">{!!$demo->description!!}</p>
                        <p class="text-small">Size: {{$demo->size}} ({{$demo->size_conversion}})</p>
                        <p class="text-small">Price: {{$demo->price}} </p>
                        <a href="{{route('frontend.product.show',[$demo->name,$demo->code])}}" target="_blank" class="btn bg-blue text-white" style="border-radius: 5px; background-color:#001737">VIEW PRODUCT</a>
                    </div> 
                </div>
            @endforeach
        </div>
        
        
        
        
        
    </div><!-- container -->
</div><!-- content -->


@section('page-scripts')
<script type="text/javascript">
    
    
</script>

@endsection

@stop