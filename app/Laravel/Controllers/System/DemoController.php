<?php 

namespace App\Laravel\Controllers\System;
use App\Laravel\Models\ScheduleDemo;
use App\Laravel\Models\ScheduleDemoProductDetail;
use Illuminate\Http\Request;
use DB;
use Session;

class DemoController extends Controller{

	public function index(){
        $scheduled_demo = ScheduleDemo::all()->sortByDesc("created_at");
		return view('system.scheduled-demo.index', compact(['scheduled_demo']));
	}

	public function show(Request $request, $id){
		$scheduled_demo_info = ScheduleDemo::where('id' ,$id)->get();
		$scheduled_demo = DB::table('schedule_demo_product_details')
		->join('schedule_demos', 'schedule_demo_product_details.schedule_demo_id', '=', 'schedule_demos.id')
		->join('product_details', 'schedule_demo_product_details.product_detail_id', '=', 'product_details.id')
		->join('products', 'product_details.product_id', '=', 'products.id')
		->select('products.*','schedule_demos.*','product_details.*')
		->where('schedule_demo_product_details.schedule_demo_id','=',$id)
		->get(); 
	
		return view('system.scheduled-demo.show', compact(['scheduled_demo','scheduled_demo_info']));
	}


	public function delete(Request $request, $id) {

        $product = ScheduleDemo::where('id', $id)->delete();
        Session::flash('success-message', 'Successfully deleted scheduled demo'); 
        return redirect('admin/scheduled-demo');

	}
}