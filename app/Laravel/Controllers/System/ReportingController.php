<?php 

namespace App\Laravel\Controllers\System;


use App\Laravel\Models\Partner;
use App\Laravel\Models\Sale;
use App\Laravel\Models\Business;
use App\Laravel\Models\TransactionManager;

use Carbon,Auth;

class ReportingController extends Controller{

	public function daily(){
		return view('portal.reporting.daily');
	}

	public function monthly(){
		return view('portal.reporting.monthly');
	}

	public function transaction_logs(){
		return view('portal.reporting.transaction-logs');
	}


}