<?php

namespace App\Laravel\Controllers\System;
use Illuminate\Http\Request;
use App\Laravel\Models\Subscribe;
use Session;

class SubscribeController extends Controller
{
    //

	public function Index(){ 

        $subscribers = Subscribe::all()->sortByDesc("created_at");
        return view('system.subscribe.index', compact('subscribers'));

    }
    
    public function delete($id){
        $showroom = Subscribe::where('id', $id)->delete();
        Session::flash('success-message', 'Successfully remove user to news letter notification'); 
        return redirect('/admin/subscribe');
	}
}
