<?php

namespace App\Laravel\Controllers\System;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Laravel\Models\Product;
use App\Laravel\Models\ProductDetail;
use Illuminate\Support\Facades\Validator;

use App\Laravel\Services\ImageUploader;
use Session;

class ProductController extends Controller
{
    public function index(){

     $products = Product::orderBy('name', 'desc')->get();
     return view('system.products.index', compact('products'));
    }


    public function edit($id){
		$product = Product::where('id', $id)->get();
		return view('system.products.edit', compact('product'));
	}

    public function delete($id){
        $product = Product::where('id', $id)->delete();
        Session::flash('success-message', 'Successfully deleted product'); 
        return redirect('admin/products');
	}
    public function create(){
		return view('system.products.create');
    }
    
    public function save(Request $request){

        $validation_list= [	];
		
		$validator = Validator::make($request->all(), $validation_list);
        
        
        		
		if ($validator->fails()) {
			return Redirect::back()->withInput($request->all())->withErrors($validator);
        }
        
        else { 

            $code = $request->code;
            $name =$request->name;
            $description =$request->description;
            $featured = $request->featured ? '1' : '0';;


      
             $destination = public_path('uploads/images/products');

             if($request->hasFile('image_rectangle')) {
                $image = ImageUploader::upload($request->file('image_rectangle'), "uploads/products");

                $image_rectangle_url = "{$image['directory']}/resized/{$image['filename']}";

             // $image_rectangle_name = time().uniqid().'-product.'.request()->image_rectangle->getClientOriginalName();
             // $image_rectangle_url = env('APP_URL').'uploads/images/products/'.$image_rectangle_name;
             // request()->image_rectangle->move( $destination, $image_rectangle_name);
             }

             if($request->hasFile('image_square')) {

             // $image_square_name = time().uniqid().'-product.'.request()->image_square->getClientOriginalName();
             // $image_square_url = env('APP_URL').'uploads/images/products/'.$image_square_name;
             // request()->image_square->move( $destination, $image_square_name);

             $image = ImageUploader::upload($request->file('image_square'), "uploads/products");

             $image_square_url = "{$image['directory']}/resized/{$image['filename']}";

             }


             if($request->hasFile('image_circle')) {
        
             // $image_circle_name = time().uniqid().'-product.'.request()->image_circle->getClientOriginalName();
             // $image_circle_url = env('APP_URL').'uploads/images/products/'.$image_circle_name;
             // request()->image_circle->move( $destination, $image_circle_name);

             $image = ImageUploader::upload($request->file('image_circle'), "uploads/products");

             $image_circle_url = "{$image['directory']}/resized/{$image['filename']}";

             }

             $product = Product::create([
                'code' => $code ? $code : 'N/A', 
                'name' => $name ?  $name : 'N/A', 
                'description' => $description ? $description : 'N/A',
                'featured' =>  $featured,
                'third_line' => $request->third_line,
                'text_color' => $request->color,
                'image_rectangle' => isset($image_rectangle_url) ? $image_rectangle_url : 'N/A'  ,
                'image_square' =>  isset($image_square_url) ? $image_square_url : 'N/A',
                'image_circle' =>  isset($image_circle_url) ? $image_circle_url : 'N/A'

            ]);

    
             $product_id = $product->id;

             $count = count($request->prices);

              
           
             for ($i=0; $i < $count ; $i++) { 

                if($request->prices[$i] != null) {

                    $validation_list= [
                        'prices.'.$i => 'required|integer',
                      
                    ];
                    
                    $messages = [
                        'prices.*.integer'  => 'All prices must be an integer value'
                    ];
            
                    $validator = Validator::make($request->all(), $validation_list, $messages);
               

                    if ($validator->fails()) 
                     { 
                            	return Redirect::back()->withInput($request->all())->withErrors($validator);
                     }
                 } 



                  $productDetail = ProductDetail::create([
                   'product_id' =>   $product_id,
                   'price' => $request->prices[$i],
                   'size' => $request->sizes[$i],
                   'size_conversion' => $request->conversion_sizes[$i]
                 ]);
             }
            
                

            Session::flash('success-message', 'Successfully added new product'); 
			return redirect('admin/products');
        }


    }


    public function update(Request $request, $id) {
 
        $validation_list= [];
		
		$validator = Validator::make($request->all(), $validation_list);
        
        		
		if ($validator->fails()) {
			return Redirect::back()->withInput($request->all())->withErrors($validator);
        }
        
        else { 

            $code = $request->code;
            $name =$request->name;
            $description =$request->description;
            $featured = $request->featured ? '1' : '0';;

            $destination = public_path('uploads/images/products');
             
             if($request->hasFile('image_rectangle')) {

                $validator = Validator::make($request->all(),  [
                    'image_rectangle' => 'required|image',

                ]);

                $image = ImageUploader::upload($request->file('image_rectangle'), "uploads/products");

                $image_rectangle_url = "{$image['directory']}/resized/{$image['filename']}";

                // $image_rectangle_name = time().uniqid().'-product.'.request()->image_rectangle->getClientOriginalName();
                // $image_rectangle_url = env('APP_URL').'uploads/images/products/'.$image_rectangle_name;
                // request()->image_rectangle->move( $destination, $image_rectangle_name);

                $product = Product::updateOrCreate(
                    ['id' => $id],
                    [

                    'image_rectangle' => $image_rectangle_url,
                ]);

             }


             if($request->hasFile('image_square')) {

                $validator = Validator::make($request->all(),  [
                    'image_square' => 'required|image',

                ]);

                $image = ImageUploader::upload($request->file('image_square'), "uploads/products");

                $image_square_url = "{$image['directory']}/resized/{$image['filename']}";
          
                // $image_square_name = time().uniqid().'-product.'.request()->image_square->getClientOriginalName();
                // $image_square_url = env('APP_URL').'uploads/images/products/'.$image_square_name;
                // request()->image_square->move( $destination, $image_square_name);

                
                $product = Product::updateOrCreate(
                    ['id' => $id],
                    [

                    'image_square' => $image_square_url,
                ]);


            }

            
            if($request->hasFile('image_circle')) {

                $validator = Validator::make($request->all(),  [
                    'image_circle' => 'required|image',

                ]);
             // $image_circle_name = time().uniqid().'-product.'.request()->image_circle->getClientOriginalName();
             // $image_circle_url = env('APP_URL').'uploads/images/products/'.$image_circle_name;
             // request()->image_circle->move( $destination, $image_circle_name);

                $image = ImageUploader::upload($request->file('image_circle'), "uploads/products");

                $image_circle_url = "{$image['directory']}/resized/{$image['filename']}";

                  
             $product = Product::updateOrCreate(
                ['id' => $id],
                [

                'image_circle' => $image_circle_url
            ]);
            }

            $product = Product::updateOrCreate(
                ['id' => $id],
                [
                'code' => $code ?  $code : 'N/A', 
                'name' => $name? $name: 'N/A', 
                'description' => $description ? $description : 'N/A',
                'third_line' => $request->third_line,
                'text_color' => $request->color,
                'featured' =>  $featured,
            ]);

    
             $product_id = $product->id;
             $count = count($request->detail_id);

             for ($i=0; $i < $count ; $i++) { 

                    
                if($request->prices[$i] != null) {

                    $validation_list= [
                        'prices.'.$i => 'required|integer',
                      
                    ];
                    
                    $messages = [
                        'prices.*.integer'  => 'All prices must be an integer value'
                    ];
            
                    $validator = Validator::make($request->all(), $validation_list, $messages);
               

                    if ($validator->fails()) 
                     { 
                            	return Redirect::back()->withInput($request->all())->withErrors($validator);
                     }
                 } 

                  $productDetail = ProductDetail::updateOrCreate(
                  ['id'=> $request->detail_id[$i]],
                  [
                   'product_id' =>   $product_id,
                   'price' => $request->prices[$i],
                   'size' => $request->sizes[$i],
                   'size_conversion' => $request->conversion_sizes[$i]
                 ]);
             
            }
            
            Session::flash('success-message', 'Successfully update product '. $request->code); 
			return Redirect::back();
        }

    }
	

}
