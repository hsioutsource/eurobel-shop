<?php

namespace App\Laravel\Controllers\System;

/**
 * Models used in this controller
 */
use App\Laravel\Models\{Sale,SaleDetail};

/**
 * Request used in this Controller
 */
use App\Laravel\Requests\System\SaleRequest;

use App\Laravel\Services\ImageUploader;

use Session;

class SaleController extends Controller
{
    protected $data;

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->data['sales'] = Sale::paginate(10);

        return view('system.sale.index',$this->data);
    }


    public function create()
    {
        return view('system.sale.create',$this->data);
    }

    public function store(SaleRequest $request)
    {
        $sale = new sale;

        $sale->code = $request->code;
        $sale->name = $request->name;
        $sale->third_line = $request->third_line;
        $sale->text_color = $request->color;
        $sale->description = $request->description;
        
        $images = ['image_rectangle','image_square','image_circle'];

        foreach($images as $image)
        {
            if($request->hasFile($image))
            {
                $new_image = ImageUploader::upload($request->file($image),'uploads/sale');

                $sale->$image = "{$new_image['directory']}/resized/{$new_image['filename']}";
            }
        }
        $sale->save();

        foreach($request->prices as $key => $price)
        {
            if($price)
            {
                $sale_detail = new SaleDetail;

                $sale_detail->sale_id = $sale->id;
                $sale_detail->size = $request->sizes[$key];
                $sale_detail->size_conversion = $request->conversion_sizes[$key];
                $sale_detail->price = $price;
                $sale_detail->save(); 
            }
        }

        session()->flash('success-message','Successfully created sale');

        return redirect()->route('system.sale.index');
    }
    

    public function show()
    {
        $this->data['sale'] = request()->sale;

        return view('system.sale.show',$this->data);

    }
    public function edit()
    {
        $this->data['sale'] = request('sale');

        return view('system.sale.edit',$this->data);
    }

    public function update(SaleRequest $request)
    {
        // dd($request->all());

        $sale = request('sale');

        $sale->code = $request->code;
        $sale->name = $request->name;
        $sale->third_line = $request->third_line;
        $sale->text_color = $request->color;
        $sale->description = $request->description;
        
        $images = ['image_rectangle','image_square','image_circle'];

        foreach($images as $image)
        {
            if($request->hasFile($image))
            {
                $new_image = ImageUploader::upload($request->file($image),'uploads/sale');

                $sale->$image = "{$new_image['directory']}/resized/{$new_image['filename']}";
            }
        }
        $sale->save();

        foreach($request->prices as $key => $price)
        {
            if($price)
            {
                $sale_detail = SaleDetail::where('size',$request->sizes[$key])
                                         ->where('sale_id',$sale->id)
                                         ->where('size_conversion',$request->conversion_sizes[$key])
                                         ->first();

                $sale_detail->sale_id = $sale->id;
                $sale_detail->size = $request->sizes[$key];
                $sale_detail->size_conversion = $request->conversion_sizes[$key];
                $sale_detail->price = $price;
                $sale_detail->save(); 
            }
        }

        session()->flash('success-message',"Successfully update sale");

        return redirect()->route('system.sale.index');
    }

    public function delete()
    {
        $sale = request('sale');

        $sale_description = SaleDetail::where('sale_id',$sale->id)->get();

        foreach($sale_description as $description)
        {
            $description->delete();
        }

        $sale->delete();

        Session::flash('success-message','Successfully deleted sale');

        return redirect()->route('system.sale.index');
    }
}