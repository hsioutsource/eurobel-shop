<?php 

namespace App\Laravel\Controllers\System;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Laravel\Models\AboutPageSettings;
use Illuminate\Support\Facades\Validator;
use Session;
use Illuminate\Support\Arr;

use Carbon,Auth;

class AboutPageSettingsController extends Controller{
	


	public function index(){
		
	  $aboutpagesettings = AboutPageSettings::all();
	  return view('system.aboutpagesettings.index', compact('aboutpagesettings'));
    }
    
    public function update(Request $request){
			
	
		 $countValue = AboutPageSettings::count();
		 
		if ( $countValue  === 0) {

			$validation_list= [
				'text' => 'required|array',
			];
				
			$messages = [
				'text.array' => 'Please enter atleast one description for text',
			];
	
			$validator = Validator::make($request->all(), $validation_list, $messages);
	
	
	
			if ($validator->fails()) {
				return Redirect::back()->withInput($request->all())->withErrors($validator);
			}
			
			else { 
			$count = count($request->text);
		 
			for ($i=0; $i < $count ; $i++) { 
	
				if(isset($request->image[$i])) {
					$destination_about_image = public_path('uploads/images/aboutus');
					$about_image_name = time().'-homepagemodalbanner.'.$request->image[$i]->getClientOriginalName();
					$about_image_url = env('APP_URL').'uploads/images/aboutus/'.$about_image_name;
					$request->image[$i]->move( $destination_about_image, $about_image_name);
	
					$aboutpageimage = AboutPageSettings::updateOrCreate(
					['id' => $i + 1],
					[
						'image' => $about_image_url,
					]);
	
				 }
	
	
				
				$aboutpagesettings = AboutPageSettings::updateOrCreate(
						['id'=> $i + 1],
						[
						'text' =>  isset($request->text[$i]) ? $request->text[$i] : '',
						'align_image_left' =>  isset($request->alignimageleft[$i]) ?  '1'  :  '0' ,
				]);
				 
				}
			}

		}

		else { 

			
			$max_count_id = AboutPageSettings::max('id');
			$min_count_id = AboutPageSettings::min('id');

			//dd($max_count_id);
		   
		        
			// /dd(();
			$validation_list= [
				'text' => 'required|array',
			];
				
			$messages = [
				'text.array' => 'Please enter atleast one description for text',
			];
	
			$validator = Validator::make($request->all(), $validation_list, $messages);
	
	
	
			if ($validator->fails()) {
				return Redirect::back()->withInput($request->all())->withErrors($validator);
			}
			
			else { 
			$count = ($max_count_id + count($request->id) + 2);


			for ($i= $min_count_id; $i <= $count ; $i++) { 

			if(isset($request->id[$i])) {
				
				if(isset($request->image[$i])) {
					$destination_about_image = public_path('uploads/images/aboutus');
					$about_image_name = time().'-homepagemodalbanner.'.$request->image[$i]->getClientOriginalName();
					$about_image_url = env('APP_URL').'uploads/images/aboutus/'.$about_image_name;
					$request->image[$i]->move( $destination_about_image, $about_image_name);
	
					$aboutpageimage = AboutPageSettings::updateOrCreate(
					['id' => $i],
					[
						'image' => $about_image_url,
					]);
	
				}


			
				$aboutpagesettings = AboutPageSettings::updateOrCreate(
						['id'=> $i],
						[
						'text' =>   $request->text[$i],
						'align_image_left' =>  isset($request->alignimageleft[$i]) ?  '1'  :  '0' ,
				]);
			}

		 else { 
			  $resultQuery  = AboutPageSettings::where('id', $i)->get();
			  if(!$resultQuery->isEmpty()  && !isset($request->id[$i])) {
				$aboutpagedelete = AboutPageSettings::where('id', $i)->delete();
			  }
			}
		  }
		}
	  }
	Session::flash('success-message', 'Successfully update about page settings'); 
	return Redirect::back();
	}
}