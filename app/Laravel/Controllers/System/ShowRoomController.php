<?php

namespace App\Laravel\Controllers\System;
use Illuminate\Http\Request;
use App\Laravel\Models\ShowRoom;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Session;

use App\Laravel\Services\ImageUploader;


class ShowRoomController extends Controller
{
    //


    public function index(){

        $showrooms = ShowRoom::orderBy('location_name', 'desc')->get();
        return view('system.showroom.index', compact('showrooms'));
    }

    public function create(){

        return view('system.showroom.create');
    }

    public function edit($id){

        $showroom = ShowRoom::where('id', $id)->get();
        return view('system.showroom.edit',compact('showroom'));
    }
    public function delete($id){
        $showroom = ShowRoom::where('id', $id)->delete();
        Session::flash('success-message', 'Successfully deleted show room'); 
        return redirect('/admin/showroom');
	}
    public function update(Request $request, $id) {


		$validation_list= [
			'location_name' => 'required',	
			'address' => 'required',
            'contact_number' => 'required|min:6',
		];
		
		$validator = Validator::make($request->all(), $validation_list);
        
        
        		
		if ($validator->fails()) {
			return Redirect::back()->withInput($request->all())->withErrors($validator);
        }
        
        else { 

            $location_name = $request->location_name;
            $address =$request->address;
            $contact_number =$request->contact_number;

            if($request->hasFile('image')) {

             $validator = Validator::make($request->all(),  [
                    'image' => 'required|image',

            ]);

            $destination = public_path('uploads/images/showrooms');

            // $image_name = time().'-product.'.request()->image->getClientOriginalName();
            // $image_url = env('APP_URL').'uploads/images/showrooms/'.$image_name;
            // request()->image->move( $destination, $image_name);

            $image = ImageUploader::upload($request->file('image'), "uploads/showrooms");

            $image_url = "{$image['directory']}/resized/{$image['filename']}";


            $product = ShowRoom::updateOrCreate(
            ['id' => $id],
            [
                'image' => $image_url
            
            ]);
            }

            $product = ShowRoom::updateOrCreate(
                ['id' => $id],
                [
                'location_name' => $location_name, 
                'address' => $address, 
                'contact_number' => $contact_number
            
            ]);
            Session::flash('success-message', 'Successfully updated show room '. $request->location_name); 
			return Redirect::back();
        }


    }


    public function save(Request $request){

        
		$validation_list= [
			'location_name' => 'required',	
			'address' => 'required',
            'contact_number' => 'required|min:6',
            'image' => 'required'
		];
		
		$validator = Validator::make($request->all(), $validation_list);
        
        
        		
		if ($validator->fails()) {
			return Redirect::back()->withInput($request->all())->withErrors($validator);
        }
        
        else { 

            $location_name = $request->location_name;
            $address =$request->address;
            $contact_number =$request->contact_number;

            
            $destination = public_path('uploads/images/showrooms');

            // $image_name = time().'-product.'.request()->image->getClientOriginalName();
            // $image_url = env('APP_URL').'uploads/images/showrooms/'.$image_name;
            // request()->image->move( $destination, $image_name);

            $image = ImageUploader::upload($request->file('image'), "uploads/showrooms");

            $image_url = "{$image['directory']}/resized/{$image['filename']}";



            $product = ShowRoom::create([
                'location_name' => $location_name, 
                'address' => $address, 
                'contact_number' => $contact_number,
                'image' => $image_url
            
            ]);
            Session::flash('success-message', 'Successfully added new show room'); 
			return redirect('/admin/showroom');
        }

    }
}