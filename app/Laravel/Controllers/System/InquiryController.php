<?php

namespace App\Laravel\Controllers\System;
use Illuminate\Http\Request;
use App\Laravel\Models\Inquiry;
use Session;
class InquiryController extends Controller
{
    public function index(){
      
        $inquiries = Inquiry::all()->sortByDesc("created_at");
        return view('system.inquiry.index', compact('inquiries'));
    }

    public function delete($id){
        $showroom = Inquiry::where('id', $id)->delete();
        Session::flash('success-message', 'Successfully remove user to inquiry list'); 
        return redirect('/admin/inquiries');
	}

}
