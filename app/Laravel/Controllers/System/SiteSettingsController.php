<?php 

namespace App\Laravel\Controllers\System;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Laravel\Models\SiteSettings;
use App\Laravel\Models\HomePageImageslider;
use App\Laravel\Requests\System\HomePageImageRequest;
use Illuminate\Support\Facades\Validator;
use Session;
use App\Laravel\Services\ImageUploader;
use App\Laravel\Services\FileUploader;



use Carbon,Auth;

class SiteSettingsController extends Controller{
	
	public function index(){
		
		$sitesettings = SiteSettings::where('id', '=', '1')->get();
		$homepage_imageslider = HomePageImageslider::get();
		return view('system.sitesettings.index',compact('sitesettings','homepage_imageslider'));
	}
	
	public function update(Request $request) { 

		
		$validation_list= [
			'facebook_link' => 'required',	
			'instagram_link' => 'required',
			'email' => 'required|email',
			'mission_vision' => 'required|min:20',
			'contact_number_1' => 'required',
			'contact_number_2' => 'required',
			'contact_number_3' => 'required',
			'contact_number_4' => 'required',
		];
		
		$validator = Validator::make($request->all(), $validation_list);
		
		
		if ($validator->fails()) {
			return Redirect::back()->withInput($request->all())->withErrors($validator);
		}
		
		else{ 
			$destination = public_path('uploads/pdf/');
			$show_banner = $request->show_banner ? '1' : '0';
			$facebook_link = $request->facebook_link;
			$instagram_link = $request->instagram_link;
			$mission_vision = $request->mission_vision;
			$contact_number_1 = $request->contact_number_1;
			$contact_number_2 = $request->contact_number_2;
			$contact_number_3 = $request->contact_number_3;
			$contact_number_4 = $request->contact_number_4;
			$email = $request->email;
			$home_page_slider_text = $request->home_page_slider_text;
			 
			 
			$update = SiteSettings::updateOrCreate(
				['id' => 1],
				['show_banner' => $show_banner,
				'facebook_link' => $facebook_link,
				'instagram_link' =>  $instagram_link,
				'mission_vision' => $mission_vision,
				'email' => $email,
				'contact_number_1' => $contact_number_1,
				'contact_number_2' => $contact_number_2,
				'contact_number_3' => $contact_number_3,
				'contact_number_4' => $contact_number_4, 
 				]
				
			);

			if($request->hasFile('banner_modal_image')) {

				


				$destination_modal_banner = public_path('uploads/images/homepagemodalbanner');
                $validator = Validator::make($request->all(),  [
					'banner_modal_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

                ]);

                $image = ImageUploader::upload($request->file('banner_modal_image'), "uploads/banner");

                $banner_modal_image_url = "{$image['directory']}/resized/{$image['filename']}";

				// $banner_modal_image_name = time().'-homepagemodalbanner.'.request()->banner_modal_image->getClientOriginalName();
				// $banner_modal_image_url = env('APP_URL').'uploads/images/homepagemodalbanner/'.$banner_modal_image_name;
				// request()->banner_modal_image->move( $destination_modal_banner, $banner_modal_image_name);

                $product = SiteSettings::updateOrCreate(
                    ['id' => '1'],
				[
					'banner_modal_image' => $banner_modal_image_url,
                ]);

			 }

		
			if($request->hasFile('catalog')) {

                $validator = Validator::make($request->all(),  [
                    'catalog' => 'required',

                ]);
				$catalog_name = time().'-catalog.'.request()->catalog->getClientOriginalName();
				$remove_spaces_name = preg_replace('/\s+/', '', $catalog_name);
				// $catalog_url = env('APP_URL').'uploads/pdf/'.$remove_spaces_name;
                // request()->catalog->move( $destination, $remove_spaces_name);

                $file = FileUploader::upload($request->file('catalog'), "uploads/files");

                $catalog_url = "{$file['directory']}/{$file['filename']}";

                $product = SiteSettings::updateOrCreate(
                    ['id' => '1'],
				[
					'catalog_name' => $remove_spaces_name,
					'catalog_link' => $catalog_url
                ]);

			 }
			 
			 		
			if($request->hasFile('privacy')) {

                $validator = Validator::make($request->all(),  [
                    'privacy' => 'required',

                ]);
                // $privacy_name = time().'-privacy.'.request()->privacy->getClientOriginalName();
                // $privacy_url = env('APP_URL').'uploads/pdf/'.$privacy_name;
                // request()->privacy->move( $destination, $privacy_name);

                $file = FileUploader::upload($request->file('privacy'), "uploads/files");

                $privacy_url = "{$file['directory']}/{$file['filename']}";

                $product = SiteSettings::updateOrCreate(
                    ['id' => '1'],
				[
					'privacy_name' => $privacy_name,
					'privacy_link' => $privacy_url
                ]);

			 }
			 
			 if($request->hasFile('terms_of_service')) {

                $validator = Validator::make($request->all(),  [
                    'terms_of_service' => 'required',

                ]);
                // $terms_of_service_name = time().'-terms_of_service.'.request()->terms_of_service->getClientOriginalName();
                // $terms_of_service_url = env('APP_URL').'uploads/pdf/'.$terms_of_service_name;
                // request()->terms_of_service->move( $destination, $terms_of_service_name);

                $file = FileUploader::upload($request->file('terms_of_service'), "uploads/files");

                $terms_of_service_url = "{$file['directory']}/{$file['filename']}";

                $product = SiteSettings::updateOrCreate(
                    ['id' => '1'],
				[
					'terms_of_service_name' => $terms_of_service_name,
					'terms_of_service_link' => $terms_of_service_url
                ]);

             }


			 }
			 $homepage_imageslider = HomePageImageslider::get();
			 $image_banner1_url = $homepage_imageslider[0]->image_banner_1;
			 $image_banner2_url = $homepage_imageslider[0]->image_banner_2;
			 $image_banner3_url = $homepage_imageslider[0]->image_banner_3;
			 $image_banner4_url = $homepage_imageslider[0]->image_banner_4;
  
			 $destination = public_path('uploads/images/homepagebanner');
			 if($request->file('image_banner1')){
				$messages = [
					'image_banner1.image' => 'The first image banner type file should be an image.',
					'image_banner1.uploaded' => 'Failed to upload the first image banner. The image maximum size is 2MB.',
					'image_banner1  .required' => 'The first image banner field is required.',
					'image_banner_text1.required' => 'The first image banner text field is required.',
				];
				$rules  = [
					'image_banner1' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
					'image_banner_text1' => 'required',
				];
			
				$validator = Validator::make($request->all(), $rules, $messages);
				if ($validator->fails()) {
					return Redirect::back()->withInput($request->all())->withErrors($validator);
				}

				// $image_banner1_name = time().'-homepagebanner.'.request()->image_banner1->getClientOriginalName();
				// $image_banner1_url = env('APP_URL').'uploads/images/homepagebanner/'.$image_banner1_name;
				// request()->image_banner1->move( $destination, $image_banner1_name);

				$image = ImageUploader::upload($request->file('image_banner1'), "uploads/banner");

				$image_banner1_url = "{$image['directory']}/resized/{$image['filename']}";
 	
			 }
			 if($request->file('image_banner2') ){
				$messages = [
					'image_banner2.image' => 'The second image banner type file should be an image.',
					'image_banner2.uploaded' => 'Failed to upload the second image banner. The image maximum size is 2MB.',
					'image_banner2.required' => 'The second image banner field is required.',
					'image_banner_text2.required' => 'The second image banner text field is required.',
				];
				$rules  = [
					'image_banner2' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
					'image_banner_text2' => 'required',
				];
				$validator = Validator::make($request->all(), $rules, $messages);

				if ($validator->fails()) {
					return Redirect::back()->withInput($request->all())->withErrors($validator);
				}
				// $image_banner2_name = time().'-homepagebanner.'.request()->image_banner2->getClientOriginalName();
				// $image_banner2_url = env('APP_URL').'uploads/images/homepagebanner/'.$image_banner2_name;
  		// 		request()->image_banner2->move( $destination, $image_banner2_name);
				$image = ImageUploader::upload($request->file('image_banner2'), "uploads/banner");

				$image_banner2_url = "{$image['directory']}/resized/{$image['filename']}";
			 
			 }

			 if($request->file('image_banner3')  ){
				$messages = [
					'image_banner3.image' => 'The third image banner type of the uploaded file should be an image.',
					'image_banner3.uploaded' => 'Failed to upload the third image banner. The image maximum size is 2MB.',
					'image_banner3.required' => 'The third image banner field is required.',
					'image_banner_text3.required' => 'The third image banner text field is required.',
				];
				$rules  = [
					'image_banner3' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
					'image_banner_text3' => 'required',
				];
				$validator = Validator::make($request->all(), $rules, $messages);
				if ($validator->fails()) {
					return Redirect::back()->withInput($request->all())->withErrors($validator);
				}

				// $image_banner3_name = time().'-homepagebanner.'.request()->image_banner3->getClientOriginalName();
				// $image_banner3_url = env('APP_URL').'uploads/images/homepagebanner/'.$image_banner3_name;
				// request()->image_banner3->move( $destination, $image_banner3_name);
				$image = ImageUploader::upload($request->file('image_banner3'), "uploads/banner");

				$image_banner3_url = "{$image['directory']}/resized/{$image['filename']}";
			 
			 }
			 if($request->file('image_banner4')){
				$messages = [
					'image_banner4.image' => 'The fourth image banner type file should be an image.',
					'image_banner4.uploaded' => 'Failed to upload the fourth image banner. The image maximum size is 2MB.',
					'image_banner4.required' => 'The fourth image banner field is required.',
					'image_banner_text4.required' => 'The fourth image banner text field is required.',
				];
				$rules  = [
					'image_banner4' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
					'image_banner_text4' => 'required',
				];
				$validator = Validator::make($request->all(), $rules, $messages);
				if ($validator->fails()) {
					return Redirect::back()->withInput($request->all())->withErrors($validator);
				}
				// $image_banner4_name = time().'-homepagebanner.'.request()->image_banner4->getClientOriginalName();
				// $image_banner4_url = env('APP_URL').'uploads/images/homepagebanner/'.$image_banner4_name;
				// request()->image_banner4->move( $destination, $image_banner4_name);
				$image = ImageUploader::upload($request->file('image_banner4'), "uploads/banner");

				$image_banner4_url = "{$image['directory']}/resized/{$image['filename']}";
			 
			 }	 
			 
			 $update = HomePageImageslider::updateOrCreate(
				['id' => '1'],
				[
				'image_banner_1' => $image_banner1_url,
				'image_banner_text1' => $request->image_banner_text1,
				'image_banner_2' => $image_banner2_url,
				'image_banner_text2' => $request->image_banner_text2,
				'image_banner_3' => $image_banner3_url,
				'image_banner_text3' => $request->image_banner_text3,
				'image_banner_4' => $image_banner4_url,
				'image_banner_text4' => $request->image_banner_text4
				]);
				 
			
			Session::flash('success-message', 'Successfully update site settings details'); 
			return Redirect::back();
		}
		
}