<?php

namespace App\Laravel\Controllers\Frontend;
use App\Laravel\Models\Subscribe;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Session;


use Illuminate\Http\Request;

class SubscribeController extends Controller
{
    //

	public function save(Request $request) { 


		$validation_list= [
			'email' => 'required|email|unique:subscribes',
          
        ];
        
        $messages = [
			'email.unique' => 'The email is already subscribed.'
		];

        $validator = Validator::make($request->all(), $validation_list, $messages);
        $errors = $validator->errors()->get('email');

        		
		if ($validator->fails()) {
            Session::flash('subs-error', $errors);
			return redirect(strtok(back()->withInput($request->all())->getTargetUrl(),'?') . '?#subscribe');
        }
        
        else { 
            $email =$request->email;
            $product = Subscribe::create([
                'email' => $email,     
            ]);
            Session::flash('success-message-subs', 'Thank you for subscribing.'); 
			return redirect(strtok(back()->getTargetUrl(),'?') . '?#subscribe');
        }

    }
    
}
