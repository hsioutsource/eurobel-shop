<?php

namespace App\Laravel\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Laravel\Models\Sale;

class SaleController Extends Controller
{
    protected $data;

    public function index(Request $request)
    {

        $this->data['product_min_price'] = 0;
        $this->data['product_max_price'] = 1000;
        $this->data['default_max_price'] = 1000;

        $this->data['min_price'] = $request->min_price;
        $this->data['max_price'] = $request->max_price;
        $this->data['products'] = Sale::all();

        return view('frontend.sales.index',$this->data);
    }

    public function show($id)
    {
        $this->data['sale'] = Sale::find($id);
        return view('frontend.sales.show',$this->data);
    }
}