<?php 

namespace App\Laravel\Controllers\Frontend;

use App\Laravel\Models\Partner;
use App\Laravel\Models\Sale;
use App\Laravel\Models\Business;
use App\Laravel\Models\TransactionManager;
use App\Laravel\Models\Product;
use App\Laravel\Models\ProductDetail;
use Illuminate\Pagination\Paginator;
use Illuminate\Http\Request;
use Carbon,Auth;
use Illuminate\Database\Eloquent\Builder;
class ProductController extends Controller{

	public function index(Request $request){

		$products = new Product();
		$product_max_price = 1000;
		$default_max_price = 1000;
		$product_min_price = 0;
		$sort = $request->sort;
		$min_price = $request->min_price;
		$max_price = $request->max_price;
		$product_count = Product::all();
		
		if(count($product_count) > 0){
			// Get min price and max
			$product_max_price = ProductDetail::first()->max('price');
			$default_max_price = ProductDetail::first()->max('price');
			$product_min_price = 0;

			if($product_min_price == $product_max_price){
				$product_min_price = 0;
			}
		}
		
	
		// Filter products
		if($sort != "" || $min_price != "" || $max_price != ""){
			$products = $this->filter_products($sort, $min_price, $max_price);
			$product_max_price = $max_price ;
			$product_min_price = $min_price;
		}else{
			$products = $products::orderBy('name', 'asc')->paginate(8);
		}
		return view('frontend.product.index' , compact(['products', 'product_min_price','product_max_price','default_max_price','sort']));
	}
	public function show(Request $request, $id = NULL , $code = NULL){
		
		$product = Product::where('name',$id)->where('code', $code)->get();
		
		$carts = session()->get('cart');
	 	
		return view('frontend.product.show', compact(['product']));
	}
	public function storeDemoProduct(Request $request, $product_id, $demo_id)
	{
		$product_detail = ProductDetail::find($demo_id);
		if(!$product_detail) {
			abort(404);
		}
		$cart = session()->get('cart');
		// if cart is empty then this the first product
		if(!$cart) {
			$cart = [
				$demo_id => [
					"size" => $product_detail->size,
					"price" => $product_detail->price,
					"product_id" => $product_detail->product_id,
					"id" => $product_detail->id,
				]
			];
			session()->put('cart', $cart);

			return redirect(strtok(back()->with('success-message-add-product', 'Product added to your Demo List')->getTargetUrl(),'?') . '?#products-details');
		}
		// if cart not empty then check if this product exist then ignore
		if(isset($cart[$demo_id])) {
			return redirect(strtok(back()->with('error-message', 'Product already added to your Demo List')->getTargetUrl(),'?') . '?#products-details');
		}
		// if item not exist in cart then add to cart 
		$cart[$demo_id] = [
			"size" => $product_detail->size,
			"price" => $product_detail->price,
			"product_id" => $product_detail->product_id,
			"id" => $product_detail->id,
		];
		session()->put('cart', $cart);
		return redirect(strtok(back()->with('success-message-add-product', 'Product added to your Demo List')->getTargetUrl(),'?') . '?#products-details');
	}
	
	public function filter_products($sort, $min_price, $max_price){
		$products = new Product();
       
		
		$data = $products::whereHas('productdetail', function (Builder  $query) use ($min_price, $max_price) {
							$query->where('price', '>=', $min_price);
							$query->where('price', '<=', $max_price);
							})
						 ->orderBy('name', $sort)
						 ->paginate(8);
		return $data; 
	}
	public function search(Request $request){
		$search_text = $request->search_text;
		$products = Product::where('name', 'LIKE', "%{$search_text}%")
					->orWhere('code', 'LIKE', "%{$search_text}%")
					->get();
		return view('frontend.product.search', compact(['products','search_text']));
	}
}