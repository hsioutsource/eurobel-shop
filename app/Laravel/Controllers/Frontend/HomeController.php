<?php 

namespace App\Laravel\Controllers\Frontend;

use App\Laravel\Models\Partner;
use App\Laravel\Models\Sale;
use App\Laravel\Models\Business;
use App\Laravel\Models\TransactionManager;
use App\Laravel\Models\SiteSettings;
use App\Laravel\Models\Product;
use App\Laravel\Models\HomePageImageslider;

use Illuminate\Support\Facades\Cookie;
use Session;

use Carbon,Auth;

class HomeController extends Controller{

	public function underdevelopment(){
	 return view('frontend.underdevelopment');
	}

	public function index() {
  	  $homepage_imageslider = HomePageImageslider::where('id' , '1')->get();
	  $products = Product::where('featured' , '1')->get();
	  return view('frontend.index', compact('products','homepage_imageslider'));
	}
}