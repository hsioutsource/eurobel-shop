<?php 

namespace App\Laravel\Controllers\Frontend;

use App\Laravel\Models\Partner;
use App\Laravel\Models\Sale;
use App\Laravel\Models\Business;
use App\Laravel\Models\TransactionManager;
use App\Laravel\Models\ProductDetail;
use App\Laravel\Models\ScheduleDemo;
use App\Laravel\Models\ScheduleDemoProductDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

use Carbon,Auth, Session, DB;

class DemoController extends Controller{

	public function index(){
		$cart = session()->get('cart');
		if($cart == null){
			$products = [];
		}else{
			$demo_list_id = $this->demoListArray($cart);
			$products = ProductDetail::with('product')->whereIn('id', $demo_list_id)->get();
		}
		return view('frontend.demo.index', compact(['products']));
	}

	public function removeDemolist($id){
		$cart = session()->get('cart');
		if(isset($cart[$id])) {
			unset($cart[$id]);
			session()->put('cart', $cart);
		}
		 return redirect()->back()->with('success-remove-message', 'Product is removed from your Demo List.');
	}

	function demoListArray($array) {
		$demo_list = [];
		foreach ($array as $key => $val) {
			array_push($demo_list ,$key);
		}
		return $demo_list;
	 }
	 public function save(Request $request) { 
		 
		$todayDate = date('m/d/Y');
		$validation_list= [
			'demo_date' => 'required|after_or_equal:'.$todayDate,
			'demo_time' => 'required',
			'full_name' => 'required',	
			'address' => 'required',	
			'email_address' => $request->email_address != null ? 'sometimes|email' : '',
            'contact_number' => $request->contact_number != null ? 'sometimes|min:5|max:11' : '',
		];


		if(!$request->email_address && !$request->contact_number) {
			Session::flash('error-message', 'You must enter valid email address or contact number'); 
			return redirect(strtok(back()->withInput($request->all())->getTargetUrl(),'?') . '?#demo');
		 }

		  
	 	$formatted_date =  date('F d,Y');

		$messages = [
			'after_or_equal' => 'The :attribute  must be any day after ' . $formatted_date ,
			'contact_number.required' => 'Please enter valid Mobile or Telephone number',
			'contact_number.max' => 'Please enter valid Mobile or Telephone number'
		];


		$validator = Validator::make($request->all(), $validation_list, $messages);
		$cart = session()->get('cart');
	
		if ($validator->fails()) {
			return redirect(strtok(back()->withInput($request->all())->withErrors($validator)->getTargetUrl(),'?') . '?#demo');
        }
        else { 
			
            $demo_date = $request->demo_date;
            $demo_time = $request->demo_time;
            $full_name = $request->full_name;
            $address = $request->address;
            $email_address =$request->email_address;
			$contact_number =$request->contact_number;
			
            $schedule_demo = ScheduleDemo::create([
                'demo_date' => $demo_date, 
                'demo_time' => $demo_time, 
                'full_name' => $full_name, 
                'address' => $address, 
                'email_address' => $email_address != null ? $email_address : 'N/A', 
                'contact_number' => $contact_number != null ? $contact_number  : 'N/A',
			]);
			$schedule_demo_id = $schedule_demo->id;
			$cart = session()->get('cart');	
			if($cart != null){
				foreach ($cart as $key => $val) { 
					ScheduleDemoProductDetail::create([
					  'schedule_demo_id' => $schedule_demo_id, 
					  'product_detail_id' => $key,
				  ]);
			  }
			}
			session()->flush('cart');	
            Session::flash('success-message', 'Your preferred demo schedule has been successfully sent.  We’ll contact you soon to confirm.'); 
			return redirect(strtok(back()->getTargetUrl(),'?') . '?#demo');
        }
	}

}