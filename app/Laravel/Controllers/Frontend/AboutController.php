<?php 

namespace App\Laravel\Controllers\Frontend;

use App\Laravel\Models\AboutPageSettings;
use Carbon,Auth;

class AboutController extends Controller{

	public function index(){
		$aboutpagesettings = AboutPageSettings::all();
		return view('frontend.about.index', compact('aboutpagesettings'));
	}
}