<?php 

namespace App\Laravel\Controllers\Frontend;

use App\Laravel\Models\ShowRoom;

use Carbon,Auth;

class ShowroomController extends Controller{

	public function index(){

		$showrooms = ShowRoom::all();
		return view('frontend.showroom.index', compact('showrooms'));
	}
}