<?php 

namespace App\Laravel\Controllers\Frontend;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Laravel\Models\Inquiry;
use Illuminate\Support\Facades\Validator;

use Session;

use Carbon,Auth;

class ContactController extends Controller{

	public function index(){
		return view('frontend.contact.index');
	}

	public function save(Request $request) { 


		$validation_list= [
			'full_name' => 'required',	
            'email' => $request->email != null ? 'sometimes|email' : '',
            'contact_number' => $request->contact_number != null ? 'sometimes|min:5|max:11' : '',
        ];
    

      

        if(!$request->email && !$request->contact_number) {
            Session::flash('error-message', 'You must enter valid email address or contact number'); 
            return redirect(strtok(back()->withInput($request->all())->getTargetUrl(),'?') . '?#demo');
        }

		
		$validator = Validator::make($request->all(), $validation_list);
        
        
        		
		if ($validator->fails()) {
			return Redirect::back()->withInput($request->all())->withErrors($validator);
        }
        
        else { 

            $full_name = $request->full_name;
            $email =$request->email;
            $contact_number =$request->contact_number;



            $product = Inquiry::create([
                'full_name' => $full_name, 
                'email' => $email != null ? $email : 'N/A', 
                'contact_number' => $contact_number != null ? $contact_number : 'N/A',
            
            ]);
            Session::flash('success-message', 'Thank you! We’ll get back to you soon.'); 
			return Redirect::back();
        }



	}
}