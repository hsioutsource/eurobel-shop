<?php 

namespace App\Laravel\Controllers\Frontend;
use Illuminate\Http\Request;
use Carbon,Auth;

class DownloadController extends Controller{



	public function downloadCatalog(Request $request, $download_name, $file_name){
       
        $file = public_path()."/uploads/pdf/".$file_name;

        $headers = array
        (
            'content-Type:application/pdf', 
        );

        return  response()->download($file, $download_name,$headers );
	
	}
}