<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;

class ScheduleDemo extends Model
{
    protected $fillable = [ "demo_date","demo_time", "full_name", "address","email_address","contact_number"];

    public function scheduleDemoProductDetail(){
        return $this->belongsToMany('\App\Laravel\Models\ScheduleDemoProductDetail', 'schedule_demo_product_details', 'schedule_demo_id' );
    }

    
    public function productDetail(){
        return $this->belongsToMany('App\Laravel\Models\ScheduleDemoProductDetail', 'schedule_demo_product_details');
    }
     
}
