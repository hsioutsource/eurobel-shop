<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Inquiry extends Model
{
    //

    protected $fillable = ["full_name", "email", "contact_number"];

}
