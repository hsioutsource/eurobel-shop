<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ProductDetail extends Model
{
    //

    protected $fillable = ["product_id", "price", "size", "size_conversion"];

    public function product(){
        return $this->hasOne('App\Laravel\Models\Product',  'id', 'product_id');
    }
    public function demo(){
        return $this->belongsToMany('App\Laravel\Models\ScheduleDemoProductDetail', 'schedule_demo_product_details');
    }
}
