<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;

class ScheduleDemoProductDetail extends Model
{
    protected $fillable = [ "schedule_demo_id", "product_detail_id"];
    
    public function demo(){
        return $this->hasMany('\App\Laravel\Models\ScheduleDemo','id','schedule_demo_id');
    }
    public function product_detail(){
        return $this->belongsToMany('\App\Laravel\Models\ProductDetail','schedule_demo_product_details', 'id','product_detail_id');
    }
}
