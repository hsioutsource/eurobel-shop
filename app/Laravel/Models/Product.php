<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    
    protected $fillable = ["code", "name", "description", "featured", "image_rectangle", "image_square", "image_circle", "price","third_line","text_color"];


    public function productdetail(){

        return $this->hasMany('App\Laravel\Models\ProductDetail', 'product_id', 'id');
    }

}