<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class HomePageImageslider extends Model
{
    //
    protected $fillable = [
         "image_banner_1", "image_banner_text1",
         "image_banner_2", "image_banner_text2",
         "image_banner_3", "image_banner_text3",
         "image_banner_4", "image_banner_text4",
    ];

 

}
