<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ShowRoom extends Model
{
    
    protected $fillable = ["location_name","address","contact_number","image"];
}
