<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;

class Subscribe extends Model
{
    
    protected $fillable = ['email'];
}
