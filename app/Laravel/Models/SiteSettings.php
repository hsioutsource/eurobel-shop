<?php


namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SiteSettings extends Model
{
    //
    
    protected $fillable = [
    'show_banner',
    'facebook_link',
    'instagram_link', 
    'mission_vision', 
    'email', 
    'contact_number_1', 
    'contact_number_2',
    'contact_number_3', 
    'contact_number_4',
    'catalog_name',
    'catalog_link',
    'privacy_link',
    'privacy_name',
    'terms_of_service_link',
    'terms_of_service_link_name',
    'banner_modal_image'
    ];


}
