<?php 

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Business extends Model{
	
	use SoftDeletes;
	
	/**
	 * Enable soft delete in table
	 * @var boolean
	 */
	protected $softDelete = true;
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'business';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [];


	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * The attributes that created within the model.
	 *
	 * @var array
	 */
	protected $appends = [];

	public function getDisplayScopeAttribute(){
		if($this->business_scope == "CITYPROV")
			return "CITY/MUNICIPALITY";

		return $this->business_scope;
	}

	public function getCityNameAttribute(){
		if(count(explode('-', $this->town_name)) > 1) {
			return explode('-', $this->town_name)[1];
		}

		return $this->town_name;
	}

	public function owner(){
		return $this->hasOne('App\Laravel\Models\BusinessOwner', 'business_id', 'id');
	}

	public function files(){
		return $this->hasMany('App\Laravel\Models\BusinessFile', 'business_id', 'id');
	}

	public function transactions(){
		return $this->hasMany('App\Laravel\Models\TransactionManager', 'business_id', 'id');
	}

	public function sanitary_permit(){
		return $this->hasOne('App\Laravel\Models\BusinessFile', 'business_id', 'id')->where('type',"sanitary-permit")->orderBy("created_at" , "DESC");
	}

	public function bnrs_cert(){
		return $this->hasOne('App\Laravel\Models\BusinessFile', 'business_id', 'id')->where('type',"bnrs-certificate")->orderBy("created_at" , "DESC");
	}
	public function fire_safety(){
		return $this->hasOne('App\Laravel\Models\BusinessFile', 'business_id', 'id')->where('type',"fire-safety-certificate")->orderBy("created_at" , "DESC");
	}
	public function business_clearance(){
		return $this->hasOne('App\Laravel\Models\BusinessFile', 'business_id', 'id')->where('type',"business-clearance")->orderBy("created_at" , "DESC");
	}

	public function building_inspecton(){
		return $this->hasOne('App\Laravel\Models\BusinessFile', 'business_id', 'id')->where('type',"business-inspection")->orderBy("created_at" , "DESC");
	}
	
}