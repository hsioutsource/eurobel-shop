<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;


class AboutPageSettings extends Model
{
    

    protected $fillable = [
      'image',
      'align_image_left',
      'text'
    ];
    
}
